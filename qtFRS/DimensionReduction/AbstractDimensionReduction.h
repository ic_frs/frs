#ifndef ABSTRACTDIMENSIONREDUCTION_H
#define ABSTRACTDIMENSIONREDUCTION_H

#include <opencv2\core\core.hpp>
#include "../Features/Biovector.h"

class AbstractDimensionReduction
{
public:
	AbstractDimensionReduction() {} ;
	virtual ~AbstractDimensionReduction() {} ;

	virtual void createReductionSpace(cv::Mat &biovecMat) = 0;
	virtual Biovector reduce(Biovector &srcBiovec) = 0;

private:

};

#endif