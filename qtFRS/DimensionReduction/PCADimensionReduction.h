#ifndef PCADIMENSIONREDUCTION_H
#define PCADIMENSIONREDUCTION_H

#include <opencv2\core.hpp>
#include "AbstractDimensionReduction.h"

class PCADimensionReduction : public AbstractDimensionReduction
{
public:
	PCADimensionReduction() {} ;
	PCADimensionReduction(cv::Mat &biovecMat);

	~PCADimensionReduction() {} ;

	void createReductionSpace(cv::Mat &biovecMat);
	Biovector reduce(Biovector &srcBiovec);

private:
	cv::PCA _mPCA;
};

#endif