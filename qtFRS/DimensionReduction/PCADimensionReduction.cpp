#include "PCADimensionReduction.h"

PCADimensionReduction::PCADimensionReduction(cv::Mat &biovecMat)
{
	createReductionSpace(biovecMat);
}

void PCADimensionReduction::createReductionSpace(cv::Mat &biovecMat)
{
	_mPCA(biovecMat, cv::Mat(), CV_PCA_DATA_AS_ROW, 1.0);
}

Biovector PCADimensionReduction::reduce(Biovector &srcBiovec)
{
	Biovector resultBiovec;

	_mPCA.project(srcBiovec).row(0).copyTo(resultBiovec);

	return resultBiovec;
}