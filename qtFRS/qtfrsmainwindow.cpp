#include <QtWidgets/QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>
#include <QDebug>
#include <QTimer>

#include <time.h>
#include <chrono>

#include "GlobalOptions.h"

#include "qtfrsmainwindow.h"
#include "qtfrsconfigurationmanager.h"
#include "qtfrsbiovectorcreate.h"

#include "Tracker/Tracker.h"

using namespace std;
using namespace trk;

qtFRSMainWindow::qtFRSMainWindow(QWidget *parent)
	: QMainWindow(parent)
{
	if (!instance)
	{
		instance = this;
	}

	ui.setupUi(this);

	//��������� �� ������� � ������������ �������� � ������� ������������
	searchConfigManager();

	mVideoTimer = QSharedPointer<QTimer>(new QTimer);
	mThreadPool = QThreadPool::globalInstance();
	mDimensionReductor.clear();

	//������
	connect(mVideoTimer.data(), &QTimer::timeout,
		this, &qtFRSMainWindow::processFrame);
	
	//������� ������ �����, �����, ����
	connect(ui.startVideo, &QPushButton::clicked,
		this, &qtFRSMainWindow::processStartButton);
	connect(ui.pauseVideo, &QPushButton::clicked,
		this, &qtFRSMainWindow::processPauseButton);
	connect(ui.stopVideo, &QPushButton::clicked,
		this, &qtFRSMainWindow::processStopButton);

	connect(this, &qtFRSMainWindow::startButtonClicked,
		this, &qtFRSMainWindow::startVideoStream);
	connect(this, &qtFRSMainWindow::pauseButtonClicked,
		this, &qtFRSMainWindow::pauseVideoStream);
	connect(this, &qtFRSMainWindow::stopButtonClicked,
		this, &qtFRSMainWindow::stopVideoStream);

	//������������ ����� ���������� ���������� � ������ � ������������
	connect(this, &qtFRSMainWindow::frameReady,
		this, &qtFRSMainWindow::showFrame);
	connect(this, &qtFRSMainWindow::detectFace,
		this, &qtFRSMainWindow::addScrollAreaImage);
	connect(this, &qtFRSMainWindow::videoStreamStop,
		this, &qtFRSMainWindow::clearFrame);

	connect(this, &qtFRSMainWindow::recogFace,
		this, &qtFRSMainWindow::addPersonScrollArea);
	
	//�������� ��������� ������������
	connect(ui.configOpenButton, &QPushButton::clicked,
		this, &qtFRSMainWindow::openConfigManager);
	connect(ui.chooseConfigComboBox, SIGNAL(currentIndexChanged(const int)),
		this, SLOT(configComboBoxChanged(const int)));

	//����������
	connect(this, &qtFRSMainWindow::clikedCreateBiovectorBase,
		this, &qtFRSMainWindow::createBiovectors);
	connect(this, &qtFRSMainWindow::clikedReadBiovectorBase,
		this, &qtFRSMainWindow::readBiovectors);
	connect(this, &qtFRSMainWindow::biovectorBaseMessage2MainForm,
		this, &qtFRSMainWindow::biovectorBaseMessage2BiovectorBaseForm);

	//��� ������ ����������� � ������ "�������"
	_viewScrollAreaFaceImagesLayout = new QVBoxLayout;

	_viewScrollAreaDetectFaceImagesLayout = new QHBoxLayout;
	_viewScrollAreaRecognizeFaceImagesLayout = new QHBoxLayout;

	_viewScrollAreaFaceImagesLayout->addLayout(_viewScrollAreaDetectFaceImagesLayout);
	_viewScrollAreaFaceImagesLayout->addLayout(_viewScrollAreaRecognizeFaceImagesLayout);

	ui.scrollAreaWidgetContents->setLayout(_viewScrollAreaFaceImagesLayout);

	_viewScrollAreaFaceImagesLayout->setAlignment(Qt::AlignRight);

	_viewScrollAreaDetectFacesList = QSharedPointer<QList<QLabel*>>(new QList<QLabel*>());
	_viewScrollAreaRecognizeFacesList = QSharedPointer<QList<QLabel*>>(new QList<QLabel*>());

	_maxViewScrollAreaImageListSize = 50;
	_viewScrollAreaDetectFacesList->reserve(_maxViewScrollAreaImageListSize);
	_viewScrollAreaRecognizeFacesList->reserve(_maxViewScrollAreaImageListSize);

	for (int i = 0; i < _maxViewScrollAreaImageListSize; ++i)
	{
		_viewScrollAreaDetectFacesList->push_front(new QLabel);
		_viewScrollAreaRecognizeFacesList->push_front(new QLabel);
	}

	//��� ������ ���������� � "�������" ������
	_viewPersonScrollAreaLayout = new QHBoxLayout;
	
	_viewPersonScrollAreaRecognizeLayout = new QVBoxLayout;
	_viewPersonScrollAreaInfLayout = new QVBoxLayout;

	_viewPersonScrollAreaLayout->addLayout(_viewPersonScrollAreaRecognizeLayout);
	_viewPersonScrollAreaLayout->addLayout(_viewPersonScrollAreaInfLayout);

	ui.scrollAreaWidgetContents_2->setLayout(_viewPersonScrollAreaLayout);

	_viewPersonScrollAreaLayout->setAlignment(Qt::AlignTop);

	_viewPersonScrollAreaRecognizeList = QSharedPointer<QList<QLabel*>>(new QList<QLabel*>());
	_viewPersonScrollAreaInfList = QSharedPointer<QList<QLabel*>>(new QList<QLabel*>());;
	
	_viewPersonScrollAreaRecognizeList->reserve(_maxViewScrollAreaImageListSize);
	_viewPersonScrollAreaInfList->reserve(_maxViewScrollAreaImageListSize);

	for (int i = 0; i < _maxViewScrollAreaImageListSize; i++)
	{
		_viewPersonScrollAreaRecognizeList->push_front(new QLabel);
		_viewPersonScrollAreaInfList->push_front(new QLabel);
	}

	clearFrame();
}

//������� ���� ������� ���������� - ��� ������� �� ����� ����� ���������
qtFRSMainWindow* qtFRSMainWindow::instance;
qtFRSMainWindow* qtFRSMainWindow::getInstance()
{
	if (!instance)
	{
		instance = new qtFRSMainWindow();
	}
	return instance;
}
qtFRSMainWindow::~qtFRSMainWindow()
{
	if (videoStreaming)
		stopVideoStream();
}

//�������� ��������� ������������
void qtFRSMainWindow::openConfigManager()
{
	qtFRSConfigurationManager manager;
	manager.exec();
}

//�������� ��������� ��������� �����������
void qtFRSMainWindow::openBiovectorOptions()
{
	qtFRSBiovectorCreate* bioVopts = new qtFRSBiovectorCreate;
	bioVopts->setAttribute(Qt::WA_DeleteOnClose);

	connect(bioVopts, &qtFRSBiovectorCreate::clikedCreateBiovectorBaseButton,
		this, &qtFRSMainWindow::clikedCreateBiovectorBase);
	connect(bioVopts, &qtFRSBiovectorCreate::clikedReadBiovectorBaseButton,
		this, &qtFRSMainWindow::clikedReadBiovectorBase);

	connect(this, &qtFRSMainWindow::biovectorBaseMessage2BiovectorBaseForm,
		bioVopts, &qtFRSBiovectorCreate::showBiovectorBaseMessage);

	bioVopts->exec();
}

//�������� �����������
void qtFRSMainWindow::createBiovectors(QString &imagesPath)
{
	_faceImagesPath = imagesPath;
	mBiovectorBase = QSharedPointer<BiovectorBase>(new BiovectorBase());
	connect(mBiovectorBase.data(), &BiovectorBase::biovectorBaseMessageAppear,
		this, &qtFRSMainWindow::biovectorBaseMessage2MainForm);
	mBiovectorBase->createBivectorBase(_faceImagesPath.toStdString(), *mFaceDetector, mFaceProcessors);
	mBiovectorBase->readBiovectorBase(OPTIONS["Global"]["BiovectorBaseFolder"].string_data);
	mDimensionReductor.clear();
	if (OPTIONS["Global"]["ReduceDimensions"].bool_data)
	{
		mDimensionReductor = QSharedPointer<PCADimensionReduction>(new PCADimensionReduction());
		mDimensionReductor->createReductionSpace(mBiovectorBase->getBiovectorsAsRowsMat());
		mBiovectorBase->reduceDimensions(mDimensionReductor.data());
	}
	mBiovectorBase->getPersonInf(OPTIONS["Global"]["BiovectorBaseFolder"].string_data);
}

//������ �����������
void qtFRSMainWindow::readBiovectors(QString &imagesPath)
{
	_faceImagesPath = imagesPath;
	mBiovectorBase = QSharedPointer<BiovectorBase>(new BiovectorBase());
	connect(mBiovectorBase.data(), &BiovectorBase::biovectorBaseMessageAppear,
			this, &qtFRSMainWindow::biovectorBaseMessage2MainForm);
	
	mBiovectorBase->readBiovectorBase(OPTIONS["Global"]["BiovectorBaseFolder"].string_data);
	if (OPTIONS["Global"]["ReduceDimensions"].bool_data)
	{
		mDimensionReductor = QSharedPointer<PCADimensionReduction>(new PCADimensionReduction());
		mDimensionReductor->createReductionSpace(mBiovectorBase->getBiovectorsAsRowsMat());
		mBiovectorBase->reduceDimensions(mDimensionReductor.data());
	}
	mBiovectorBase->getPersonInf(OPTIONS["Global"]["BiovectorBaseFolder"].string_data);
}



/*
������� �� ������� ������
*/
void qtFRSMainWindow::processStartButton()
{
	toggleChooseConfigComboBox(false);
	emit startButtonClicked();
};
void qtFRSMainWindow::processPauseButton()
{
	emit pauseButtonClicked();
}
void qtFRSMainWindow::processStopButton()
{
	toggleChooseConfigComboBox(true);
	emit stopButtonClicked();
}

/*
������ ��� ������� � �������� ��������� �����.
����� � �������� ���������� ���������� � ����� ������� �� ���������.
������������ ����� ���������� � ������� �� �����.
*/
void qtFRSMainWindow::startThreadsProcessing()
{
	GlobalOptions::THREAD_COUNT = std::max(1, mThreadPool->maxThreadCount() - 3);
	videoStreaming = false;
	processingOnPause = false;

	//��������� ������ �������� � ��������� �������������
	mReadBuffer = QSharedPointer<FrameBuffer>(new FrameBuffer());
	mOutputBuffer = QSharedPointer<FrameBuffer>(new FrameBuffer());
	mFaceBuffer = QSharedPointer<FaceBuffer>(new FaceBuffer());

	mReadMutex = QSharedPointer<QMutex>(new QMutex());
	mReadWaitCondition = QSharedPointer<QWaitCondition>(new QWaitCondition());
	mThreadSemaphore = QSharedPointer<QSemaphore>(new QSemaphore);
	
	//������ �������������� �������
	for (int irun = 0; irun < GlobalOptions::THREAD_COUNT; irun++)
	{
		startProcessingThread();
	}

	//����� �������� ��� ������ � ������������
	mDecoderRunnable = new DecoderRunnable(
		mReadBuffer, 
		mOutputBuffer, 
		mVideoCapture, 
		mReadMutex, 
		mReadWaitCondition, 
		mThreadSemaphore);
	mThreadPool->start(mDecoderRunnable);
}
/*
������ � �������� ������ ������ ���������
*/
void qtFRSMainWindow::startProcessingThread()
{
	FrameProcessingRunnable* processRunnable = new FrameProcessingRunnable(
		mReadBuffer, 
		mOutputBuffer, 
		mFaceBuffer, 
		mRecognizer, 
		mReadMutex, 
		mReadWaitCondition, 
		mThreadSemaphore);
	mProcessingRunnables.push_back(processRunnable);

	connect(processRunnable, &FrameProcessingRunnable::appendTextToMainWindowLog, this, &qtFRSMainWindow::appendTextToLog);

	mThreadPool->start(mProcessingRunnables.back());
	appendTextToLog(tr("Main Window: Frame processing thread started."));
}

/*
������ ��������� �����
*/
void qtFRSMainWindow::startVideoStream()
{
	ui.startVideo->setEnabled(false);

	//���������� � ��������� (�������� ���� ����������� ��������), ���� ��� �� ������ ����������
	mVideoCapture = QSharedPointer<cv::VideoCapture>(new cv::VideoCapture());

	//�������������� ����������
	mVideoCapture.create();

	// �������� ��������� ��������� ����������� � ��������
	try
	{
		std::stringstream ss(OPTIONS["Global"]["ThreadDecoderInput"].string_data);

		int i;
		if ((ss >> i).fail() || !(ss >> std::ws).eof())
		{
			throw std::bad_cast();
		}

		mVideoCapture->open(stoi(OPTIONS["Global"]["ThreadDecoderInput"].string_data));
	}
	catch (...)
	{
		mVideoCapture->open(OPTIONS["Global"]["ThreadDecoderInput"].string_data);
	}	

	//�������� FPS � �������� ����� �������
	if (OPTIONS["Global"]["GetFPS"].bool_data)
	{
		GlobalOptions::FPS = mVideoCapture->get(CV_CAP_PROP_FPS);
	}
	else
	{
		GlobalOptions::FPS = OPTIONS["Global"]["FPS"].float_data;
	}
	GlobalOptions::FRAME_PERIOD = 1 / GlobalOptions::FPS;
	
	//������� ��� ��������������� ��������� �����������
	Dog* dogFilter = new Dog();
	mFilters.push_back(QSharedPointer<AbstractFilter>(dogFilter));
	//LPRSR* retinexFilter = new LPRSR();
	//TangTriggs* tangTriggs = new TangTriggs();
	//DftFilter* dftFilter = new DftFilter();
	//LogGaborDecomposition* logGaborFilter = new LogGaborDecomposition(cv::Size(150, 150), 1, 8, 8, 2.1, 0.5, 1.1);
	//filters.push_back(retinexFilter);
	//filters.push_back(tangTriggs);
	//filters.push_back(dftFilter);
	//filters.push_back(logGaborFilter);

	//������ ��� ���������� ��������� �� ����������� ����
	STDFeatureExtrator* stdExtractor = new STDFeatureExtrator(10, 10, 2, 2);
	mExtractors.push_back(QSharedPointer<AbstractExtractor>(stdExtractor));

	//���������� ����������� ��� (��������� ��������� � ���������� ���������)
	FaceImageProcessor* faceProcessor = new FaceImageProcessor(mFilters, mExtractors);
	mFaceProcessors.push_back(QSharedPointer<FaceImageProcessor>(faceProcessor));

	//�������� ��� �� ������ ����������� �������
	mFaceDetector = QSharedPointer<FaceDetector>(new FaceDetector(OPTIONS["Global"]["MinFaceDetectSize"].int_data, OPTIONS["Global"]["FaceDetectorCascadeFile"].string_data));

	//�������� ����������� ��������� ������
	FaceModeller modeller(20, 20, FM_8LIMIT_ROTATIONS, 0);
	if (OPTIONS["Global"]["CreateRotated"].bool_data)
	{
		modeller.createRMImagesInFolder(OPTIONS["Global"]["BiovectorBaseFolder"].string_data, *mFaceDetector, modeller);
	}

	//��������/������ �����������
	openBiovectorOptions();
	
	//������������� ��� �� ������ ������ ��������� �������
	kNNClassifier* knn = new kNNClassifier(mBiovectorBase, OPTIONS["Global"]["NearNeighboursCount"].int_data, OPTIONS["Global"]["RecognizerThreshold"].float_data);
	mClassifiers.push_back(QSharedPointer<AbstractClassifier>(knn));

	//�������������� ���
	mRecognizer = QSharedPointer<Recognizer>(new Recognizer(mBiovectorBase, mClassifiers, mFaceProcessors, mDimensionReductor));

	//������/���������� ��������� �����������
	startThreadsProcessing();

	mVideoTimer->start((int)(GlobalOptions::FRAME_PERIOD * 1000));

	ui.pauseVideo->setEnabled(true);
	ui.stopVideo->setEnabled(true);
	videoStreaming = true;
	appendTextToLog(tr("Main Window: Video processing started."));
}

/*
������������ ��������� �����
*/
void qtFRSMainWindow::pauseVideoStream()
{
	if (!processingOnPause)
	{
		mVideoTimer->stop();
		ui.pauseVideo->setText(tr("Continue"));
		processingOnPause = true;
	}
	else
	{
		mVideoTimer->start((int)(GlobalOptions::FRAME_PERIOD * 1000));
		ui.pauseVideo->setText(tr("Pause"));
		processingOnPause = false;
	}
}

/*
��������� ��������� �����
*/
void qtFRSMainWindow::stopVideoStream()
{
	videoStreaming = false;
	ui.pauseVideo->setEnabled(false);
	ui.stopVideo->setEnabled(false);

	//������������� ������ ������
	mVideoTimer->stop();

	//������������� ������
	mDecoderRunnable->setToFinish();
	for (int irun = 0; irun < mProcessingRunnables.size(); irun++)
		mProcessingRunnables[irun]->setToFinish();

	while (mThreadSemaphore->available() > 0)
		QThread::msleep((int)(100));

	appendTextToLog(tr("Main Window: Decoder and Frame processing threads finished."));
	mProcessingRunnables.clear();

	//������������� �����
	mVideoCapture->release();
	emit videoStreamStop();

	//������� ������� � ������
	mReadBuffer.clear();
	mOutputBuffer.clear();
	mFaceBuffer.clear();

	mRecognizer.clear();

	for (int i = 0; i < mClassifiers.size(); i++)
		mClassifiers[i].clear();
	mClassifiers.clear();

	mFaceDetector.clear();

	for (int i = 0; i < mFaceProcessors.size(); i++)
		mFaceProcessors[i].clear();
	mFaceProcessors.clear();

	for (int i = 0; i < mExtractors.size(); i++)
		mExtractors[i].clear();
	mExtractors.clear();

	for (int i = 0; i < mFilters.size(); i++)
		mFilters[i].clear();
	mFilters.clear();

	mVideoCapture.clear();
	appendTextToLog(tr("Main Window: Video processing stopped; it is safe to configure settings and restart."));

	ui.startVideo->setEnabled(true);
}

/*
����� ����� � ������� videoOutLabel
*/
void qtFRSMainWindow::showFrame(QImage &img)
{
	ui.videoOutLabel->setPixmap(QPixmap::fromImage(img));
}

/*
������� ������� videoOutLabel
*/
void qtFRSMainWindow::clearFrame()
{
	ui.videoOutLabel->setPixmap(QPixmap(1280, 720));
}

/*
��������� ����� ���������� � ������
����� ������ ����� ����� �� ������ � ����� � �����
*/
void qtFRSMainWindow::processFrame()
{
	/*
	try
	{
		cv::Mat imageOPENCVorig;
		*mVideoCapture.data() >> imageOPENCVorig;

		chrono::system_clock::time_point today = chrono::system_clock::now();
		time_t tt;
		tt = chrono::system_clock::to_time_t(today);

		char curTime[80];

		strftime(curTime, 80, "%Hh_%Mm_%Ss", localtime(&tt));

		string fileName = curTime;

		if (imageOPENCVorig.empty())
		{
			QString m = "Input video stream path is empty!";
			//emit messageWindowAppear(m);
			stopVideoStream();

			return;
			}
		


		cv::Mat imageOPENCVcopy,frame_tmp;
		imageOPENCVorig.copyTo(imageOPENCVcopy);
		vector < Rect > curFacesRects;

		int numDetectedFace = _faceDetector->detect(imageOPENCVcopy);
		std::vector < cv::Rect > faceFrameRects = _faceDetector->getFacesRects();
		trk::Tracker nTracker(_faceDetector->getFacesRects());
		vector <int> curNumTr = nTracker.lastTracks.Nums;

		//��������� �����
		int facesCount = _faceDetector->detect(imageOPENCVcopy);
		curFacesRects = _faceDetector->getFacesRects();
		nTracker.tracking(curFacesRects, curNumTr);

		for (int i = 0; i < curFacesRects.size(); i++)
		{
			cv::rectangle(imageOPENCVcopy, curFacesRects[i], CV_RGB(255, 0, 0), 2, CV_AA, 0);
			cv::Mat faceImageOPENCV(imageOPENCVorig, faceFrameRects[i]);
			cv::resize(faceImageOPENCV, faceImageOPENCV, cv::Size(50, 50));
			cv::cvtColor(faceImageOPENCV, faceImageOPENCV, cv::COLOR_BGR2RGB);
			QImage faceImageQT((uchar*)faceImageOPENCV.data, faceImageOPENCV.cols, faceImageOPENCV.rows, faceImageOPENCV.step, QImage::Format_RGB888);

			//��������� ������
			char text_buf[32];
			//����� � ������
			itoa(curNumTr[i], text_buf, 10);
			//����� ������
			putText(imageOPENCVcopy, text_buf, cvPoint(curFacesRects[i].x, curFacesRects[i].y), CV_FONT_HERSHEY_COMPLEX, 1.0, cvScalar(0, 0, 255));

			cv::imwrite("E:\\rec2701\\" + fileName + "f.png", faceImageOPENCV);
			cv::imwrite("E:\\rec2701\\" + fileName + ".png", imageOPENCVorig);

			emit detectFace(faceImageQT);
		}

		cv::cvtColor(imageOPENCVcopy, imageOPENCVcopy, cv::COLOR_BGR2RGB);

		QImage imageQT((uchar*)imageOPENCVcopy.data, imageOPENCVcopy.cols, imageOPENCVcopy.rows, imageOPENCVcopy.step, QImage::Format_RGB888);

		emit frameReady(imageQT);
	}
	catch (std::exception& ex)
	{
		QString m = ex.what();
		//emit messageWindowAppear(m);

		stopVideoStream();

		return;
	}
	*/
	
	//����� ������� ����� � ������ ������
	cv::Mat frameImageCV;
	if (mOutputBuffer->size() > 0)
	{
		//��������� ����� �� ������
		FrameStruct frameStruct = mOutputBuffer->GetFrame();
		frameImageCV = frameStruct.frameMat;
		cv::cvtColor(frameImageCV, frameImageCV, cv::COLOR_BGR2RGB);
		QImage imageQT((uchar*)frameImageCV.data, frameImageCV.cols, frameImageCV.rows, frameImageCV.step, QImage::Format_RGB888);

		//������ �� ����� �����
		emit frameReady(imageQT);
	}

	//����� ��� � ������� ����� � ������ ���
	FaceStruct faceStruct;
	std::list<FaceStruct> frameFaceList;
	if (mFaceBuffer->generalSize() > 0)
	{
		frameFaceList = mFaceBuffer->GetFrameFaceList();
		while (frameFaceList.size() > 0)
		{
			faceStruct = frameFaceList.front();
			frameFaceList.pop_front();
			
			cv::Mat faceImageOPENCV = faceStruct.faceMat;
			cv::resize(faceImageOPENCV, faceImageOPENCV, cv::Size(50, 50));
			cv::cvtColor(faceImageOPENCV, faceImageOPENCV, cv::COLOR_BGR2RGB);
			QImage faceImageQT((uchar*)faceImageOPENCV.data, faceImageOPENCV.cols, faceImageOPENCV.rows, faceImageOPENCV.step, QImage::Format_RGB888);
			
			cv::Mat recPersonFaceImg;
			QString recPersonName;

			if (faceStruct.isRecognized())
			{
				recPersonFaceImg = imread(faceStruct.personData.imagePath, CV_LOAD_IMAGE_GRAYSCALE);
				recPersonName = mBiovectorBase->personNameCluster[faceStruct.personData.personId];
				emit recogFace(faceImageQT, recPersonName);
			}
			else
			{
				recPersonFaceImg = imread("Unrecognized.jpg", CV_LOAD_IMAGE_GRAYSCALE);
				//recPersonName = "Unrecognized Person";
			}
			cv::resize(recPersonFaceImg, recPersonFaceImg, cv::Size(50, 50));
			cv::cvtColor(recPersonFaceImg, recPersonFaceImg, cv::COLOR_GRAY2RGB);
			QImage recPersonImageQT((uchar*)recPersonFaceImg.data, recPersonFaceImg.cols, recPersonFaceImg.rows, recPersonFaceImg.step, QImage::Format_RGB888);

			emit detectFace(faceImageQT, recPersonImageQT);
			
		}
	}

	//������������ ���������� ����������� ���
	while (mFaceBuffer->framesCount() > GlobalOptions::OUTPUT_FRAME_LIMIT)
	{
		mFaceBuffer->RemoveFirstFrame();
	}
}


//����������� ������������� � ������������ ��� � ������ "�������"
void qtFRSMainWindow::addScrollAreaImage(QImage &img, QImage &recImg)
{
	QLabel* labelImage = new QLabel();
	QLabel* labelImage2 = new QLabel();
	labelImage->setPixmap(QPixmap::fromImage(img));
	labelImage2->setPixmap(QPixmap::fromImage(recImg));

	_viewScrollAreaDetectFacesList->push_front(labelImage);
	_viewScrollAreaDetectFaceImagesLayout->insertWidget(0, labelImage);

	_viewScrollAreaRecognizeFacesList->push_front(labelImage2);
	_viewScrollAreaRecognizeFaceImagesLayout->insertWidget(0, labelImage2);

	_viewScrollAreaFaceImagesLayout->removeWidget(_viewScrollAreaDetectFacesList->last());
	delete _viewScrollAreaDetectFacesList->takeLast();

	_viewScrollAreaRecognizeFaceImagesLayout->removeWidget(_viewScrollAreaRecognizeFacesList->last());
	delete _viewScrollAreaRecognizeFacesList->takeLast();
}

//����������� ���������� � ����������(���, ����, �����) ������������ � �������������� ������ � ������ "�������"

void qtFRSMainWindow::addPersonScrollArea(QImage &recImg, QString &personName)
{
	QLabel* labelImage2 = new QLabel();
	QLabel* labelTimer = new QLabel();

	labelImage2->setPixmap(QPixmap::fromImage(recImg));
	labelTimer->setText(personName +"\n" + QTime::currentTime().toString("hh:mm:ss ") + QDate::currentDate().toString("dd.MM.yyyy"));

	_viewPersonScrollAreaRecognizeList->push_front(labelImage2);
	_viewPersonScrollAreaRecognizeLayout->insertWidget(0, labelImage2);

	_viewPersonScrollAreaInfList->push_front(labelTimer);
	_viewPersonScrollAreaInfLayout->insertWidget(0, labelTimer);

	_viewPersonScrollAreaLayout->removeWidget(_viewPersonScrollAreaRecognizeList->last());
	delete _viewPersonScrollAreaRecognizeList->takeLast();
	
	_viewPersonScrollAreaInfLayout->removeWidget(_viewPersonScrollAreaInfList->last());
	delete _viewPersonScrollAreaInfList->takeLast();
}

/*
�������� ��������� ����������� ������
*/
void qtFRSMainWindow::toggleChooseConfigComboBox(bool state)
{
	ui.chooseConfigComboBox->setEnabled(state);
	ui.configOpenButton->setEnabled(state);
}

/*
�������� ��������� ������ ����������
*/
void qtFRSMainWindow::toggleVideoControl(bool state)
{
	ui.startVideo->setEnabled(state);
	ui.pauseVideo->setEnabled(false);
	ui.stopVideo->setEnabled(false);
}

/*
��������� ���������� ������ ������������
*/
void qtFRSMainWindow::updateConfigurationList()
{
	//������� ������
	ui.chooseConfigComboBox->blockSignals(true);
	ui.chooseConfigComboBox->clear();
	ui.chooseConfigComboBox->addItems(GlobalOptions::CONFIG_NAMES);
	ui.chooseConfigComboBox->blockSignals(false);

	//�������� ������ �� ������ �������
	int index = GlobalOptions::CONFIG_KEYS.indexOf(GlobalOptions::CURRENT_KEY);
	ui.chooseConfigComboBox->setCurrentIndex(index);
}

/*
���� ���� �������� ��������� � ����� �� ��������� � ������ ���������� ���������
*/
void qtFRSMainWindow::searchConfigManager()
{
	if (QFile(GlobalOptions::CONFIG_MANAGER_PATH).exists())
	{
		getReady(true);
	}
	else
	{
		//���� �������� �� ������, �������� � ������������� ��������� ���� ��������
		QDir dir(GlobalOptions::CONFIG_MANAGER_FOLDER);
		if (!dir.exists())
			dir.mkpath(".");
		QMessageBox(
			QMessageBox::Warning,
			QObject::tr("Configuration manager data not found!"),
			QObject::tr("Please use \"Load Manager Data...\" option in the Configuration manager to specify the path to FRSConfigManagerData.conf. The file will be copied into the default configuration folder. In case no file is provided, it will be created automatically upon using \"New Configuration...\" option."))
			.exec();
		openConfigManager();
	}
}

/*
����� ������������
*/
void qtFRSMainWindow::configComboBoxChanged(const int index)
{
	if (index != -1)
	{
		QSettings *mSettings = new QSettings(GlobalOptions::CONFIG_MANAGER_PATH, QSettings::IniFormat);
		QString currentKey = GlobalOptions::CONFIG_KEYS[index];
		mSettings->setValue("manager/currentKey", currentKey);

		getReady(false);
	}
}

/*
����� ������ � ���
*/
void qtFRSMainWindow::appendTextToLog(QString &text)
{
	QMutexLocker logLocker(&mLogMutex);
	ui.logPlainText->appendPlainText(text);
	ui.logPlainText->verticalScrollBar()->setValue(ui.logPlainText->verticalScrollBar()->maximum());
}

/*
���������� ����������� ������, �������� �� ������������ ������������
*/
void qtFRSMainWindow::getReady(bool updateList)
{
	if (GlobalOptions::loadConfigManager(GlobalOptions::CONFIG_MANAGER_PATH, true))
	{
		toggleChooseConfigComboBox(true);
		if (updateList)
		{
			updateConfigurationList();
		}
		if (GlobalOptions::loadCurrentConfiguration(GlobalOptions::CURRENT_PATH, true))
		{
			toggleVideoControl(true);
		}
		else
		{
			toggleVideoControl(false);
		}
	}
	else
	{
		toggleChooseConfigComboBox(false);
		toggleVideoControl(false);
	}
}

