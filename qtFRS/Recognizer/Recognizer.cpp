#include "Recognizer.h"


Recognizer::Recognizer(
	QSharedPointer<BiovectorBase> base,
	std::vector<QSharedPointer<AbstractClassifier>> &classif,
	std::vector<QSharedPointer<FaceImageProcessor>> &faceProcs,
	QSharedPointer<AbstractDimensionReduction> dimReductor)
{
	mBiovectorBase = base;
	mClassifiers = classif;
	mFaceProcessors = faceProcs;
	mDimensionReductor = dimReductor;
}

Recognizer::~Recognizer()
{

}

void Recognizer::recognize(cv::Mat &faceImage, std::vector < cv::Point2f > &facePoints, PersonData &rcnPersonData)
{
	PersonData curPerson;
	rcnPersonData = curPerson;

	Biovector faceBiovector;
	for (auto &faceProc : mFaceProcessors)
	{
		faceProc->processFace(faceImage, facePoints, faceBiovector);
	}

	if (OPTIONS["Global"]["ReduceDimensions"].bool_data)
	{
		faceBiovector = mDimensionReductor->reduce(faceBiovector);
	}


	std::vector < int > accumClassify(mBiovectorBase->getClustersCnt(), 0);

	for (auto &curClassifier : mClassifiers)
	{
		curClassifier->classify(faceBiovector);
		curPerson = curClassifier->getClassifiedPerson();
		if (!curPerson.imagePath.empty())
			++accumClassify[curPerson.personId];
	}

	if (*std::max_element(accumClassify.begin(), accumClassify.end()) != 0)
	{
		int resultPersonId = (std::max_element(accumClassify.begin(), accumClassify.end()) - accumClassify.begin());
		rcnPersonData = mBiovectorBase->getCurPerson(resultPersonId, 0);
	}
}