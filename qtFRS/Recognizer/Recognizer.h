#ifndef RECOGNIZER_H
#define RECOGNIZER_H

#include <opencv2\core.hpp>
#include <opencv2\ml.hpp>
#include "AbstractDistance.h"
#include "AbstractClassifier.h"
#include "AbstractDimensionReduction.h"
#include "BiovectorBase.h"

class Recognizer
{
public:
	Recognizer();
	Recognizer(
		QSharedPointer<BiovectorBase> base, 
		std::vector<QSharedPointer<AbstractClassifier>> &classif, 
		std::vector<QSharedPointer<FaceImageProcessor>> &faceProcs, 
		QSharedPointer<AbstractDimensionReduction> dimReductor);

	~Recognizer();

	void recognize(cv::Mat &faceImage, std::vector < cv::Point2f > &facePoints, PersonData &rcnPersonData);

private:
	std::vector<QSharedPointer<AbstractClassifier>> mClassifiers;
	std::vector<QSharedPointer<FaceImageProcessor>> mFaceProcessors;
	
	QSharedPointer<BiovectorBase> mBiovectorBase;
	QSharedPointer<AbstractDimensionReduction> mDimensionReductor;
};

#endif