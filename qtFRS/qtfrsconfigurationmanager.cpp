#include "qtfrsconfigurationmanager.h"
#include "ui_qtfrsconfigurationmanager.h"


qtFRSConfigurationManager::qtFRSConfigurationManager(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::qtFRSConfigurationManager)
{
	ui->setupUi(this);

	//�������� ������� � ����� ��� ��������
	QTabWidget *configTabWidget = new QTabWidget();
	ui->configSettingsBox->layout()->addWidget(configTabWidget);
	configTabWidget->setObjectName(QString("configTabWidget"));

	ui->configSettingsBox->layout()->removeWidget(ui->configSettingsButtonBox);
	ui->configSettingsBox->layout()->addWidget(ui->configSettingsButtonBox);

	foreach(const GroupPair groupPair, OPTIONS_META)
	{
		//�������� ����� � ��� ������� ������ �������������� ���� ������ ����
		QWidget *form1 = new QWidget;
		QFormLayout *formLayout1 = new QFormLayout;
		form1->setLayout(formLayout1);
		QWidget *form2 = new QWidget;
		QFormLayout *formLayout2 = new QFormLayout;
		form2->setLayout(formLayout2);

		QHBoxLayout *configsLayout = new QHBoxLayout;
		configsLayout->addWidget(form1);
		configsLayout->addWidget(form2);

		QWidget *scrollAreaContent = new QWidget;
		QScrollArea *scrollArea = new QScrollArea;
		scrollArea->setWidgetResizable(true);
		scrollArea->setWidget(scrollAreaContent);
		scrollAreaContent->setLayout(configsLayout);

		QWidget *tab = new QWidget;
		tab->setLayout(new QVBoxLayout);
		tab->layout()->addWidget(scrollArea);

		configTabWidget->addTab(tab, groupPair.first);
		configTabWidget->setCurrentWidget(tab);

		int ind = 0;
		foreach(const KeyPair keyPair, groupPair.second)
		{
			switch (keyPair.second.type) {
			case ParameterType::Bool:
			{
				QCheckBox *checkboxInput = new QCheckBox(); 
				if (ind++ <= groupPair.second.count() * 0.5)
					formLayout1->addRow(keyPair.second.name, checkboxInput);
				else
					formLayout2->addRow(keyPair.second.name, checkboxInput);
				formFields[groupPair.first + "/" + keyPair.first] = checkboxInput;
				break; 
			}
			case ParameterType::String:
			case ParameterType::Int:
			case ParameterType::Float:
			default:
			{
				QLineEdit *lineInput = new QLineEdit();
				if (ind++ <= groupPair.second.count() * 0.5)
					formLayout1->addRow(keyPair.second.name, lineInput);
				else
					formLayout2->addRow(keyPair.second.name, lineInput);
				formFields[groupPair.first + "/" + keyPair.first] = lineInput;
				break;
			}
			}
		}
	}
	
	//�������� ���� ������������
	if (GlobalOptions::loadConfigManager(GlobalOptions::CONFIG_MANAGER_PATH, false))
	{
		//������� ������ ������������ � ������� �������� ��������
		GlobalOptions::loadCurrentConfiguration(GlobalOptions::CURRENT_PATH); 
		initConfigurationManagement();
	}
	else
	{
		//�������� �� ��� ��������, ������� ���� � ������ �����������
		disableConfigurationManagement();
	}

	//��������� ����� ��� ������
	connect(ui->closeButton, &QPushButton::clicked,
		this, &qtFRSConfigurationManager::closeWindow);

	connect(ui->configNewButton, &QPushButton::clicked,
		this, &qtFRSConfigurationManager::saveNewConfiguration);
	connect(ui->configLoadButton, &QPushButton::clicked,
		this, &qtFRSConfigurationManager::loadConfiguration);
	connect(ui->loadManagerDataButton, &QPushButton::clicked,
		this, &qtFRSConfigurationManager::loadManagerData);
	connect(ui->configDeleteButton, &QPushButton::clicked,
		this, &qtFRSConfigurationManager::deleteConfiguration);

	connect(ui->configSaveButton, &QPushButton::clicked,
		this, &qtFRSConfigurationManager::saveConfiguration);
	connect(ui->configDiscardButton, &QPushButton::clicked,
		this, &qtFRSConfigurationManager::discardChanges);

	connect(ui->configDefaultButton, &QPushButton::clicked,
		this, &qtFRSConfigurationManager::setDefaultConfiguration);
	connect(ui->configResetButton, &QPushButton::clicked,
		this, &qtFRSConfigurationManager::resetValuesToDefault);

	connect(ui->configComboBox, SIGNAL(currentIndexChanged(const int)),
		this, SLOT(configComboBoxChanged(const int)));
}

/*
�������� ����
*/
void qtFRSConfigurationManager::closeWindow()
{
	close();
}

/*
�������� ����� ������ ������������
*/
void qtFRSConfigurationManager::saveNewConfiguration()
{
	//�������� ����� � �������������� � ��������� ������ ��� �����	
	QDir dir(GlobalOptions::CONFIG_MANAGER_FOLDER);
	if (!dir.exists())
		dir.mkpath(".");
	QString suggestedPath = GlobalOptions::CONFIG_MANAGER_FOLDER + "/Configuration__" + QTime::currentTime().toString("hh.mm") + "_" + QDate::currentDate().toString("dd.MM.yyyy") + ".conf";
	QString configPath = QFileDialog::getSaveFileName(0, tr("New configuration..."), suggestedPath, tr("Configuration File (*.conf)"));
	
	//�������� �� ���� � ����������
	if (configPath.isEmpty())
		return;
	QFileInfo fileInfo(configPath);
	if (fileInfo.suffix().isEmpty())
		configPath += ".conf";
	fileInfo = QFileInfo(configPath);
	
	//������� ���� ��������� ������������
	int currentID;
	QString configKey;
	QSettings *settings = new QSettings(GlobalOptions::CONFIG_MANAGER_PATH, QSettings::IniFormat);
	currentID = settings->value("manager/currentID", 0).toInt();
	configKey = QString::fromStdString("config" + std::to_string(currentID));

	//���� ����� ���� �������������� ������, ��������������� ������ � ��������� ����������������
	if (GlobalOptions::MANAGER_STATE != ManagerState::NotLoaded 
		&& GlobalOptions::CONFIG_PATHS.contains(configPath))
	{
		configKey = GlobalOptions::CONFIG_KEYS[GlobalOptions::CONFIG_PATHS.indexOf(configPath)];
	}
	else
	{
		currentID++;
		settings->setValue("manager/currentID", currentID);
		configKey = QString::fromStdString("config" + std::to_string(currentID));
	}
	
	//���� ���� ����� ����������� ���, ��������� � ����� ������� � ����� ���������
	QString configName = fileInfo.completeBaseName();
	if (GlobalOptions::MANAGER_STATE != ManagerState::NotLoaded 
		&& !GlobalOptions::CONFIG_PATHS.contains(configPath) && GlobalOptions::CONFIG_NAMES.contains(fileInfo.completeBaseName()))
	{
		configName += "_copy";

		QMessageBox(
			QMessageBox::Information,
			QObject::tr("Specified name already exists."),
			QObject::tr("New configuration has been saved as ") + configName)
			.exec();
	}
	
	//�������� ������ � ����� ������������
	QDir root(".");
	settings->setValue(configKey + "/name", configName);
	settings->setValue(configKey + "/path", root.relativeFilePath(configPath));
	settings->sync();

	//�������� ���� ����� ������������ � �������� ���������
	QSettings *cSettings = new QSettings(configPath, QSettings::IniFormat);
	if (GlobalOptions::MANAGER_STATE != ManagerState::NotLoaded && GlobalOptions::CURRENT_DEFAULT_KEY != configKey)
	{
		QSettings *dSettings = new QSettings(GlobalOptions::CURRENT_DEFAULT_PATH, QSettings::IniFormat);
		foreach(const GroupPair groupPair, OPTIONS_META)
		{
			foreach(const KeyPair keyPair, groupPair.second)
			{
				QString dValue = dSettings->value(groupPair.first + "/" + keyPair.first, "").toString();
				cSettings->setValue(groupPair.first + "/" + keyPair.first, dValue);
			}
		}
	}
	else
	{
		foreach(const GroupPair groupPair, OPTIONS_META)
		{
			foreach(const KeyPair keyPair, groupPair.second)
			{
				cSettings->setValue(groupPair.first + "/" + keyPair.first, "");
			}
		}
	}
	cSettings->sync();

	//���� �����, �������� ���� ��������� � ���������� ���������� � ����� �������
	if (GlobalOptions::MANAGER_STATE == ManagerState::NotLoaded)
	{
		settings->setValue("manager/currentID", currentID);
		settings->setValue("manager/currentKey", configKey);
		settings->setValue("manager/defaultKey", configKey);
		settings->setValue("manager/managerKey", configKey);
		settings->sync();
	}

	if (GlobalOptions::loadConfigManager(GlobalOptions::CONFIG_MANAGER_PATH, true))
	{
		initConfigurationManagement();
	}
	else
	{
		disableConfigurationManagement();
	}
}

/*
�������� ������������ �� �����
*/
void qtFRSConfigurationManager::loadConfiguration()
{
	//�������� ����� � �������������� � ��������� ������� ����	
	QDir dir(GlobalOptions::CONFIG_MANAGER_FOLDER);
	if (!dir.exists())
		dir.mkpath(".");
	QString suggestedPath = GlobalOptions::CONFIG_MANAGER_FOLDER;
	QString configPath = QFileDialog::getOpenFileName(0, tr("Load configuration..."), suggestedPath, tr("Configuration File (*.conf)"));

	//�������� ������������
	QFileInfo fileInfo(configPath);
	qDebug() << fileInfo.suffix();
	if (!QFile(configPath).exists() || fileInfo.suffix() != "conf")
		return;

	if (!GlobalOptions::ensureConfigurationValid(configPath, false))
		return;

	//������� ���� ��������� ������������
	int currentID;
	QString configKey;
	QSettings *settings = new QSettings(GlobalOptions::CONFIG_MANAGER_PATH, QSettings::IniFormat);
	currentID = settings->value("manager/currentID", 0).toInt();
	configKey = QString::fromStdString("config" + std::to_string(currentID));

	//���� ����� ���� �������������� ������, ��������������� ������ � ��������� ����������������
	if (GlobalOptions::MANAGER_STATE != ManagerState::NotLoaded
		&& GlobalOptions::CONFIG_PATHS.contains(configPath))
	{
		configKey = GlobalOptions::CONFIG_KEYS[GlobalOptions::CONFIG_PATHS.indexOf(configPath)];
	}
	else
	{
		currentID++;
		settings->setValue("manager/currentID", currentID);
		configKey = QString::fromStdString("config" + std::to_string(currentID));
	}

	//���� ���� ����� ����������� ���, ��������� � ����� ������� � ����� ���������
	QString configName = fileInfo.completeBaseName();
	if (GlobalOptions::MANAGER_STATE != ManagerState::NotLoaded
		&& !GlobalOptions::CONFIG_PATHS.contains(configPath) && GlobalOptions::CONFIG_NAMES.contains(fileInfo.completeBaseName()))
	{
		configName += "_copy";

		QMessageBox(
			QMessageBox::Information,
			QObject::tr("Specified name already exists."),
			QObject::tr("New configuration has been saved as ") + configName)
			.exec();
	}

	//�������� ������ � ����� ������������
	QDir root(".");
	settings->setValue(configKey + "/name", configName);
	settings->setValue(configKey + "/path", root.relativeFilePath(configPath));
	settings->sync();

	//���� �����, �������� ���� ��������� � ���������� ���������� � ����� �������
	if (GlobalOptions::MANAGER_STATE == ManagerState::NotLoaded)
	{
		settings->setValue("manager/currentID", currentID);
		settings->setValue("manager/currentKey", configKey);
		settings->setValue("manager/defaultKey", configKey);
		settings->setValue("manager/managerKey", configKey);
		settings->sync();
	}

	if (GlobalOptions::loadConfigManager(GlobalOptions::CONFIG_MANAGER_PATH, true))
	{
		initConfigurationManagement();
	}
	else
	{
		disableConfigurationManagement();
	}
}

/*
�������� ���� ��������� ������������, ������� ������ ��������� ������ ������������ ������������
*/
void qtFRSConfigurationManager::loadManagerData()
{
	QDir dir(GlobalOptions::CONFIG_MANAGER_FOLDER);
	if (!dir.exists())
		dir.mkpath(".");
	QString dataFilename = QFileDialog::getOpenFileName(0, tr("Load manager data..."), GlobalOptions::CONFIG_MANAGER_PATH, tr("FRSConfigManagerData.conf"));
	if (dataFilename.isEmpty())
		return;

	if (GlobalOptions::loadConfigManager(dataFilename, true))
	{
		initConfigurationManagement();
	}
	else
	{
		disableConfigurationManagement();
	}		
}

/*
���������� �������� �� ����� � ������� ������������
*/
void qtFRSConfigurationManager::saveConfiguration()
{
	//������� ���� ��������� ������������
	QSettings *cSettings = new QSettings(GlobalOptions::CURRENT_MANAGER_PATH, QSettings::IniFormat);

	//�������� ���� �������� �� ������������
	foreach(const GroupPair groupPair, OPTIONS_META)
	{
		foreach(const KeyPair keyPair, groupPair.second)
		{
			QString value = "";
			switch (keyPair.second.type) {
			case ParameterType::Int: 
			{
				value = ((QLineEdit*)formFields[groupPair.first + "/" + keyPair.first])->text();
				try {
					//��������� ������ �� �������, �� ������� �����, �� �������� � �������
					if (value.toStdString().empty())
						throw 1;
					errno = 0; char *temp;
					long value_tol = strtol(value.toStdString().c_str(), &temp, 0);
					if (temp == value.toStdString().c_str() || *temp != '\0' || ((value_tol == LONG_MIN || value_tol == LONG_MAX) && errno == ERANGE))
						throw 2;
					if (value_tol > keyPair.second.max)
						throw 3;
					if (value_tol < keyPair.second.min)
						throw 4;
				}
				catch (int error)
				{
					QString errorMessage;
					switch (error) {
					case 1:
						errorMessage = QObject::tr("Required parameter is empty: ");
						break;
					case 2:
						errorMessage = QObject::tr("Failed to parse integer parameter: ");
						break;
					case 3:
						errorMessage = QObject::tr("Parameter exceeds maximum value: ");
						break;
					case 4:
						errorMessage = QObject::tr("Parameter is less than minimum value: ");
						break;
					}
					QMessageBox(
						QMessageBox::Critical,
						QObject::tr("Input error"),
						errorMessage + keyPair.second.name)
						.exec();
					return;
				}
				break; 
			}
			case ParameterType::Float: 
			{
				value = ((QLineEdit*)formFields[groupPair.first + "/" + keyPair.first])->text();
				try {
					//��������� ������ �� �������, �� ������� �����, �� �������� � �������
					if (value.toStdString().empty())
						throw 1;
					errno = 0; char *temp;
					float value_tof = strtof(value.toStdString().c_str(), &temp);
					if (temp == value.toStdString().c_str() || *temp != '\0' || errno == ERANGE)
						throw 2;
					if (value_tof > keyPair.second.max)
						throw 3;
					if (value_tof < keyPair.second.min)
						throw 4;
				}
				catch (int error)
				{
					QString errorMessage;
					switch (error) {
					case 1:
						errorMessage = QObject::tr("Required parameter is empty: ");
						break;
					case 2:
						errorMessage = QObject::tr("Failed to parse floating point parameter: ");
						break;
					case 3:
						errorMessage = QObject::tr("Parameter exceeds maximum value: ");
						break;
					case 4:
						errorMessage = QObject::tr("Parameter is less than minimum value: ");
						break;
					}
					QMessageBox(
						QMessageBox::Critical,
						QObject::tr("Input error"),
						errorMessage + keyPair.second.name)
						.exec();
					return;
				}
				break; 
			}
			case ParameterType::String: 
			{
				value = ((QLineEdit*)formFields[groupPair.first + "/" + keyPair.first])->text();
				try {
					//��������� ������ �� �������
					if (value.toStdString().empty())
						throw 1;
				}
				catch (int error)
				{
					QString errorMessage;
					switch (error) {
					case 1:
						errorMessage = QObject::tr("Required parameter is empty: ");
						break;
					}
					QMessageBox(
						QMessageBox::Critical,
						QObject::tr("Input error"),
						errorMessage + keyPair.second.name)
						.exec();
					return;
				}
				break; 
			}
			case ParameterType::Bool:
			default:
				break;
			}
		}
	}

	//�������� ��������
	foreach(const GroupPair groupPair, OPTIONS_META)
	{
		foreach(const KeyPair keyPair, groupPair.second)
		{
			QString value = "";
			switch (keyPair.second.type) {
			case ParameterType::Int: 
			{
				//��� ��� ������ ����� � ��������� � ������ ��� ����������
				value = ((QLineEdit*)formFields[groupPair.first + "/" + keyPair.first])->text();
				char *temp;
				long value_tol = strtol(value.toStdString().c_str(), &temp, 0);
				value = QString::fromStdString(std::to_string(value_tol));
				break; 
			}
			case ParameterType::Float: 
			{
				value = ((QLineEdit*)formFields[groupPair.first + "/" + keyPair.first])->text();
				char *temp;
				float value_tof = strtof(value.toStdString().c_str(), &temp);
				value = QString::fromStdString(std::to_string(value_tof));
				break; 
			}
			case ParameterType::Bool: 
			{
				if (((QCheckBox*)formFields[groupPair.first + "/" + keyPair.first])->checkState() == Qt::Checked)
					value = "1";
				else
					value = "0";
				break; 
			}
			case ParameterType::String:
			default: 
			{
				value = ((QLineEdit*)formFields[groupPair.first + "/" + keyPair.first])->text();
				break; }
			}
			cSettings->setValue(groupPair.first + "/" + keyPair.first, value);
		}
	}
	cSettings->sync();

	//��������� ���
	QSettings *mSettings = new QSettings(GlobalOptions::CONFIG_MANAGER_PATH, QSettings::IniFormat);
	mSettings->setValue(GlobalOptions::CURRENT_MANAGER_KEY + "/name", ui->configName->text());

	if (GlobalOptions::loadConfigManager(GlobalOptions::CONFIG_MANAGER_PATH, false))
	{
		initConfigurationManagement();
	}
	else
	{
		disableConfigurationManagement();
	}
}

/*
�������� ������������ (��� ������, ��� � �����)
*/
void qtFRSConfigurationManager::deleteConfiguration()
{
	//������� ������ � ������������ � ����� ��������� � � ������, ������� ����
	QSettings *mSettings = new QSettings(GlobalOptions::CONFIG_MANAGER_PATH, QSettings::IniFormat);
	mSettings->remove(GlobalOptions::CURRENT_MANAGER_KEY);
	QFile::remove(GlobalOptions::CURRENT_MANAGER_PATH);

	int index = GlobalOptions::CONFIG_KEYS.indexOf(GlobalOptions::CURRENT_MANAGER_KEY);
	GlobalOptions::CONFIG_KEYS.removeAt(index);
	GlobalOptions::CONFIG_NAMES.removeAt(index);
	GlobalOptions::CONFIG_PATHS.removeAt(index);

	if (!GlobalOptions::CONFIG_KEYS.empty())
	{
		mSettings->setValue("manager/managerKey", GlobalOptions::CONFIG_KEYS[0]);

		if (GlobalOptions::CURRENT_MANAGER_KEY == GlobalOptions::CURRENT_KEY)
		{
			mSettings->setValue("manager/currentKey", GlobalOptions::CONFIG_KEYS[0]);
		}
		if (GlobalOptions::CURRENT_MANAGER_KEY == GlobalOptions::CURRENT_DEFAULT_KEY)
		{
			mSettings->setValue("manager/defaultKey", GlobalOptions::CONFIG_KEYS[0]);
		}
	}
	
	//��������� ������ ������������
	if (GlobalOptions::loadConfigManager(GlobalOptions::CONFIG_MANAGER_PATH, false))
	{
		initConfigurationManagement();
	}
	else
	{
		disableConfigurationManagement();
	}
}

/*
������ ������� ������ (������������ �����)
*/
void qtFRSConfigurationManager::discardChanges()
{
	updateFormFields();
}

/*
��������� ���������� ������ ������������, ������ ����� �������������� �������� � ��������� ���� �����
*/
void qtFRSConfigurationManager::initConfigurationManagement()
{
	ui->configSettingsBox->setEnabled(true);
	ui->configDefaultButton->setEnabled(true);
	ui->configResetButton->setEnabled(true);
	ui->configDeleteButton->setEnabled(true);
	ui->configComboBox->setEnabled(true);
	
	updateConfigurationList();
	updateFormFields();
}

/*
��������� ���������� ������ ������������
*/
void qtFRSConfigurationManager::updateConfigurationList()
{
	//������� �������� � ��������� ������������
	int index = GlobalOptions::CONFIG_KEYS.indexOf(GlobalOptions::CURRENT_DEFAULT_KEY);
	GlobalOptions::CONFIG_NAMES[index] += " (default)";

	//������� ������ � ��� ��� �������� ���������� ���������
	ui->configComboBox->blockSignals(true);
	ui->configComboBox->clear();
	ui->configComboBox->addItems(GlobalOptions::CONFIG_NAMES);
	ui->configComboBox->blockSignals(false);
	GlobalOptions::loadConfigManager(GlobalOptions::CONFIG_MANAGER_PATH, false);

	//�������� ������ �� ������ �������
	index = GlobalOptions::CONFIG_KEYS.indexOf(GlobalOptions::CURRENT_MANAGER_KEY);
	ui->configComboBox->setCurrentIndex(index);
}

/*
��������� ���� ����� ��� ������ ������������
*/
void qtFRSConfigurationManager::updateFormFields()
{
	QSettings *settings = new QSettings(GlobalOptions::CURRENT_MANAGER_PATH, QSettings::IniFormat);
	QSettings *mSettings = new QSettings(GlobalOptions::CONFIG_MANAGER_PATH, QSettings::IniFormat);
	ui->configName->setText(mSettings->value(GlobalOptions::CURRENT_MANAGER_KEY + "/name").toString());
	ui->configPath->setText(mSettings->value(GlobalOptions::CURRENT_MANAGER_KEY + "/path").toString());

	foreach(const GroupPair groupPair, OPTIONS_META)
	{
		foreach(const KeyPair keyPair, groupPair.second)
		{
			switch (keyPair.second.type) {
			case ParameterType::Int: 
			{
				char *temp;
				long value_tol = strtol(settings->value(groupPair.first + "/" + keyPair.first).toString().toStdString().c_str(), &temp, 0);
				QString value = QString::fromStdString(std::to_string(value_tol));
				((QLineEdit*)formFields[groupPair.first + "/" + keyPair.first])->setText(value);
				break;
			}
			case ParameterType::Float:
			{
				char *temp;
				long value_tof = strtof(settings->value(groupPair.first + "/" + keyPair.first).toString().toStdString().c_str(), &temp);
				QString value = QString::fromStdString(std::to_string(value_tof));
				((QLineEdit*)formFields[groupPair.first + "/" + keyPair.first])->setText(value);
				break;
			}
			case ParameterType::String: 
			{
				QString value = settings->value(groupPair.first + "/" + keyPair.first).toString();
				((QLineEdit*)formFields[groupPair.first + "/" + keyPair.first])->setText(value);
				break;
			}					
			case ParameterType::Bool: 
			{
				QString value = settings->value(groupPair.first + "/" + keyPair.first).toString();
				((QCheckBox*)formFields[groupPair.first + "/" + keyPair.first])->setCheckState(value == "1" ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
				break;
			}
			default:
				break;
			}
		}
	}
}

/*
������ ����� �������������� ����������
*/
void qtFRSConfigurationManager::disableConfigurationManagement()
{
	ui->configSettingsBox->setEnabled(false);
	ui->configDefaultButton->setEnabled(false);
	ui->configResetButton->setEnabled(false);
	ui->configDeleteButton->setEnabled(false);
	ui->configComboBox->setEnabled(false);
}

/*
������������� ������������ �� ���������
*/
void qtFRSConfigurationManager::setDefaultConfiguration()
{
	QSettings *mSettings = new QSettings(GlobalOptions::CONFIG_MANAGER_PATH, QSettings::IniFormat);
	mSettings->setValue("manager/defaultKey", GlobalOptions::CURRENT_MANAGER_KEY);

	if (GlobalOptions::loadConfigManager(GlobalOptions::CONFIG_MANAGER_PATH, false))
	{
		initConfigurationManagement();
	}
	else
	{
		disableConfigurationManagement();
	}
}

/*
���������� �������� � ����� �� �������� �� ���������
*/
void qtFRSConfigurationManager::resetValuesToDefault()
{
	QSettings *settings = new QSettings(GlobalOptions::CURRENT_DEFAULT_PATH, QSettings::IniFormat);

	foreach(const GroupPair groupPair, OPTIONS_META)
	{
		foreach(const KeyPair keyPair, groupPair.second)
		{
			switch (keyPair.second.type) {
			case ParameterType::Int:
			{
				char *temp;
				long value_tol = strtol(settings->value(groupPair.first + "/" + keyPair.first).toString().toStdString().c_str(), &temp, 0);
				QString value = QString::fromStdString(std::to_string(value_tol));
				((QLineEdit*)formFields[groupPair.first + "/" + keyPair.first])->setText(value);
				break;
			}
			case ParameterType::Float:
			{
				char *temp;
				long value_tof = strtof(settings->value(groupPair.first + "/" + keyPair.first).toString().toStdString().c_str(), &temp);
				QString value = QString::fromStdString(std::to_string(value_tof));
				((QLineEdit*)formFields[groupPair.first + "/" + keyPair.first])->setText(value);
				break;
			}
			case ParameterType::String:
			{
				QString value = settings->value(groupPair.first + "/" + keyPair.first).toString();
				((QLineEdit*)formFields[groupPair.first + "/" + keyPair.first])->setText(value);
				break;
			}
			case ParameterType::Bool:
			{
				QString value = settings->value(groupPair.first + "/" + keyPair.first).toString();
				((QCheckBox*)formFields[groupPair.first + "/" + keyPair.first])->setCheckState(value == "1" ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
				break;
			}
			default:
				break;
			}
		}
	}
}

/*
��������� ����� ��� ��������� ������ ����������� ������
*/
void qtFRSConfigurationManager::configComboBoxChanged(const int index)
{
	if (index != -1)
	{
		QSettings *mSettings = new QSettings(GlobalOptions::CONFIG_MANAGER_PATH, QSettings::IniFormat);
		QString managerKey = GlobalOptions::CONFIG_KEYS[index];
		mSettings->setValue("manager/managerKey", managerKey);

		if (!GlobalOptions::loadConfigManager(GlobalOptions::CONFIG_MANAGER_PATH, true))
			disableConfigurationManagement();
		else
			updateFormFields();
	}
	
}

qtFRSConfigurationManager::~qtFRSConfigurationManager()
{
	qtFRSMainWindow::getInstance();
	connect(this, &qtFRSConfigurationManager::issueOpenConfigManager,
		qtFRSMainWindow::getInstance(), &qtFRSMainWindow::getReady);
	emit issueOpenConfigManager();

	delete ui;
}
