#ifndef QTFRSMAINWINDOW_H
#define QTFRSMAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include <QFile>
#include <QScrollBar>

#include <opencv2\core.hpp>
#include <opencv2\highgui.hpp>
#include <opencv2\imgproc.hpp>
#include <opencv2\videoio.hpp>

#include "ui_qtfrsmainwindow.h"

#include "Features/STD.h"

#include "Filters/Dog.h"

#include "GlobalOptions/GlobalOptions.h"

#include "FaceModeller.h"
#include "FaceDetector.h"
#include "ShapeWrapper.h"


#include "Dog.h"
#include "LogGabor.h"
#include "TangTriggs.h"

#include "STD.h"

#include "EuclidDistance.h"
#include "HiSqDistance.h"
#include "CosineDistance.h"

#include "BiovectorBase.h"
#include "Recognizer.h"

#include "FrameBuffer.h"
#include "FaceBuffer.h"
#include "DecoderRunnable.h"
#include "FrameProcessingRunnable.h"

#include "kNNClassifier.h"

#include "FaceImageProcessor.h"
#include "PCADimensionReduction.h"

class qtFRSMainWindow : public QMainWindow
{
	Q_OBJECT

public:
	qtFRSMainWindow(QWidget *parent = 0);
	~qtFRSMainWindow();

	static qtFRSMainWindow* instance;
	static qtFRSMainWindow* getInstance();

private:
	Ui::qtFRSMainWindowClass ui;

	QString _videoInputPath;
	QString _biovectorsPath;
	QString _faceImagesPath;

	//���������� � ������ ������ ������
	QSharedPointer<QTimer> mVideoTimer;
	QSharedPointer<cv::VideoCapture> mVideoCapture;

	//����������, ��������, �������, ��������������
	DecoderRunnable *mDecoderRunnable;
	QSharedPointer<BiovectorBase> mBiovectorBase;
	QSharedPointer<FaceDetector> mFaceDetector;
	std::vector<QSharedPointer<FaceImageProcessor>> mFaceProcessors;
	std::vector<QSharedPointer<AbstractFilter>> mFilters;
	std::vector<QSharedPointer<AbstractExtractor>> mExtractors;
	std::vector<QSharedPointer<AbstractClassifier>> mClassifiers;
	QSharedPointer<AbstractDimensionReduction> mDimensionReductor;
	QSharedPointer<Recognizer> mRecognizer;

	//������
	QSharedPointer<FrameBuffer> mReadBuffer;
	QSharedPointer<FrameBuffer> mOutputBuffer;
	QSharedPointer<FaceBuffer> mFaceBuffer;

	//������
	QThreadPool* mThreadPool;
	std::vector<FrameProcessingRunnable*> mProcessingRunnables;


	void startThreadsProcessing();
	void startProcessingThread();

	QMutex mLogMutex;
	QSharedPointer<QMutex> mReadMutex;
	QSharedPointer<QWaitCondition> mReadWaitCondition;
	QSharedPointer<QSemaphore> mThreadSemaphore;

	bool videoStreaming = false;
	bool processingOnPause = false;
	
	//��� ���������� ������ "�������"
	QVBoxLayout* _viewScrollAreaFaceImagesLayout;
	QHBoxLayout* _viewScrollAreaDetectFaceImagesLayout;
	QHBoxLayout* _viewScrollAreaRecognizeFaceImagesLayout;

	QSharedPointer<QList<QLabel*>> _viewScrollAreaDetectFacesList;
	QSharedPointer<QList<QLabel*>> _viewScrollAreaRecognizeFacesList;

	//��� ��������� ������ "�������"
	QHBoxLayout* _viewPersonScrollAreaLayout;
	QVBoxLayout* _viewPersonScrollAreaRecognizeLayout;
	QVBoxLayout* _viewPersonScrollAreaInfLayout;

	QSharedPointer<QList<QLabel*>> _viewPersonScrollAreaRecognizeList;
	QSharedPointer<QList<QLabel*>> _viewPersonScrollAreaInfList;

	int _maxViewScrollAreaImageListSize;


	int testID;
	clock_t timerStart;

signals:
	void startButtonClicked();
	void pauseButtonClicked();
	void stopButtonClicked();

	void frameReady(QImage &img);
	void detectFace(QImage &img, QImage &recImg);
	void recogFace(QImage &recImg, QString &personName);

	void videoStreamStop();

	void clikedCreateBiovectorBase(QString& directoryPath);
	void clikedReadBiovectorBase(QString& directoryPath);
	void startReduceDimension(bool state);

	void biovectorBaseMessage2MainForm(QString& message);
	void biovectorBaseMessage2BiovectorBaseForm(QString& message);
	
	void startCreateBiovector();

private slots:
	void processStartButton();
	void processPauseButton();
	void processStopButton();

	void openConfigManager();
	void openBiovectorOptions();

	void toggleChooseConfigComboBox(bool state);
	void toggleVideoControl(bool state);
	void updateConfigurationList();
	void searchConfigManager();
	void configComboBoxChanged(const int index);

	//TODO: � ������ ����� ������� �������-�������, ��������������
public slots:
	void createBiovectors(QString &imagesPath);
	void readBiovectors(QString &imagesPath);
	
	void showFrame(QImage &img);
	void processFrame();
	void clearFrame();

	void addScrollAreaImage(QImage &img, QImage &recImg);
	void addPersonScrollArea(QImage &recImg, QString &personName);

	void startVideoStream();
	void pauseVideoStream();
	void stopVideoStream();

	void appendTextToLog(QString &text);

	void getReady(bool updateList);
};

#endif // QTFRSMAINWINDOW_H
