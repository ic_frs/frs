#include "qtfrsbiovectorcreate.h"
#include "ui_qtfrsbiovectorcreate.h"
#include "qtfrsmainwindow.h"

qtFRSBiovectorCreate::qtFRSBiovectorCreate(QWidget *parent) :
QDialog(parent),
ui(new Ui::qtFRSBiovectorCreate)
{
	ui->setupUi(this);

	_faceImagesDirectory = QString::fromStdString(OPTIONS["Global"]["BiovectorBaseFolder"].string_data);
	ui->lineEditFaceImagePath->setText(_faceImagesDirectory);

	if (OPTIONS["Global"]["CreateBiovectors"].bool_data)
	{
		QTimer::singleShot(1, this, &qtFRSBiovectorCreate::createBiovectorBase);
		//qtFRSBiovectorCreate::close();
	}
	else
	{
		QTimer::singleShot(1, this, &qtFRSBiovectorCreate::readBiovectorBase);
		//qtFRSBiovectorCreate::rejected();
	}
}

//����������� �������� �������� ����������� � ���������� ����
void qtFRSBiovectorCreate::showBiovectorBaseMessage(QString& message)
{
	ui->textEditBiovectorBaseMessages->append(message);
	QCoreApplication::processEvents();
}

//������������� ������� ������ � �������� ������� �� �������� ����������
void qtFRSBiovectorCreate::createBiovectorBase()
{
	emit clikedCreateBiovectorBaseButton(_faceImagesDirectory);
}

//������ �����������
void qtFRSBiovectorCreate::readBiovectorBase()
{
	emit clikedReadBiovectorBaseButton(_faceImagesDirectory);
}

qtFRSBiovectorCreate::~qtFRSBiovectorCreate()
{
	delete ui;
}
