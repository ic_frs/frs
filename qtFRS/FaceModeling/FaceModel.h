#ifndef _F_MODEL_H_
#define _F_MODEL_H_

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "Triangle.h"
#include <math.h>

using namespace trl;

// ����� ������ ������������ ����
namespace fml{
	class MFace
	{
	public:
		std::vector <Triangle> faceTriangles;	// ������ �������������, ������������ �� ������ ����� ����
		double Model3DM[123][182];	// ������� ������� ��� ������ ����
		cv::Point2i coordsM[20000];	// ������ ��������� ��� ����������� ������ ����� ���� � �� ��������
		double coefM;	// ���������� ��������������� ��� ������� �������
		int typeOfModel = 0;
	public:
		MFace();	// �����������
		MFace(cv::Point3f *fPoints, cv::Point2i* coords, double** Model3D, double coef);

		void MFace::drawFace(cv::Mat img);	// ��������� ����������� ����� �� �������� �����������
		void MFace::sortByZ();	// ���������� (�� ������������)
		int MFace::maxST();	// ����� ������������ � ������������ ��������
		int MFace::maxLN(); // ����� ������������ � ������������ ��������
		void MFace::break2Trs(int num);	// ��������� ������ ������������ �� 2 �� ���������� �������
		void MFace::breakATrs(int count);	// ���� ��������� ������������� � ������������ �������� �� ��������� ������ ����� ������������� � ������

		void MFace::rotation(cv::Point3f centerP, double alpha, double beta);	// ������� �� �������� ���� �� ��� X � �� ��� Y � ������� � �������� �����
		void MFace::rotationZ(cv::Point3f centerP, double alpha, double beta);	// ������� ���������� Z �� �������� ���� �� ��� X � �� ��� Y � ������� � �������� �����
	};
} //namespace fml
#endif