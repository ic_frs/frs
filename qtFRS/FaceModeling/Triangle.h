#ifndef _TRIANGLE_H_
#define _TRIANGLE_H_

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// ����� ������������
namespace trl{
	class Triangle
	{
	public:
		cv::Point3f pt1, pt2, pt3;	// ���������� �����
		int ix1, ix2, ix3;	// ������� �����
		double l12, l13, l23, sT;	// ����� ������ � ������� ������������
	public:
		Triangle();	// �����������
		Triangle(cv::Point3f p1, cv::Point3f p2, cv::Point3f p3, int i1, int i2, int i3);

		void Triangle::draw(cv::Mat image, cv::Scalar color = cvScalar(255, 255, 255)); // ��������� ������������ ����� ������� �� �������� �����������
		cv::Point2f* Triangle::getTrianglePoints();	// ��������� ����� ������������ � ���� ������� �� ���� ��������� (��� ��������)
	};
} //namespace fml
#endif