#include "Triangle.h"
using namespace trl;

Triangle::Triangle()
{

}

Triangle::Triangle(cv::Point3f p1, cv::Point3f p2, cv::Point3f p3, int i1, int i2, int i3)
{
	pt1 = p1; pt2 = p2; pt3 = p3;
	ix1 = i1; ix2 = i2; ix3 = i3;
	l12 = sqrt((pt2.x - pt1.x)*(pt2.x - pt1.x) + (pt2.y - pt1.y)*(pt2.y - pt1.y));
	l13 = sqrt((pt3.x - pt1.x)*(pt3.x - pt1.x) + (pt3.y - pt1.y)*(pt3.y - pt1.y));
	l23 = sqrt((pt2.x - pt3.x)*(pt2.x - pt3.x) + (pt2.y - pt3.y)*(pt2.y - pt3.y));
	sT = abs((pt1.x - pt3.x)*(pt2.y - pt3.y) - (pt2.x - pt3.x)*(pt1.y - pt3.y));
}

void Triangle::draw(cv::Mat image, cv::Scalar color)
{
	line(image, cv::Point(pt1.x, pt1.y), cv::Point(pt2.x, pt2.y), color);
	line(image, cv::Point(pt1.x, pt1.y), cv::Point(pt3.x, pt3.y), color);
	line(image, cv::Point(pt2.x, pt2.y), cv::Point(pt3.x, pt3.y), color);
}

cv::Point2f* Triangle::getTrianglePoints()
{
	cv::Point2f* trPoints = new cv::Point2f[3];
	trPoints[0].x = pt1.x;
	trPoints[0].y = pt1.y;
	trPoints[1].x = pt2.x;
	trPoints[1].y = pt2.y;
	trPoints[2].x = pt3.x;
	trPoints[2].y = pt3.y;
	return trPoints;
}

