#include "Dog.h"
#include <opencv2\highgui\highgui.hpp>

Dog::Dog(void)
{
}

Dog::~Dog(void)
{
}

void Dog::process(const cv::Mat& source, cv::Mat& dest)
{
	// �����������, ��������� �� ����� �������� ������ �� �������
	cv::Mat bwImg = source + 1;

	// ����������� � ������� ����� � ��������� ������
	bwImg.convertTo(dest, CV_32F);

	// ����������������
	//log(dest, dest);

	// �����-���������, ������������, ���������������
	pow(dest, 0.1, dest);
	normalize(dest, dest, 0, 255, cv::NORM_MINMAX);
	convertScaleAbs(dest, dest);

	// Difference of Gaussian
	cv::Mat g1, g2;
	GaussianBlur(dest, g1, cv::Size(11, 11), 1.0, 1.0);
	GaussianBlur(dest, g2, cv::Size(11, 11), 3., 3.);
	dest = g1 - g2;

	// ��������� ������������
	normalize(dest, dest, 0, 255, cv::NORM_MINMAX);

	cv::equalizeHist(dest, dest);

	//cv::imwrite("dog" + std::to_string(rand() % 1000) + ".jpg", dest);
}