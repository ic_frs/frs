#ifndef DOG_H
#define DOG_H

#include <opencv2\core\core.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include "AbstractFilter.h"

class Dog : public AbstractFilter
{
public:
	Dog(void);
	~Dog(void);

	void process(const cv::Mat& source, cv::Mat& dest);
};

#endif
