#ifndef DFTFILTER_H
#define DFTFILTER_H

#include <opencv2\core.hpp>
#include <opencv2\imgproc.hpp>
#include <opencv2\highgui.hpp>
#include "AbstractFilter.h"

class DftFilter : public AbstractFilter
{
public:
	DftFilter(int lSigma = 10, int hSigma = 100);

	~DftFilter();

	void process(const cv::Mat &source, cv::Mat &dest);
	void process(cv::Mat source, cv::Mat &dest, cv::Mat filter);

	cv::Mat DftFilter::getMag(cv::Mat source);

	void DftFilter::dftShift(cv::Mat source, cv::Mat& dest);
	void DftFilter::getComplexImg(cv::Mat source, cv::Mat& complexImg);
	void DftFilter::getMag(cv::Mat source, cv::Mat& mag);

private:
	void rejectFrequency(cv::Mat& complexImg, cv::Mat window);
	void calcWindow(cv::Mat& windowImg);
	void calcWindow2(cv::Mat& windowImg);

	float calcGauss(float meanX, float meanY, float sigma, int x, int y);

	int _mHightSigma;
	int _mLowSigma;

};

#endif