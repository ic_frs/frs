#ifndef LPRSRFILTER_H
#define LPRSRFILTER_H

#include <opencv2\core.hpp>
#include <opencv2\imgproc.hpp>
#include <opencv2\highgui.hpp>
#include "AbstractFilter.h"

class LPRSR : public AbstractFilter
{
public:
	LPRSR(int spraysNum = 1, int pixelPerSprayNum = 250, int inputKernelSize = 25,
		  int intensityChangeKernelSize = 25, int r = 1, int c = 1, double intensityChangeSigma = 0.0,
		  double upperBound = 255.0, double inputSigma = 0.0, bool normalizeIntensityChange = false);

	~LPRSR() {};

private:
	int _mSpraysNum;
	int _mPixelPerSprayNum;
	int _mInputKernelSize;
	int _mIntensityChangeKernelSize;
	int _mR;
	int _mC;
	double _mIntensityChangeSigma;
	double _mUpperBound;
	double _mInputSigma;
	bool _mNormalizeIntensityChange;

	void process(const cv::Mat &source, cv::Mat &dest);
	void PerformRandomSpraysRetinex(const cv::Mat &source, cv::Mat &dest);
	void DeleteSprays(cv::Point2i **sprays, int spraysCount);
	cv::Point2i **CreateSprays(int spraysCount, int R);
	void Filter64F(const cv::Mat &source, cv::Mat &dest);
};

#endif