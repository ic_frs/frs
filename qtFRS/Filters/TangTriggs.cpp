#include "TangTriggs.h"

TangTriggs::TangTriggs(float a, float t,
					   float g, int s0,
					   int s1)
{
	_mAlpha = a;
	_mTau = t;
	_mGamma = g;
	_mSigma0 = s0;
	_mSigma1 = s1;
}

TangTriggs::~TangTriggs(void)
{
}

void TangTriggs::process(const cv::Mat src, cv::Mat& dest)
{
	// Convert to floating point:
	cv::Mat X = src.clone();	
	X.convertTo(X, CV_32FC1);
	// Start preprocessing:
	cv::Mat I;
	pow(X, _mGamma, I);
	// Calculate the DOG Image:
	{
		cv::Mat gaussian0, gaussian1;
		// Kernel Size:
		int kernel_sz0 = (3 * _mSigma0);
		int kernel_sz1 = (3 * _mSigma1);
		// Make them odd for OpenCV:
		kernel_sz0 += ((kernel_sz0 % 2) == 0) ? 1 : 0;
		kernel_sz1 += ((kernel_sz1 % 2) == 0) ? 1 : 0;
		GaussianBlur(I, gaussian0, cv::Size(kernel_sz0, kernel_sz0), _mSigma0, _mSigma0, cv::BORDER_CONSTANT);
		GaussianBlur(I, gaussian1, cv::Size(kernel_sz1, kernel_sz1), _mSigma1, _mSigma1, cv::BORDER_CONSTANT);
		subtract(gaussian0, gaussian1, I);
	}

	{
		double meanI = 0.0;
		{
			cv::Mat tmp;
			pow(abs(I), _mAlpha, tmp);
			meanI = mean(tmp).val[0];

		}
		I = I / pow(meanI, 1.0 / _mAlpha);
	}

	{
		double meanI = 0.0;
		{
			cv::Mat tmp;
			pow(min(abs(I), _mTau), _mAlpha, tmp);
			meanI = mean(tmp).val[0];
		}
		I = I / pow(meanI, 1.0 / _mAlpha);
	}

	// Squash into the tanh:
	{
		for (int r = 0; r < I.rows; r++) {
			for (int c = 0; c < I.cols; c++) {
				I.at<float>(r, c) = tanh(I.at<float>(r, c) / _mTau);
			}
		}
		I = _mTau * I;
	}

	cv::normalize(I, dest, 0, 255, cv::NORM_MINMAX, CV_8UC1);
}