#ifndef LOGGABOR_H
#define LOGGABOR_H

#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <iostream>
#include "DftFilter.h"
#include "AbstractFilter.h"

using namespace std;
using namespace cv;

class LogGaborDecomposition : public AbstractFilter
{
public:
	LogGaborDecomposition(Size kS, int ns, int no, float minWL, float mul, float sOnF, float dTOnSig);

	~LogGaborDecomposition();

	void showKernels();
	void process(const Mat& source, Mat& dest);

	vector < vector < Mat > > makeKernels();

private:
	Mat GetRadius();
	Mat GetRadialComponent(Mat radius, float waveLength);
	Mat GetAngularComponent(Mat radius, float angle);

	DftFilter* _mDftFilter;

	vector < vector < Mat > > kernels;

	Size kerSize;
	
	int _mNScale;
	int _mNOrient;
	float _mMinWaveLength;
	float _mMult;
	float _mSigmaOnF;
	float _mDThetaOnSigma;
	float _mThetaSigma;
	int _mKernelsType;

};

#endif //LOGGABOR_H