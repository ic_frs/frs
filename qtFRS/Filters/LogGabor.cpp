#include "LogGabor.h"

LogGaborDecomposition::LogGaborDecomposition(cv::Size kS, int ns, int no, float minWL, float mul, float sOnF, float dTOnSig)
{
	kerSize = kS;

	_mNScale = ns;
	_mNOrient = no;
	_mMinWaveLength = minWL;
	_mMult = mul;
	_mSigmaOnF = sOnF;
	_mDThetaOnSigma = dTOnSig;
	_mKernelsType = CV_32F;
	_mThetaSigma = CV_PI / no / _mDThetaOnSigma;

	makeKernels();
}

LogGaborDecomposition::~LogGaborDecomposition()
{
	delete _mDftFilter;
}

std::vector<std::vector<cv::Mat>> LogGaborDecomposition::makeKernels()
{
	cv::Mat radius = GetRadius();

	for (int i = 0; i < _mNScale; ++i)
	{	
		float waveLength = _mMinWaveLength * pow(_mMult, i);

		cv::Mat radialComponent = GetRadialComponent(radius, waveLength);
		
		std::vector<cv::Mat> tempKernels;

		for (int j = 0; j < _mNOrient; ++j)
		{
			float angle = j * (2 * CV_PI / _mNOrient);

			cv::Mat angularComponent = GetAngularComponent(radius, angle);
			cv::Mat kernel = radialComponent.mul(angularComponent);

			_mDftFilter->dftShift(kernel, kernel);

			tempKernels.push_back(kernel);

			double min, max;
			cv::Mat win;
			cv::minMaxLoc(kernel, &min, &max);
			kernel.convertTo(win, CV_8U, 255 / max);
			_mDftFilter->dftShift(win, win);

			//cv::imwrite(std::to_string(i) + std::to_string(j) + ".jpg", win);
			//cv::namedWindow("ker" + std::to_string(i) + std::to_string(j), CV_WINDOW_NORMAL);
			//cv::imshow("ker" + std::to_string(i) + std::to_string(j), kernel);
		}

		kernels.push_back(tempKernels);
	}

	return kernels;
}

void LogGaborDecomposition::process(const cv::Mat &source, cv::Mat& dest)
{
	dest = cv::Mat::zeros(kerSize, CV_32F);

	double min, max;
	double mean;

	cv::Mat filtredImage;

	for (int i = 0; i < (int)kernels.size(); ++i)
	{
		for (int j = 0; j < (int)kernels[i].size(); ++j)
		{
			_mDftFilter->process(source, filtredImage, kernels[i][j]);

			cv::minMaxLoc(filtredImage, &min, &max);
			mean = cv::mean(filtredImage)[0];
			filtredImage -= mean;
			filtredImage.convertTo(filtredImage, CV_32F, 1.0 / max);
			cv::normalize(filtredImage, filtredImage, 0, 1, CV_MINMAX);

			dest += filtredImage;
		}
	}

	dest /= kernels.size() * kernels[0].size();

	cv::minMaxLoc(dest, &min, &max);
	dest.convertTo(dest, CV_8U, 255 / max);
}

void LogGaborDecomposition::showKernels()
{
	for (int i = 0; i < (int)kernels.size(); ++i)
	{
		for (int j = 0; j < (int)kernels[0].size(); ++j)
		{
			cv::namedWindow("ker" + std::to_string(i) + std::to_string(j), CV_WINDOW_NORMAL);
			cv::imshow("ker" + std::to_string(i) + std::to_string(j), kernels[i][j]);
		}
	}
}

cv::Mat LogGaborDecomposition::GetRadius()
{
	int xmax = kerSize.width / 2;
	int xmin = -kerSize.width / 2;
	int ymax = kerSize.height / 2;
	int ymin = -kerSize.height / 2;

	cv::Mat radius(kerSize, _mKernelsType);

	for (int i = 0; i < (int)radius.size().height; ++i)
	{
		for (int j = 0; j < (int)radius.size().width; ++j)
		{
			radius.at<float>(i, j) = sqrt(pow((float)(ceil(ymin + i)) / radius.size().height, 2) + pow((float)(ceil(xmin + j)) / radius.size().width, 2));
		}
	}

	_mDftFilter->dftShift(radius, radius);

	//cv::namedWindow("rad", CV_WINDOW_NORMAL);
	//cv::imshow("rad", radius);

	return radius;
}

cv::Mat LogGaborDecomposition::GetRadialComponent(cv::Mat radius, float waveLength)
{
	cv::Mat radialComponent(kerSize, _mKernelsType);
	cv::Mat lowpass(kerSize, _mKernelsType);

	float f0 = 1.0 / waveLength;
	float cut = 0.45;
	float sharp = 15;

	for (int i = 0; i < radialComponent.size().height; ++i)
	{
		for (int j = 0; j < radialComponent.size().width; ++j)
		{
			radialComponent.at<float>(i, j) = exp(-(pow(log(radius.at<float>(i, j) / f0), 2)) / (2.0 * pow(log(_mSigmaOnF), 2)));
			lowpass.at<float>(i, j) = (1.0 / (1.0 + pow((radius.at<float>(i, j) / cut), (2 * sharp))));
		}
	}

	_mDftFilter->dftShift(radialComponent, radialComponent);
	_mDftFilter->dftShift(lowpass, lowpass);

	radialComponent = lowpass.mul(radialComponent);

	//cv::namedWindow("lowpass", CV_WINDOW_NORMAL);
	//cv::imshow("lowpass", lowpass);

	//cv::namedWindow("radialComponent", CV_WINDOW_NORMAL);
	//cv::imshow("radialComponent", radialComponent);

	return radialComponent;
}

cv::Mat LogGaborDecomposition::GetAngularComponent(cv::Mat radius, float angle)
{
	cv::Mat angularComponent(kerSize, _mKernelsType);

	float theta;
	float sintheta;
	float costheta;
	float ds;
	float dc;
	float dtheta;

	int xmax = kerSize.width / 2;
	int xmin = -kerSize.width / 2;
	int ymax = kerSize.height / 2;
	int ymin = -kerSize.height / 2;

	for (int i = 0; i < radius.size().height; ++i)
	{
		for (int j = 0; j < radius.size().width; ++j)
		{
			theta = atan2((float)-(ceil(ymin + i)) / radius.size().height, (float)(ceil(xmin + j)) / radius.size().width);
			sintheta = sin(theta);
			costheta = cos(theta);
			ds = sintheta * cos(angle) - costheta * sin(angle);
			dc = costheta * cos(angle) + sintheta * sin(angle);
			dtheta = abs(atan2(ds, dc));
			angularComponent.at<float>(i, j) = exp((-pow(dtheta, 2)) / (2.0 * _mThetaSigma));
		}
	}
	
	//cv::namedWindow("angularComponent", CV_WINDOW_NORMAL);
	//cv::imshow("angularComponent", angularComponent);

	return angularComponent;
}