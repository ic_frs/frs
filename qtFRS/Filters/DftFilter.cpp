#include "DftFilter.h"

DftFilter::DftFilter(int lSigma, int hSigma) : _mLowSigma(lSigma), _mHightSigma(hSigma)
{
}

DftFilter::~DftFilter(void)
{
}

void DftFilter::dftShift(cv::Mat source, cv::Mat& dest)
{
	// crop the spectrum, if it has an odd number of rows or columns
	source.copyTo(dest);

	dest = dest(cv::Rect(0, 0, dest.cols & -2, dest.rows & -2));

	int cx = dest.cols / 2;
	int cy = dest.rows / 2;

	// rearrange the quadrants of Fourier image
	// so that the origin is at the image center
	cv::Mat tmp;
	cv::Mat q0(dest, cv::Rect(0, 0, cx, cy));
	cv::Mat q1(dest, cv::Rect(cx, 0, cx, cy));
	cv::Mat q2(dest, cv::Rect(0, cy, cx, cy));
	cv::Mat q3(dest, cv::Rect(cx, cy, cx, cy));

	q0.copyTo(tmp);
	q3.copyTo(q0);
	tmp.copyTo(q3);

	q1.copyTo(tmp);
	q2.copyTo(q1);
	tmp.copyTo(q2);
}

void DftFilter::getMag(cv::Mat source, cv::Mat& mag)
{
	cv::Mat planes[3];

	cv::split(source, planes);
	cv::magnitude(planes[0], planes[1], planes[0]);
	mag = planes[0];

	cv::normalize(mag, mag, 0, 1, CV_MINMAX);
}

void DftFilter::getComplexImg(cv::Mat source, cv::Mat& complexImg)
{
	int M, N;

	M = cv::getOptimalDFTSize(source.rows);
	N = cv::getOptimalDFTSize(source.cols);

	int subtrahend = 1;

	cv::Mat resizeImg = source.clone();

	while ((M % 2) || (N % 2))
	{
		cv::resize(source, resizeImg, cv::Size(source.rows - subtrahend, source.cols - subtrahend));
		M = cv::getOptimalDFTSize(resizeImg.rows);
		N = cv::getOptimalDFTSize(resizeImg.cols);
		++subtrahend;
	}

	source = resizeImg.clone();

	cv::Mat padded;
	cv::copyMakeBorder(source, padded, 0, M - source.rows, 0, N - source.cols, cv::BORDER_CONSTANT, cv::Scalar::all(0));

	cv::Mat planes[] = { cv::Mat_<float>(padded), cv::Mat::zeros(padded.size(), CV_32F) };
	cv::merge(planes, 2, complexImg);
	cv::dft(complexImg, complexImg);
}

cv::Mat DftFilter::getMag(cv::Mat source)
{
	cv::Mat magImg;
	cv::Mat complexImg;

	getComplexImg(source, complexImg);
	getMag(complexImg, magImg);

	dftShift(magImg, magImg);

	cv::log(magImg, magImg);

	cv::normalize(magImg, magImg, 0, 1, CV_MINMAX);

	return magImg;
}

void DftFilter::process(cv::Mat source, cv::Mat &dest, cv::Mat filter)
{
	cv::Mat magImg;
	cv::Mat complexImg;

	int originalWidth(source.cols), originalHeight(source.rows);

	getComplexImg(source, complexImg);
	getMag(complexImg, magImg);

	rejectFrequency(complexImg, filter);

	getMag(complexImg, magImg);
	//cv::imshow("mag", magImg);

	cv::Mat shiftMagImg;
	dftShift(magImg, shiftMagImg);
	normalize(shiftMagImg, shiftMagImg, 0, 1, CV_MINMAX);
	//cv::imshow("mag", shiftMagImg);

	cv::Mat shiftFilter;
	dftShift(filter, shiftFilter);
	//cv::imshow("logGabor", shiftFilter);

	cv::Mat inverseTransform;
	cv::dft(complexImg, inverseTransform, cv::DFT_INVERSE | cv::DFT_REAL_OUTPUT);
	normalize(inverseTransform, inverseTransform, 0, 255, CV_MINMAX);

	inverseTransform.convertTo(dest, CV_8UC1);

	resize(dest, dest, cv::Size(originalWidth, originalHeight));
}

void DftFilter::process(const cv::Mat &source, cv::Mat &dest)
{
	cv::Mat magImg;

	int originalWidth(source.cols), originalHeight(source.rows);

	int M, N;
	
	
	M = cv::getOptimalDFTSize(source.rows);
	N = cv::getOptimalDFTSize(source.cols);

	int subtrahend = 1;

	cv::Mat resizeImg = source.clone();

	while ((M%2) || (N%2))
	{
		cv::resize(source, resizeImg, cv::Size(source.rows - subtrahend, source.cols - subtrahend));
		M = cv::getOptimalDFTSize(resizeImg.rows);
		N = cv::getOptimalDFTSize(resizeImg.cols);
		++subtrahend;
	}

	cv::Mat padded;
	cv::copyMakeBorder(resizeImg, padded, 0, M - resizeImg.rows, 0, N - resizeImg.cols, cv::BORDER_CONSTANT, cv::Scalar::all(0));

	cv::Mat planes[] = { cv::Mat_<float>(padded), cv::Mat::zeros(padded.size(), CV_32F) };
	cv::Mat complexImg;
	cv::merge(planes, 2, complexImg);
	cv::dft(complexImg, complexImg);

	cv::split(complexImg, planes);
	cv::magnitude(planes[0], planes[1], planes[0]);
	magImg = planes[0];


	// crop the spectrum, if it has an odd number of rows or columns
	magImg = magImg(cv::Rect(0, 0, magImg.cols & -2, magImg.rows & -2));


	/*--------------------------------*/
	cv::Mat windowMat;
 	windowMat.create(complexImg.rows, complexImg.cols, CV_32F);
 	calcWindow(windowMat);
 	
	rejectFrequency(complexImg, windowMat);
	/*--------------------------------*/


	int cx = magImg.cols / 2;
	int cy = magImg.rows / 2;

	// rearrange the quadrants of Fourier image
	// so that the origin is at the image center
	cv::Mat tmp;
	cv::Mat q0(magImg, cv::Rect(0, 0, cx, cy));
	cv::Mat q1(magImg, cv::Rect(cx, 0, cx, cy));
	cv::Mat q2(magImg, cv::Rect(0, cy, cx, cy));
	cv::Mat q3(magImg, cv::Rect(cx, cy, cx, cy));

	q0.copyTo(tmp);
	q3.copyTo(q0);
	tmp.copyTo(q3);

	q1.copyTo(tmp);
	q2.copyTo(q1);
	tmp.copyTo(q2);

	cv::normalize(magImg, magImg, 0, 255, CV_MINMAX);

	//imshow("aaaa", magImg);

	cv::Mat inverseTransform;
	cv::dft(complexImg, inverseTransform, cv::DFT_INVERSE|cv::DFT_REAL_OUTPUT);
	normalize(inverseTransform, inverseTransform, 0, 255, CV_MINMAX);

	inverseTransform.convertTo(dest, CV_8UC1);

	resize(dest, dest,  cv::Size(originalWidth, originalHeight));
}

void DftFilter::rejectFrequency(cv::Mat& complexImg, cv::Mat window)
{
 	for (int x = 0; x < window.cols; ++x)
 	{
 		for (int y = 0; y < window.rows; ++y)
 		{
 			complexImg.at<cv::Vec2f>(y,x)[0] = complexImg.at<cv::Vec2f>(y,x)[0] * window.at<float>(y, x);
 			complexImg.at<cv::Vec2f>(y,x)[1] = complexImg.at<cv::Vec2f>(y,x)[1] * window.at<float>(y, x);
 		}
	}
}

void DftFilter::calcWindow( cv::Mat& windowImg)
{
	double meanX = double(windowImg.cols) / 2.;
	double meanY = double(windowImg.rows) / 2.;

	double min = 1.;

	for(int x = 0; x < windowImg.cols; ++x)
		for(int y = 0; y < windowImg.rows; ++y)
			windowImg.at<float>(x, y) = 1.0 - calcGauss(meanX, meanY, _mLowSigma, x, y);

	calcWindow2(windowImg);

	normalize(windowImg, windowImg, 0, 1, cv::NORM_MINMAX);

	//imshow("Window", windowImg);

	int cx = windowImg.cols / 2;
	int cy = windowImg.rows / 2;

	cv::Mat tmp;
	cv::Mat q0(windowImg, cv::Rect(0, 0, cx, cy));
	cv::Mat q1(windowImg, cv::Rect(cx, 0, cx, cy));
	cv::Mat q2(windowImg, cv::Rect(0, cy, cx, cy));
	cv::Mat q3(windowImg, cv::Rect(cx, cy, cx, cy));

	q0.copyTo(tmp);
	q3.copyTo(q0);
	tmp.copyTo(q3);

	q1.copyTo(tmp);
	q2.copyTo(q1);
	tmp.copyTo(q2);
}

void DftFilter::calcWindow2( cv::Mat& windowImg )
{
	double meanX = double(windowImg.cols) / 2.;
	double meanY = double(windowImg.rows) / 2.;

	double min = 1.;

	for(int x = 0; x < windowImg.cols; ++x)
		for(int y = 0; y < windowImg.rows; ++y)
			windowImg.at<float>(x, y) = windowImg.at<float>(x, y) * calcGauss(meanX, meanY, _mHightSigma, x, y);;
}

float DftFilter::calcGauss( float meanX, float meanY, float sigma, int x, int y )
{
	double degX = ((double(x) - meanX) * (double(x) - meanX) * -1.) / (2. * sigma * sigma);
	double degY = ((double(y) - meanY) * (double(y) - meanY) * -1.) / (2. * sigma * sigma);
	double deg = degX + degY;

	return pow(2.718281828, deg);
}