#ifndef TANGTRIGGS_H
#define TANGTRIGGS_H

#include <opencv2\core\core.hpp>
#include <opencv2\imgproc\imgproc.hpp>

class TangTriggs
{
public:
	TangTriggs(float a = 0.1, float t = 10.0,
			   float g = 0.2, int s0 = 1,
			   int s1 = 2);
	~TangTriggs(void);

	void process(const cv::Mat source, cv::Mat& dest);

private:
	float _mAlpha;
	float _mTau;
	float _mGamma;
	int _mSigma0;
	int _mSigma1;
};

#endif
