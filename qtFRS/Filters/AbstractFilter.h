#ifndef ABSTRACTFILTER_H
#define ABSTRACTFILTER_H

#include <opencv2\core\core.hpp>

class AbstractFilter
{
public:

	virtual ~AbstractFilter() {};

	virtual void process(const cv::Mat &source, cv::Mat &dest) = 0;
};

#endif