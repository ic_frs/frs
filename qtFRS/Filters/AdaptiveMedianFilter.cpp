#include "AdaptiveMedianFilter.h"

AdaptiveMedianFilter::AdaptiveMedianFilter(int minWinSize, int maxWinSize)
{
	minSize = minWinSize;
	maxSize = maxWinSize;
}

std::vector<int> AdaptiveMedianFilter::matToVect(cv::Mat src)
{
	std::vector<int> vect;

	for (int i = 0; i < src.rows; ++i)
	{
		for (int j = 0; j < src.cols; ++j)
		{
			vect.push_back(src.at<uchar>(i, j));
		}
	}

	return vect;
}

//���������� �������� �������
int AdaptiveMedianFilter::getMedian(cv::Mat src)
{
	//�.�. ������ ���������, ������������� ������� � ������
	std::vector<int> matVect = matToVect(src);

	std::sort(matVect.begin(), matVect.end());

	//� ���� �������� �� ��������
	int median = matVect[std::ceil((float)matVect.size() / 2)];

	return median;
}

int AdaptiveMedianFilter::getAdaptMedian(cv::Mat src, int curRow, int curCol, int winSize, int maxWinSize)
{
	//����� �������, � ������� ����� ������ �������
	cv::Mat scanRegion(src, cv::Range(curRow - (winSize / 2), curRow + std::ceil((float)winSize / 2)), cv::Range(curCol - (winSize / 2), curCol + std::ceil((float)winSize / 2)));

	double min, max;
	cv::minMaxLoc(scanRegion, &min, &max);

	//���� �������� �������
	int median = getMedian(scanRegion);

	//�������� ������� - min; - max
	int medMin = median - min;
	int medMax = median - max;

	if ((medMin > 0) && (medMax < 0))
	{
		//�������� ������������ ������� ������� - min; - max
		int curPixMin = src.at<uchar>(curRow, curCol) - min;
		int curPixMax = src.at<uchar>(curRow, curCol) - max;

		if ((curPixMin > 0) && (curPixMax < 0))
		{
			return src.at<uchar>(curRow, curCol);
		}
		else
		{
			return median;
		}
	}
	else
	{
		//����������� �������, � ������� ���� �������
		winSize += 2;

		if (winSize <= maxWinSize)
		{
			//��������, � ������� �������� ������� ������ �������
			return getAdaptMedian(src, curRow, curCol, winSize, maxWinSize);
		}
		else
		{
			return src.at<uchar>(curRow, curCol);
		}
	}
}

void AdaptiveMedianFilter::process(cv::Mat src, cv::Mat& dst)
{
	dst = src.clone();

	for (int i = (maxSize / 2); i < (src.rows - (maxSize / 2)); ++i)
	{
		for (int j = (maxSize / 2); j < (src.cols - (maxSize / 2)); ++j)
		{
			dst.at<uchar>(i, j) = getAdaptMedian(src, i, j, minSize, maxSize);
		}
	}
}