#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <algorithm>
#include <math.h>

class AdaptiveMedianFilter
{
public:
	AdaptiveMedianFilter(int minWinSize, int maxWinSize);
	
	void process(cv::Mat src, cv::Mat& dst);

private:
	//����������� � ������������ ������ ���� �������
	int minSize;
	int maxSize;

	//TODO: ������� matToVect � ��������� helper
	std::vector<int> AdaptiveMedianFilter::matToVect(cv::Mat src);
	int AdaptiveMedianFilter::getMedian(cv::Mat src);
	int AdaptiveMedianFilter::getAdaptMedian(cv::Mat src, int curRow, int curCol, int winSize, int maxWinSize);

};