#ifndef KNNCLASSIFIER_H
#define KNNCLASSIFIER_H

#include <opencv2\ml.hpp>

#include "AbstractClassifier.h"

class kNNClassifier : public AbstractClassifier
{
public:
	kNNClassifier() {};
	kNNClassifier(QSharedPointer<BiovectorBase> base, int kNeigbours, float distThreshold);

	~kNNClassifier();

	void classify(Biovector &faceBiovector);

	PersonData getClassifiedPerson();

private:
	QSharedPointer<BiovectorBase> mBiovectorBase;

	PersonData *mClassifiedPerson;

	cv::Ptr < cv::ml::TrainData > mTrainData;
	cv::Ptr < cv::ml::KNearest > mKNN;
};

#endif