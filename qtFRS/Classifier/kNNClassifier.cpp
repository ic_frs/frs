#include "kNNClassifier.h"

kNNClassifier::kNNClassifier(QSharedPointer<BiovectorBase> base, int kNeigbours, float distThreshold)
{
	mDistThreshold = distThreshold;

	mBiovectorBase = base;

	mTrainData = cv::ml::TrainData::create(mBiovectorBase->getBiovectorsAsRowsMat(), cv::ml::ROW_SAMPLE, mBiovectorBase->getBiovectorLabels());

	mKNN = cv::ml::KNearest::create();
	mKNN->setIsClassifier(true);
	mKNN->setAlgorithmType(cv::ml::KNearest::Types::BRUTE_FORCE);
	mKNN->setDefaultK(kNeigbours);
	mKNN->train(mTrainData);
}

kNNClassifier::~kNNClassifier()
{
	delete mClassifiedPerson;
}

void kNNClassifier::classify(Biovector &faceBiovector)
{
	mClassifiedPerson = new PersonData();

	cv::Mat curBiovector(faceBiovector, true);

	std::vector < float > distance;
	std::vector < float > result;
	std::vector < float > response;

	int recognPersonId = mKNN->findNearest(curBiovector.t(), mKNN->getDefaultK(), result, response, distance);

	if (*std::max_element(distance.begin(), distance.end()) < mDistThreshold)
	{
		*mClassifiedPerson = mBiovectorBase->getCurPerson(recognPersonId, 0);
		qDebug() << "recPerson:" << mClassifiedPerson->personId;
	}
		
}

PersonData kNNClassifier::getClassifiedPerson()
{
	return *mClassifiedPerson;
}