#ifndef ABSTRACTCLASSIFIER_H
#define ABSTRACTCLASSIFIER_H

#include <QtCore>
#include <opencv2\core.hpp>
#include "BiovectorBase.h"

class AbstractClassifier
{
public:
	AbstractClassifier() {};
	virtual ~AbstractClassifier() {};

	virtual void classify(Biovector &faceBiovector) = 0;

	virtual PersonData getClassifiedPerson() = 0;

protected:
	float mDistThreshold;

};

#endif