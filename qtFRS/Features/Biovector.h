#ifndef BIOVECTOR_H
#define BIOVECTOR_H

#include <vector>
#include <assert.h>

class Biovector : public std::vector < float >
{

public:
	Biovector(void);

	Biovector(std::vector < float >);

	~Biovector(void);

	
	Biovector operator+(const Biovector&) const;

	void operator+=(const Biovector&);

	Biovector operator+(const float&) const;

	void operator+=(const float&);

	
	Biovector operator/(const Biovector&) const;
	
	void operator/=(const Biovector&);

	Biovector operator/(const float&) const;

	void operator/=(const float&);

	
	Biovector operator*(const Biovector&) const;

	void operator*=(const Biovector&);

	Biovector operator*(const float&) const;

	void operator*=(const float&);


	Biovector operator-(const Biovector&) const;

	void operator-=(const Biovector&);

	Biovector operator-(const float&) const;

	void operator-=(const float&);

	void concat(Biovector& b, float w);

	Biovector getSubVector(size_t index) const;
	
	size_t getSubVectorCount(void) const;

	float getSubVectorWeight(size_t index) const;


private:
	std::vector < size_t > _mSubVectors;
	std::vector < float > _mWeigths;
};

#endif