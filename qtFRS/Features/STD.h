#ifndef STD_H
#define STD_H

#include <opencv2\core\core.hpp>
#include "Biovector.h"
#include "AbstractExtractor.h"

class STDFeatureExtrator : public AbstractExtractor
{
public:
	STDFeatureExtrator(int width = 32, int height = 32, int _mWidthStep = 8, int _mHeightStep = 8);

	void extractFeatures(const cv::Mat &src, Biovector &dst);

private:
	int _mWinWidth;
	int _mWinHeight;
	int _mScanWidthStep;
	int _mScanHeightStep;
};

#endif //STD_H