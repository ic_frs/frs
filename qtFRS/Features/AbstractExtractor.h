#ifndef ABSTRACTEXTRACTOR_H
#define ABSTRACTEXTRACTOR_H

#include "Biovector.h"
#include <opencv2\core.hpp>

class AbstractExtractor
{
public:

	virtual ~AbstractExtractor() {};

	virtual void extractFeatures(const cv::Mat &src, Biovector &dst) = 0;
};

#endif