#include "STD.h"

STDFeatureExtrator::STDFeatureExtrator(int width, int height, int widthStep, int heightStep)
{
	_mWinWidth = width;
	_mWinHeight = height;
	_mScanWidthStep = widthStep;
	_mScanHeightStep = heightStep;
}

void STDFeatureExtrator::extractFeatures(const cv::Mat &source, Biovector &dst)
{
	int image_width = source.size().width;
	int image_height = source.size().height;

	std::vector<double> mean;
	std::vector<double> stddev;

	for (int h = 0; h < image_height - _mWinHeight; h += _mScanHeightStep)
	{
		for (int w = 0; w < image_width - _mWinWidth; w += _mScanWidthStep)
		{
			cv::Mat scan_region(source, cv::Range(w, w + _mWinWidth), cv::Range(h, h + _mWinHeight));
			cv::meanStdDev(scan_region, mean, stddev);

			for (int i = 0; i < (int)stddev.size(); ++i)
			{
				//dst.push_back(stddev[i]);
				dst.push_back(stddev[i] / 128.0);
			}
		}
	}
}