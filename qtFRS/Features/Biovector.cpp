#include "Biovector.h"
#include <algorithm>
#include <functional>
#include <iostream>
#include <iterator>

Biovector::Biovector(void)
{
}

Biovector::Biovector( std::vector<float> vec)
{
	this->resize(vec.size(), 0);
	std::copy(vec.begin(), vec.end(), this->begin());
}
Biovector::~Biovector(void)
{
}

Biovector Biovector::operator+( const Biovector& b) const
{
	assert(b.size() == this->size() && "b.size() != this->size()");

	Biovector res;
	res.resize(b.size());

	std::transform(this->begin(), this->end(), b.begin(), res.begin(), std::plus<float>());

	return res;
}

void Biovector::operator+=( const Biovector& b)
{
	assert(b.size() == this->size() && "b.size() != this->size()");

	std::transform(this->begin(), this->end(), b.begin(), this->begin(), std::plus<float>());
}

Biovector Biovector::operator+(const float& v) const
{
	std::vector<float> operation(this->size(), v);
	Biovector res;
	res.resize(this->size());
	std::transform(this->begin(), this->end(), operation.begin(), res.begin(), std::plus<float>());
	return res;
}

void Biovector::operator+=(const float& v)
{
	std::vector<float> operation(this->size(), v);
	std::transform(this->begin(), this->end(), operation.begin(), this->begin(), std::plus<float>());
}

 Biovector Biovector::operator/( const Biovector& b) const
 {
 	assert(b.size() == this->size() && "b.size() != this->size()");
 	
	Biovector res;
	res.resize(b.size());

 	std::transform(this->begin(), this->end(), b.begin(), res.begin(), std::divides<float>());
	return res;
 }

 void Biovector::operator/=( const Biovector& b)
 {
	 assert(b.size() == this->size() && "b.size() != this->size()");

	 std::transform(this->begin(), this->end(), b.begin(), this->begin(), std::divides<float>());
 }

 Biovector Biovector::operator/( const float& v) const
 {
	 std::vector<float> operation(this->size(), v);
	 Biovector res;
	 res.resize(this->size());
	 std::transform(this->begin(), this->end(), operation.begin(), res.begin(), std::divides<float>());
	 return res;
 }

 void Biovector::operator/=( const float& v)
 {
	 std::vector<float> operation(this->size(), v);
	 std::transform(this->begin(), this->end(), operation.begin(), this->begin(), std::divides<float>());
 }

 Biovector Biovector::operator*( const Biovector& b) const
 {
	 assert(b.size() == this->size() && "b.size() != this->size()");

	 Biovector res;
	 res.resize(b.size());

	 std::transform(this->begin(), this->end(), b.begin(), res.begin(), std::multiplies<float>());
	 return res;
 }

 void Biovector::operator*=( const Biovector& b)
 {
	 assert(b.size() == this->size() && "b.size() != this->size()");

	 std::transform(this->begin(), this->end(), b.begin(), this->begin(), std::multiplies<float>());
 }

 Biovector Biovector::operator*( const float& v) const
 {
	 std::vector<float> operation(this->size(), v);
	 Biovector res;
	 res.resize(this->size());
	 std::transform(this->begin(), this->end(), operation.begin(), res.begin(), std::multiplies<float>());
	 return res;
 }

 void Biovector::operator*=( const float& v)
 {
	 std::vector<float> operation(this->size(), v);
	 std::transform(this->begin(), this->end(), operation.begin(), this->begin(), std::multiplies<float>());
 }

 Biovector Biovector::operator-( const Biovector& b) const
 {
	 assert(b.size() == this->size() && "b.size() != this->size()");

	 Biovector res;
// 	 res.resize(b.size());
// 
// 	 std::transform(this->begin(), this->end(), b.begin(), res.begin(), std::minus<float>());

	 std::vector<float> tmp = *this;

	 for(size_t i = 0; i < b.size(); ++i)
		res.push_back( tmp[i] - b[i] );

	 return res;
 }

 void Biovector::operator-=( const Biovector& b)
 {
	 assert(b.size() == this->size() && "b.size() != this->size()");

	 std::transform(this->begin(), this->end(), b.begin(), this->begin(), std::minus<float>());
 }

 Biovector Biovector::operator-( const float& v) const
 {
	 std::vector<float> operation(this->size(), v);
	 Biovector res;
	 res.resize(this->size());
	 std::transform(this->begin(), this->end(), operation.begin(), res.begin(), std::minus<float>());
	 return res;
 }

 void Biovector::operator-=( const float& v)
 {
	 std::vector<float> operation(this->size(), v);
	 std::transform(this->begin(), this->end(), operation.begin(), this->begin(), std::minus<float>());
 }

 void Biovector::concat( Biovector& b,  float w )
 {
	std::copy(b.begin(), b.end(), std::back_inserter(*this));
	_mSubVectors.push_back(b.size());
	_mWeigths.push_back(w);
 }

 Biovector Biovector::getSubVector( size_t index ) const
 {
	 assert(index < _mSubVectors.size() && "index fail");
	 
	 size_t seek = 0;
	 for (int i = 0; i < (int)index; ++i)
		seek += _mSubVectors[i];

	 size_t length = _mSubVectors[index];

	 std::vector<float> sub(length, 0);
	 std::copy( this->begin() + seek, this->begin() + seek + length, sub.begin() );
	 
	 Biovector ret(sub);
	 return ret;
 }

 size_t Biovector::getSubVectorCount( void ) const
 {
	return _mSubVectors.size();
 }

 float Biovector::getSubVectorWeight( size_t index ) const
 {
	assert(index < _mWeigths.size() && "index fail");
	return _mWeigths[index];
 }