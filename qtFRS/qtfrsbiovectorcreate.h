#ifndef QTFRSBIOVECTORCREATE_H
#define QTFRSBIOVECTORCREATE_H

#include <QDialog>
#include "GlobalOptions.h"

namespace Ui {
	class qtFRSBiovectorCreate;
}

class qtFRSBiovectorCreate : public QDialog
{
	Q_OBJECT

public:
	explicit qtFRSBiovectorCreate(QWidget *parent = 0);
	~qtFRSBiovectorCreate();

private:
	Ui::qtFRSBiovectorCreate *ui;

	//��� �������� ����
	QString _faceImagesDirectory;

signals:
	//������ ���������������� ������� ������ ������� ����������
	void clikedCreateBiovectorBaseButton(QString& directoryPath);
	void clikedReadBiovectorBaseButton(QString& directoryPath);

public slots:
	//���� ��� ����������� �������� �������� ����������� � ���������� ����
	void showBiovectorBaseMessage(QString& message);
	
	void createBiovectorBase();
	void readBiovectorBase();
};

#endif // QTFRSBIOVECTORCREATE_H
