#include "qtfrsmainwindow.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	OPTIONS_META = GlobalOptions::InitConfigurationScheme();

	QApplication a(argc, argv);
	qtFRSMainWindow mw;
	mw.show();

	
	return a.exec();
}
