#ifndef GLOBALOPTIONS_H
#define GLOBALOPTIONS_H
#define NOMINMAX

#include <string>
#include <list>
#include <windows.h>

#include <QSettings>
#include <QFileDialog>
#include <QDebug>
#include <QFile>
#include <QMessageBox>

#include <GlobalOptionsScheme.h>


#define DLIB_JPEG_SUPPORT
#define DLIB_PNG_SUPPORT

#define OPTIONS_META GlobalOptions::PARAMETERS_META
#define OPTIONS GlobalOptions::PARAMETERS_DATA


using namespace globalOptionsScheme;

class GlobalOptions : public QObject
{
	Q_OBJECT

public:
	static GlobalOptions* instance;
	static GlobalOptions* getInstance();

	//���� ��������� ����� ������������
	static GroupList InitConfigurationScheme();
	static GroupList PARAMETERS_META;
	static GroupHash PARAMETERS_DATA;

	//������ ��������� ������������
	static ManagerState MANAGER_STATE;
	static QString CONFIG_MANAGER_FOLDER;
	static QString CONFIG_MANAGER_PATH;
	static QStringList CONFIG_KEYS;
	static QStringList CONFIG_NAMES;
	static QStringList CONFIG_PATHS;
	static QString CURRENT_KEY;
	static QString CURRENT_MANAGER_KEY;
	static QString CURRENT_DEFAULT_KEY;
	static QString CURRENT_PATH;
	static QString CURRENT_MANAGER_PATH;
	static QString CURRENT_DEFAULT_PATH;


	static void searchConfigManager();
	static bool loadConfigManager(QString managerPath, bool warnExistance = true);
	static bool loadCurrentConfiguration(QString configPath, bool warnExistance = true);
	static bool isManagerSchemeValid(QString managerPath);
	static bool ensureConfigurationValid(QString configPath, bool updateScheme = true, bool sendWarnings = false);

	static std::list<std::string> VIDEO_EXTENSIONS;
	static std::list<std::string> IMAGE_EXTENSIONS;
	static float FPS;
	static float FRAME_PERIOD;
	static int THREAD_COUNT;
	static int OUTPUT_FRAME_LIMIT;
	
signals:
	void issueOpenConfigManager();
};

#endif