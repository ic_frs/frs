#include "GlobalOptions.h"

//��������� ���������
ManagerState GlobalOptions::MANAGER_STATE = ManagerState::NotLoaded;
QString GlobalOptions::CONFIG_MANAGER_FOLDER = "Configurations";
QString GlobalOptions::CONFIG_MANAGER_PATH = "Configurations/FRSConfigManagerData.conf";
QStringList GlobalOptions::CONFIG_KEYS;
QStringList GlobalOptions::CONFIG_NAMES;
QStringList GlobalOptions::CONFIG_PATHS;
QString GlobalOptions::CURRENT_KEY;
QString GlobalOptions::CURRENT_MANAGER_KEY;
QString GlobalOptions::CURRENT_DEFAULT_KEY;
QString GlobalOptions::CURRENT_PATH;
QString GlobalOptions::CURRENT_MANAGER_PATH;
QString GlobalOptions::CURRENT_DEFAULT_PATH;
GroupList GlobalOptions::PARAMETERS_META;
GroupHash GlobalOptions::PARAMETERS_DATA;

//������ ���������
std::list<std::string> GlobalOptions::VIDEO_EXTENSIONS = { ".avi", ".mp4" };
std::list<std::string> GlobalOptions::IMAGE_EXTENSIONS = { ".jpg", ".bmp", ".png" };
float GlobalOptions::FPS;
float GlobalOptions::FRAME_PERIOD;
int GlobalOptions::THREAD_COUNT = 4;
int GlobalOptions::OUTPUT_FRAME_LIMIT = 20;

//��������
GlobalOptions* GlobalOptions::instance;
GlobalOptions* GlobalOptions::getInstance()
{
	if (!instance)
	{
		instance = new GlobalOptions();
	}
	return instance;
}

/*
��������� ���������� ��������� (������������� � main.cpp)
*/
GroupList GlobalOptions::InitConfigurationScheme()
{
	KeyList global;
	global.append(KeyPair("BiovectorBaseFolder", Parameter("Biovector Base Folder", ParameterType::String, true)));
	global.append(KeyPair("ThreadWriterOutput", Parameter("Thread Writer Output", ParameterType::String, true)));
	global.append(KeyPair("ThreadDecoderInput", Parameter("Thread Decoder Input", ParameterType::String, true)));
	global.append(KeyPair("RecognizedPersonsStorage", Parameter("Recognized Persons Storage", ParameterType::String, true)));
	global.append(KeyPair("UnrecognizedPersonsStorage", Parameter("Unrecognized Persons Storage", ParameterType::String, true)));
	global.append(KeyPair("FaceDetectorCascadeFile", Parameter("Face Detector Cascade File", ParameterType::String, true)));
	global.append(KeyPair("MinFaceDetectSize", Parameter("Min Face Detect Size", ParameterType::Int, 1, 100, true)));
	global.append(KeyPair("NearNeighboursCount", Parameter("Near Neighbours Count", ParameterType::Int, 1, 100, true)));
	global.append(KeyPair("FPS", Parameter("Frames per Second", ParameterType::Float, 1, 100, true)));
	global.append(KeyPair("RecognizerThreshold", Parameter("Recognizer Threshold", ParameterType::Float, 0.1, 500, true)));
	global.append(KeyPair("CreateRotated", Parameter("Create Rotated", ParameterType::Bool, true)));
	global.append(KeyPair("CreateBiovectors", Parameter("Create Biovectors", ParameterType::Bool, true)));
	global.append(KeyPair("ReduceDimensions", Parameter("Reduce Dimensions", ParameterType::Bool, true)));
	global.append(KeyPair("ExecutionType", Parameter("Execution Type", ParameterType::Bool, true)));
	global.append(KeyPair("GetFPS", Parameter("Get FPS automatically (Videofile)", ParameterType::Bool, true)));

	GroupList groups;
	groups.append(GroupPair("Global", global));

	return groups;
}

/*
��������� ���� �������� ��������� �� ���������� ���� � ��������� ������ ��������� � ����������
*/
bool GlobalOptions::loadConfigManager(QString managerPath, bool sendWarnings)
{
	//�������� � ������������ ���� �� ����� ���������
	if (!QFile(managerPath).exists())
	{
		if (sendWarnings)
			QMessageBox(
				QMessageBox::Warning,
				QObject::tr("Failed to read manager data!"),
				QObject::tr("The provided path to manager configuration is invalid"))
				.exec();
		MANAGER_STATE = ManagerState::NotLoaded;
		return false;
	}

	//�������� � ������������ ����� �������� ���������
	if (!isManagerSchemeValid(managerPath))
	{
		if (sendWarnings)
			QMessageBox(
				QMessageBox::Warning,
				QObject::tr("Configuration manager data is invalid!"),
				QObject::tr("The file provided as manager configuration is invalid. A new file will be created automatically upon using \"New Configuration...\" option."))
				.exec();
		MANAGER_STATE = ManagerState::NotLoaded;
		return false;
	}

	//�������� ���� �������� ��������� � ���� �����
	if ((new QFileInfo(managerPath))->absolutePath() != (new QFileInfo(CONFIG_MANAGER_PATH))->absolutePath())
	{
		QFile::remove(CONFIG_MANAGER_PATH);
		QFile::copy(managerPath, CONFIG_MANAGER_PATH);
	}
		
	QSettings *mSettings = new QSettings(CONFIG_MANAGER_PATH, QSettings::IniFormat);
	
	//������� ������ ���� ������, ��� � ����� ������������
	CONFIG_KEYS.clear();
	CONFIG_NAMES.clear();
	CONFIG_PATHS.clear();

	CONFIG_KEYS = mSettings->childGroups();
	CONFIG_KEYS.removeAt(CONFIG_KEYS.indexOf("manager"));

	foreach(const QString &key, CONFIG_KEYS) {
		//��������, ��� ������������ �������� ���������� ������ (������� ��� �������������)
		ensureConfigurationValid(mSettings->value(key + "/path").toString(), true, false);
		CONFIG_NAMES.append(mSettings->value(key + "/name").toString());
		CONFIG_PATHS.append(mSettings->value(key + "/path").toString());
	}

	//������� ������ ������� ������������
	CURRENT_KEY = mSettings->value("manager/currentKey").toString();
	CURRENT_PATH = mSettings->value(CURRENT_KEY + "/path").toString();

	CURRENT_DEFAULT_KEY = mSettings->value("manager/defaultKey").toString();
	CURRENT_DEFAULT_PATH = mSettings->value(CURRENT_DEFAULT_KEY + "/path").toString();

	CURRENT_MANAGER_KEY = mSettings->value("manager/managerKey").toString();
	CURRENT_MANAGER_PATH = mSettings->value(CURRENT_MANAGER_KEY + "/path").toString();

	//�������� ��������, ��������� ��� ������� ���� ������������, �� ��� ��������� ��������, ��� ������������ ������ � ������
	MANAGER_STATE = ManagerState::ConfigPendingValidation;
	return true;
}

/*
��������� ������� ������������ � ����������� ������
*/
bool GlobalOptions::loadCurrentConfiguration(QString configPath, bool sendWarnings)
{
	//�������� � ������������ ���� �� ����� ������������
	if (!QFile(configPath).exists())
	{
		if (sendWarnings)
			QMessageBox(
				QMessageBox::Warning,
				QObject::tr("Failed to read configuration data!"),
				QObject::tr("The provided path to currently selected configuration is invalid"))
				.exec();
		MANAGER_STATE = ManagerState::ConfigPendingValidation;
		return false;
	}

	//�������� � ������������ ����� ������������
	if (!ensureConfigurationValid(configPath, false, false))
	{
		if (sendWarnings)
			QMessageBox(
			QMessageBox::Warning,
			QObject::tr("Configuration data is invalid!"),
			QObject::tr("The file provided as current configuration is invalid. Consider recreating the configuration in the Configuration manager."))
			.exec();
		MANAGER_STATE = ManagerState::ConfigPendingValidation;
		return false;
	}

	//�������� ������ ������������ � ����������� ������
	QSettings *settings = new QSettings(configPath, QSettings::IniFormat);
	foreach(const GroupPair groupPair, OPTIONS_META)
	{
		OPTIONS[groupPair.first] = KeyHash();
		foreach(KeyPair keyPair, groupPair.second)
		{
			switch (keyPair.second.type) {
			case ParameterType::Int:
			{
				char *temp;
				long value_tol = strtol(settings->value(groupPair.first + "/" + keyPair.first).toString().toStdString().c_str(), &temp, 0);
				OPTIONS[groupPair.first][keyPair.first].int_data = (int)value_tol;
				break;
			}
			case ParameterType::Float:
			{
				char *temp;
				long value_tof = strtof(settings->value(groupPair.first + "/" + keyPair.first).toString().toStdString().c_str(), &temp);
				OPTIONS[groupPair.first][keyPair.first].float_data = (int)value_tof;
				break;
			}
			case ParameterType::String:
			{
				QString value = settings->value(groupPair.first + "/" + keyPair.first).toString();
				OPTIONS[groupPair.first][keyPair.first].string_data = value.toStdString();
				break;
			}
			case ParameterType::Bool:
			{
				QString value = settings->value(groupPair.first + "/" + keyPair.first).toString();
				OPTIONS[groupPair.first][keyPair.first].bool_data = (value == "1");
				break;
			}
			default:
				break;
			}
		}
	}

	

	//��������� ������ � ������
	MANAGER_STATE = ManagerState::ConfigValid;
	return true;
}

/*
��������� ���� �� ������� ������������ ������ � ���� �� ����� ������������
*/
bool GlobalOptions::isManagerSchemeValid(QString managerPath)
{
	QSettings *mSettings = new QSettings(managerPath, QSettings::IniFormat);
	QStringList groups = mSettings->childGroups();
	if (groups.contains("manager", Qt::CaseSensitive) && groups.count() > 1)
	{
		mSettings->beginGroup("manager");
		QStringList keys = mSettings->childKeys();
		mSettings->endGroup();

		//���� ������� ������������
		QString currentKey;
		if (keys.contains("currentKey", Qt::CaseSensitive))
		{
			currentKey = mSettings->value("manager/currentKey").toString();
			mSettings->beginGroup(currentKey);
			if (mSettings->childKeys().contains("path", Qt::CaseSensitive))
			{
				QString currentPath = mSettings->value("path").toString();
				ensureConfigurationValid(currentPath, true);
			}
			else return false;
			mSettings->endGroup();
		}
		else return false;

		//���� ������������ �� ���������
		QString defaultKey;
		if (keys.contains("defaultKey", Qt::CaseSensitive))
		{
			defaultKey = mSettings->value("manager/defaultKey").toString();
			if (defaultKey != currentKey)
			{
				mSettings->beginGroup(defaultKey);
				if (mSettings->childKeys().contains("path", Qt::CaseSensitive))
				{
					QString defaultPath = mSettings->value("path").toString();
					ensureConfigurationValid(defaultPath, true);
				}
				else return false;
				mSettings->endGroup();
			}
		}
		else return false;

		//���� ������������, �������� � ���������
		QString managerKey;
		if (keys.contains("managerKey", Qt::CaseSensitive))
		{
			managerKey = mSettings->value("manager/managerKey").toString();
			if (managerKey != currentKey && managerKey != defaultKey)
			{
				mSettings->beginGroup(managerKey);
				if (mSettings->childKeys().contains("path", Qt::CaseSensitive))
				{
					QString defaultPath = mSettings->value("path").toString();
					ensureConfigurationValid(defaultPath, true);
				}
				else return false;
				mSettings->endGroup();
			}
		}
		else return false;
	}
	else return false;
	
	return true;
}

/*
��������� ���� ������������ �� ������� ������������ ������ � ������������ ������
E��� ���������� updateScheme, �� � ������ ���������� ������/������������ ������ ��������� � ��������� ������ �������� � ������ ���������� true
����� � ������ ������ �������� �� ���� � ���������� false
*/
bool GlobalOptions::ensureConfigurationValid(QString configPath, bool updateScheme, bool sendWarnings)
{
	//�������� ���� �������� �� ������������
	QSettings *settings = new QSettings(configPath, QSettings::IniFormat);
	bool is_invalid = false;

	foreach(const GroupPair groupPair, OPTIONS_META)
	{
		foreach(const KeyPair keyPair, groupPair.second)
		{
			QString value = settings->value(groupPair.first + "/" + keyPair.first).toString();
			switch (keyPair.second.type) {
			case ParameterType::Int:
			{
				try {
					//��������� ������ �� �������, �� ������� �����, �� �������� � �������
					if (value.toStdString().empty())
						throw 1;
					errno = 0; char *temp;
					long value_tol = strtol(value.toStdString().c_str(), &temp, 0);
					if (temp == value.toStdString().c_str() || *temp != '\0' || ((value_tol == LONG_MIN || value_tol == LONG_MAX) && errno == ERANGE))
						throw 2;
					if (value_tol > keyPair.second.max)
						throw 3;
					if (value_tol < keyPair.second.min)
						throw 4;
				}
				catch (int error)
				{
					if (!updateScheme)
					{
						QString errorMessage;
						switch (error) {
						case 1:
							errorMessage = QObject::tr("Required parameter is empty: ");
							break;
						case 2:
							errorMessage = QObject::tr("Failed to parse integer parameter: ");
							break;
						case 3:
							errorMessage = QObject::tr("Parameter exceeds maximum value: ");
							break;
						case 4:
							errorMessage = QObject::tr("Parameter is less than minimum value: ");
							break;
						}
						if (sendWarnings)
							QMessageBox(
								QMessageBox::Warning,
								QObject::tr("Configuration file error"),
								errorMessage + keyPair.second.name)
								.exec();
						return false;
					}
					else
					{
						settings->setValue(groupPair.first + "/" + keyPair.first, "");
						is_invalid = true;
					}
				}
				break;
			}
			case ParameterType::Float:
			{
				
				try {
					//��������� ������ �� �������, �� ������� �����, �� �������� � �������
					if (value.toStdString().empty())
						throw 1;
					errno = 0; char *temp;
					float value_tof = strtof(value.toStdString().c_str(), &temp);
					if (temp == value.toStdString().c_str() || *temp != '\0' || errno == ERANGE)
						throw 2;
					if (value_tof > keyPair.second.max)
						throw 3;
					if (value_tof < keyPair.second.min)
						throw 4;
				}
				catch (int error)
				{
					if (!updateScheme)
					{
						QString errorMessage;
						switch (error) {
						case 1:
							errorMessage = QObject::tr("Required parameter is empty: ");
							break;
						case 2:
							errorMessage = QObject::tr("Failed to parse floating point parameter: ");
							break;
						case 3:
							errorMessage = QObject::tr("Parameter exceeds maximum value: ");
							break;
						case 4:
							errorMessage = QObject::tr("Parameter is less than minimum value: ");
							break;
						}
						if (sendWarnings)
							QMessageBox(
								QMessageBox::Warning,
								QObject::tr("Input error"),
								errorMessage + keyPair.second.name)
								.exec();
						return false;
					}
					else
					{
						settings->setValue(groupPair.first + "/" + keyPair.first, "");
						is_invalid = true;
					}
				}
				break;
			}
			case ParameterType::String:
			{
				try {
					//��������� ������ �� �������
					if (value.toStdString().empty())
						throw 1;
				}
				catch (int error)
				{
					if (!updateScheme)
					{
						QString errorMessage;
						switch (error) {
						case 1:
							errorMessage = QObject::tr("Required parameter is empty: ");
							break;
						}
						if (sendWarnings)
							QMessageBox(
								QMessageBox::Warning,
								QObject::tr("Input error"),
								errorMessage + keyPair.second.name)
								.exec();
						return false;
					}
					else
					{
						settings->setValue(groupPair.first + "/" + keyPair.first, "");
						is_invalid = true;
					}
				}
				break;
			}
			case ParameterType::Bool:
				if (value.toStdString().empty())
					settings->setValue(groupPair.first + "/" + keyPair.first, "0");
				break;
			default:
				break;
			}
		}
	}

	if (is_invalid && sendWarnings)
		QMessageBox(
			QMessageBox::Information,
			QObject::tr("Configuration was invalid"),
			"Configuration file appeared to include invalid structure or data values that have been replaced with empty strings. The configuration requires editing before usage.")
			.exec();
	return true;
}