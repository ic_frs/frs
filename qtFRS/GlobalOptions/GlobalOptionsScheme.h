/*
���������� ����� ������� ����� �������, ��� ��� ��������� � �����, � ������ ������������ � � ������ ����������� �� ������ ������
����� �������, ����� ����� �������� ���������� � ���� ���������� ���������
*/
#include <opencv2\core.hpp>
#include <opencv2\highgui.hpp>
#include <opencv2\imgproc.hpp>


namespace globalOptionsScheme{

	//��������� ��� �������� ������ ����������, �������� ��������, ���� � ������� ����������� ����������, ������� ��������
	enum ManagerState {
		NotLoaded,
		ConfigPendingValidation,
		ConfigValid
	};
	enum ParameterType {
		String,
		Float,
		Bool,
		Int
	};
	class Parameter {
	public:
		Parameter(){}
		~Parameter(){}
		Parameter(QString _name, ParameterType _type, bool _isRequired = true)
		{
			name = _name; type = _type;
			min = -1; max = -1;
			isRequired = _isRequired;
		}
		Parameter(QString _name, ParameterType _type, float _min, float _max, bool _isRequired = true)
		{
			name = _name; type = _type;
			min = _min; max = _max;
			isRequired = _isRequired;
		}

		QString name;
		ParameterType type;
		float min;
		float max;
		bool isRequired;
	};

	typedef QPair<QString, Parameter> KeyPair;
	typedef QList<KeyPair> KeyList;
	typedef QPair<QString, KeyList> GroupPair;
	typedef QList<GroupPair> GroupList;

	struct ConfigData {
		std::string string_data;
		union {
			long int_data;
			float float_data;
			bool bool_data;
		};
	};

	typedef QHash<QString, ConfigData> KeyHash;
	typedef QHash<QString, KeyHash> GroupHash;

	//��������� �������� �����
	struct FrameStruct {
		cv::Mat frameMat;
		int id;

		FrameStruct() {}
		FrameStruct(cv::Mat _frameMat, int _id)
		{
			frameMat = _frameMat.clone();
			id = _id;
		}
		FrameStruct(cv::Mat _frameMat)
		{
			frameMat = _frameMat.clone();
			id = -1;
		}
	};

	typedef QList<QImage> QImageList;

	typedef std::list<FrameStruct> FrameList;
	typedef QMap<int, FrameStruct> FrameMap;

	struct PersonData
	{
		int personId;
		int faceId;
		int biovectorMatId;
		std::string name;
		std::string surname;
		std::string fathersName;
		std::string imagePath;
	};

	//��������� �������� ����
	struct FaceStruct {
		cv::Mat faceMat;
		cv::Rect rect;
		PersonData personData;
		int frameIndex;

		FaceStruct() {}
		FaceStruct(cv::Mat _faceMat, cv::Rect _rect, int _frameIndex)
		{
			faceMat = _faceMat.clone();
			rect = _rect;
			frameIndex = _frameIndex;
		}
		void setPersonData(PersonData _recPerson)
		{
			personData = PersonData(_recPerson);
		}
		bool isRecognized()
		{
			return !personData.imagePath.empty();
		}
	};

	typedef std::list<FaceStruct> FaceList;
	typedef QMap<int, FaceList> FaceMap;


}