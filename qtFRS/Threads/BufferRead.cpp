#include "BufferRead.h"

std::mutex BufferRead::bufLock;
std::condition_variable_any BufferRead::bufVariable;

BufferRead::BufferRead()
{
	
}


BufferRead::~BufferRead()
{

}

FrameStruct BufferRead::GetFrame()
{
	FrameStruct topFrame;

	if (BufferRead::bufLock.try_lock())
	{	
		//BufferRead::bufVariable.wait(bufLock);

		std::cout << "Thread num " << std::this_thread::get_id() << ": Lock readLock" << std::endl;

		if (_mBuffer.size() != 0)
		{
			std::cout << "Thread num " << std::this_thread::get_id() << ": Get top frame" << std::endl;

			topFrame = _mBuffer.front();
			_mBuffer.pop_front();
		}

		std::cout << "Thread num " << std::this_thread::get_id() << ": Unlock readLock" << std::endl;

		BufferRead::bufLock.unlock();
	}
	else
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}

	
	return topFrame;
}

void BufferRead::AddFrame(cv::Mat img)
{
	std::unique_lock<std::mutex> lock(BufferRead::bufLock);

	cv::Mat cloneImage = img.clone();
	
	std::cout << "Thread num " << std::this_thread::get_id() << ": Lock readLock" << std::endl;

	FrameStruct curFrame = FrameStruct(++_mFrameId, cloneImage);

	_mBuffer.push_back(curFrame);

	std::cout << "Thread num " << std::this_thread::get_id() << ": ReadBuf size = " << _mBuffer.size() << std::endl;
	std::cout << "Thread num " << std::this_thread::get_id() << ": Unlock redLock" << std::endl << std::endl;

	//BufferRead::bufVariable.notify_one();
}

int BufferRead::GetSize()
{
	return _mBuffer.size();
}

void BufferRead::SetStreamEnd()
{
	_mIsStreamEnd = true;
}

bool BufferRead::IsStreamEnd()
{
	return _mIsStreamEnd;
}
