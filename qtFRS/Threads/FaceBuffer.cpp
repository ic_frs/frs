#include "FaceBuffer.h"

FaceBuffer::FaceBuffer() {}
FaceBuffer::~FaceBuffer() {}

/*
��������� ������� ���� ������������ �����
*/
FaceStruct FaceBuffer::GetFace(int frameId)
{
	QWriteLocker locker(&lock);

	FaceStruct topFace;
	if (mFaceMap[frameId].size() != 0)
	{
		topFace = mFaceMap[frameId].front();
		mFaceMap[frameId].pop_front();
	}

	return topFace;
}

/*
��������� ������� ��� ������� ����� � ������
*/
std::list<FaceStruct> FaceBuffer::GetFrameFaceList()
{
	QWriteLocker locker(&lock);
	std::list<FaceStruct> topFaceList;
	if (mFaceMap.first().size() != 0)
	{
		topFaceList = mFaceMap.first();
		mFaceMap.erase(mFaceMap.begin());
	}
	return topFaceList;
}

/*
�������� ������ ������� �����
*/
void FaceBuffer::RemoveFirstFrame()
{
	QWriteLocker locker(&lock);
	if (mFaceMap.size() != 0)
	{
		mFaceMap.erase(mFaceMap.begin());
	}
}

/*
���������� ������ �����/��� ����������������� �����
*/
void FaceBuffer::AddFace(int frameId, FaceStruct faceStruct)
{
	QWriteLocker locker(&lock);
	FaceStruct copyFaceStruct(faceStruct);
	mFaceMap[frameId].push_back(copyFaceStruct);
}

int FaceBuffer::framesCount()
{
	QReadLocker locker(&lock);
	return mFaceMap.size();
}

int FaceBuffer::facesCount(int frameId)
{
	QReadLocker locker(&lock);
	return mFaceMap[frameId].size();
}

int FaceBuffer::generalSize()
{
	QReadLocker locker(&lock);
	int size = 0;
	foreach(const std::list<FaceStruct> list, mFaceMap)
	{
		size += list.size();
	}
	return size;
}