#pragma once
#include <thread>
#include <iostream>
#include <opencv2\highgui.hpp>
#include "BufferWrite.h"



class ThreadWrite
{
public:
	ThreadWrite(BufferWrite *buf, FrameList *outbuf, std::string fileName);
	~ThreadWrite();

	void runThread();
	void stopThread();

	//static cv::VideoWriter writer;

private:
	void writeFrame();

	std::thread *workingThread;

	BufferWrite *writeBuf;

	FrameList *outBuf;

	cv::VideoWriter writer;

	std::string fileName;

};

