#ifndef DECODERRUNNABLE_H
#define DECODERRUNNABLE_H

#include <QtCore>
#include <opencv2\highgui.hpp>
#include "GlobalOptions.h"

#include "FrameBuffer.h"

class DecoderRunnable : public QRunnable
{

public:
	DecoderRunnable(
		QSharedPointer<FrameBuffer> readBuf,
		QSharedPointer<FrameBuffer> outBuf,
		QSharedPointer<cv::VideoCapture> capture,
		QSharedPointer<QMutex> readMutex,
		QSharedPointer<QWaitCondition> readWaitCondition,
		QSharedPointer<QSemaphore> threadSemaphore);
	~DecoderRunnable();

	void setToFinish();

	void run();

private:
	QSharedPointer<FrameBuffer> mReadBuf;
	QSharedPointer<FrameBuffer> mOutputBuffer;
	QSharedPointer<cv::VideoCapture> mVideoCapture;
	QSharedPointer<QMutex> mReadMutex;
	QSharedPointer<QWaitCondition> mReadWaitCondition;
	QSharedPointer<QSemaphore> mThreadSemaphore;

	bool isSetToFinish = false;
};

#endif