#include "BufferWrite.h"

std::mutex BufferWrite::bufLock;

BufferWrite::BufferWrite()
{

}


BufferWrite::~BufferWrite()
{
	
}

/*
�������� ��������� ����� � ������
*/
FrameStruct BufferWrite::GetFrame()
{
	FrameStruct topFrame;

	BufferWrite::bufLock.lock();

	//std::cout << "Write thread " << std::this_thread::get_id() << ": Lock writeLock" << std::endl;

	if (buffer.size() != 0)
	{
		//std::cout << "Write thread " << std::this_thread::get_id() << ": Get top frame for writing" << std::endl;

		topFrame = buffer.front();
		buffer.pop_front();

		std::cout << "Thread num " << std::this_thread::get_id() << ": WriteBuf size = " << buffer.size() << std::endl;
	}

	std::cout << "Write thread " << std::this_thread::get_id() << ": Unlock writeLock" << std::endl;

	BufferWrite::bufLock.unlock(); 

	return topFrame;
}

void BufferWrite::AddFrame(FrameStruct curFrame, std::vector < cv::Mat > facesImgs, std::vector < cv::Mat > recFaces)
{
	BufferWrite::bufLock.lock();

	FrameStruct copyFrame(curFrame);

	cv::Mat recognVisualPart;

	cv::Mat widenCurFrame(copyFrame.second.rows + recognVisual.rows, copyFrame.second.cols, CV_8UC3);
	copyFrame.second.copyTo(widenCurFrame.rowRange(0, copyFrame.second.rows).colRange(0, copyFrame.second.cols));
	copyFrame.second = widenCurFrame;

	for (int i = 0; i < (int)facesImgs.size(); ++i)
	{
		if (recFaces[i].channels() == 1)
			cv::cvtColor(recFaces[i], recFaces[i], CV_GRAY2BGR);
		cv::resize(recFaces[i], recFaces[i], cv::Size(80, 80));

		if (facesImgs[i].channels() == 1)
			cv::cvtColor(facesImgs[i], facesImgs[i], CV_GRAY2BGR);
		cv::resize(facesImgs[i], facesImgs[i], cv::Size(80, 80));

		recognVisualPart = recognVisual.rowRange(0, recognVisual.rows).colRange(0, recognVisual.cols - recFaces[i].cols).clone();
		recognVisualPart.copyTo(recognVisual.rowRange(0, recognVisual.rows).colRange(recFaces[i].cols, recognVisual.cols));

		recFaces[i].copyTo(recognVisual.rowRange(0, recFaces[i].rows).colRange(0, recFaces[i].cols));
		facesImgs[i].copyTo(recognVisual.rowRange(recFaces[i].rows, recFaces[i].rows + facesImgs[i].rows).colRange(0, facesImgs[i].cols));
	}

	recognVisual.copyTo(copyFrame.second.rowRange(copyFrame.second.rows - recognVisual.rows, copyFrame.second.rows).colRange(0, copyFrame.second.cols));

	std::cout << "Thread num " << std::this_thread::get_id() << ": Lock writeLock" << std::endl;
	
	buffer.push_back(copyFrame);

	std::cout << "Thread num " << std::this_thread::get_id() << ": Push frame " << copyFrame.first << std::endl;
	std::cout << "Thread num " << std::this_thread::get_id() << ": WriteBuf size = " << buffer.size() << std::endl;
	std::cout << "Thread num " << std::this_thread::get_id() << ": Unlock writeLock" << std::endl << std::endl;

	BufferWrite::bufLock.unlock();
}

int BufferWrite::GetSize()
{
	return buffer.size();
}

void BufferWrite::SetStreamEnd()
{
	isStreamEnd = true;
}

bool BufferWrite::IsStreamEnd()
{
	return isStreamEnd;
}

void BufferWrite::SortBuf()
{
	std::unique_lock<std::mutex> lock(BufferWrite::bufLock);

	buffer.sort(&BufferWrite::SortFunc);
}

bool BufferWrite::SortFunc(const FrameStruct &first, const FrameStruct &second)
{
	return (first.first < second.first);
}