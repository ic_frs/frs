#ifndef FACEBUFFER_H
#define FACEBUFFER_H

#include <list>
#include <mutex>
#include <iostream>
#include <thread>

#include <QReadWriteLock>

#include <opencv2\core.hpp>
#include <opencv2\highgui.hpp>
#include <opencv2\imgproc.hpp>

#include "GlobalOptions.h"


class FaceBuffer
{
public:
	FaceBuffer();
	~FaceBuffer();

	FaceStruct GetFace(int frameId);
	std::list<FaceStruct> GetFrameFaceList();
	
	void RemoveFirstFrame();
	void AddFace(int frameId, FaceStruct faceStruct);
	

	int framesCount();
	int facesCount(int frameId);
	int generalSize();

	QReadWriteLock lock;

private:
	FaceMap mFaceMap;

};

#endif