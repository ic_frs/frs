#ifndef FRAMEPROCESSINGRUNNABLE_H
#define FRAMEPROCESSINGRUNNABLE_H

#include <QtCore>
#include <opencv2\highgui.hpp>
#include "GlobalOptions.h"

#include "FaceDetector.h"

#include "Biovector.h"
#include "BiovectorBase.h"

#include "Recognizer.h"

#include "Tracker/Tracker.h"

#include "FrameBuffer.h"
#include "FaceBuffer.h"

class FrameProcessingRunnable : public QObject, public QRunnable
{
	Q_OBJECT

public:
	FrameProcessingRunnable(
		QSharedPointer<FrameBuffer> readBuf,
		QSharedPointer<FrameBuffer> outBuf,
		QSharedPointer<FaceBuffer> faceBuf,
		QSharedPointer<Recognizer> recognizer,
		QSharedPointer<QMutex> readMutex,
		QSharedPointer<QWaitCondition> readWaitCondition,
		QSharedPointer<QSemaphore> threadSemaphore);
	~FrameProcessingRunnable();

	void setToFinish();

	void run();

private:
	FaceDetector *mFaceDetector;
	QSharedPointer<Recognizer> mRecognizer;

	QSharedPointer<FrameBuffer> mReadBuffer;
	QSharedPointer<FrameBuffer> mOutputBuffer;
	QSharedPointer<FaceBuffer> mFaceBuffer;
	QSharedPointer<QMutex> mReadMutex;
	QSharedPointer<QWaitCondition> mReadWaitCondition;
	QSharedPointer<QSemaphore> mThreadSemaphore;

	bool isSetToFinish = false;

signals:
	void appendTextToMainWindowLog(QString& text);
};

#endif