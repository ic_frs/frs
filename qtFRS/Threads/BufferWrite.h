#ifndef BUFFERWRITE_H
#define BUFFERWRITE_H

#include <list>
#include <mutex>
#include <iostream>
#include <thread>

#include <opencv2\core.hpp>
#include <opencv2\highgui.hpp>
#include <opencv2\imgproc.hpp>

#include "GlobalOptions/GlobalOptions.h"
//typedef std::pair<int, cv::Mat> FrameStruct;

using namespace std;

class BufferWrite
{
public:
	BufferWrite();
	~BufferWrite();

	FrameStruct GetFrame();
	void AddFrame(FrameStruct curFrame, vector < cv::Mat > faceImgs, vector < cv::Mat > recFaces);
	void SetStreamEnd();
	void SortBuf();

	int GetSize();

	bool IsStreamEnd();

	static bool SortFunc(const FrameStruct &first, const FrameStruct &second);

	static mutex bufLock;
	static mutex recognLock;

	/*TODO: ���������� �� �������������� ������� ����*/
	cv::Mat recognVisual = cv::Mat::zeros(160, 1280, CV_8UC3);

private:
	list<FrameStruct> buffer;

	bool isStreamEnd = false;

	

};

#endif