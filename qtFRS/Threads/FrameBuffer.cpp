#include "FrameBuffer.h"

FrameBuffer::FrameBuffer() {}
FrameBuffer::~FrameBuffer() {}

/*
��������� ������� ����� �� ������
*/
FrameStruct FrameBuffer::GetFrame()
{
	QWriteLocker locker(&lock);

	FrameStruct topFrame;
	if (mFrameMap.size() != 0)
	{
		topFrame = mFrameMap.first();
		mFrameMap.erase(mFrameMap.begin());
	}

	return topFrame;
}

/*
���������� ������ �����/��� ����������������� �����
*/
void FrameBuffer::AddFrame(cv::Mat frameMat)
{
	QWriteLocker locker(&lock);
	FrameStruct frameStruct = FrameStruct(frameMat, ++mFrameId);
	mFrameMap.insert(frameStruct.id, frameStruct);
}

void FrameBuffer::AddFrame(FrameStruct frameStruct)
{
	QWriteLocker locker(&lock);
	FrameStruct copyFrameStruct(frameStruct);
	mFrameMap.insert(frameStruct.id, frameStruct);
}


int FrameBuffer::size()
{
	QReadLocker locker(&lock);
	return mFrameMap.size();
}


/*
���������� ������ (DEPRECATED)
*/
//void FrameBuffer::SortBuf()
//{
//	QWriteLocker locker(&lock);
//	mFrameMap.sort(&FrameBuffer::SortFunc);
//}
//
//bool FrameBuffer::SortFunc(const FrameStruct &first, const FrameStruct &second)
//{
//	return (first.id < second.id);
//}