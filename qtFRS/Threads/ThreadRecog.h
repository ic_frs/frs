#ifndef THREADRECOG_H
#define THREADRECOG_H

#include <opencv2\highgui\highgui.hpp>

#include <thread>
#include <chrono>
#include <filesystem>
#include <boost\filesystem.hpp>

#include "FaceEnchansement/FaceDetector.h"
#include "Features/Biovector.h"
#include "BufferRead.h"
#include "BufferWrite.h"
#include "BiovectorBase/BiovectorBase.h"
#include "Recognizer/Recognizer.h"
#include "GlobalOptions/GlobalOptions.h"

using namespace std;

class qtFRSMainWindow;

class ThreadRecog
{

public:
	ThreadRecog(BufferRead* bufR, BufferWrite* bufW, QImageList* fBuf);
	~ThreadRecog();

	void runThread(Recognizer& recognizer);
	void stopThread();

private:
	void processFrame(Recognizer& recognizer);

	std::thread* _mWorkingThread;

	BufferRead* _mReadBuf;
	BufferWrite* _mWriteBuf;
	QImageList* _mFacesBuf;
};

#endif