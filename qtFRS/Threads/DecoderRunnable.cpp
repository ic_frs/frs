#include "DecoderRunnable.h"

DecoderRunnable::DecoderRunnable(
	QSharedPointer<FrameBuffer> readBuf,
	QSharedPointer<FrameBuffer> outBuf,
	QSharedPointer<cv::VideoCapture> capture,
	QSharedPointer<QMutex> readMutex,
	QSharedPointer<QWaitCondition> readWaitCondition,
	QSharedPointer<QSemaphore> threadSemaphore)
{
	mReadBuf = readBuf;
	mOutputBuffer = outBuf;
	mVideoCapture = capture;
	mReadMutex = readMutex;
	mReadWaitCondition = readWaitCondition;
	mThreadSemaphore = threadSemaphore;
}

DecoderRunnable::~DecoderRunnable()
{
	//������, ����� ���������� ��������� ������ ����� ���������
	mReadMutex->lock();
	mReadWaitCondition->wakeAll();
	mReadMutex->unlock();

	mThreadSemaphore->acquire();
}

void DecoderRunnable::run()
{
	mThreadSemaphore->release();

	cv::Mat frame;

	while (!isSetToFinish)
	{
		if (mOutputBuffer->size() < GlobalOptions::OUTPUT_FRAME_LIMIT && mReadBuf->size() < GlobalOptions::THREAD_COUNT)
		{
			*mVideoCapture >> frame;

			if (frame.empty())
			{
				break;
			}

			//�������� ���� � ����� � �������� ������� � ��� �����������
			mReadMutex->lock();
			mReadBuf->AddFrame(frame);
			mReadWaitCondition->wakeOne();
			mReadMutex->unlock();
		}

		QThread::msleep((int)(GlobalOptions::FRAME_PERIOD * 1000));
	}
}

void DecoderRunnable::setToFinish()
{
	isSetToFinish = true;
}