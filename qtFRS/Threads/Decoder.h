#ifndef DECODER_H
#define DECODER_H

#include <iostream>

#include <opencv2\highgui.hpp>

#include "BufferRead.h"
#include "GlobalOptions/GlobalOptions.h"

using namespace std;


class Decoder
{
public:
	Decoder(BufferRead* buf, QSharedPointer<cv::VideoCapture> capture, string streamName);
	~Decoder();

	void runThread();
	void stopThread();

	void readStream();

private:
	thread* _mWorkingThread;

	BufferRead* _mReadBuf;
	QSharedPointer<cv::VideoCapture> _mReader;
};

#endif