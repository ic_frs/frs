#ifndef BUFFERREAD_H
#define BUFFERREAD_H

#include <list>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <iostream>
#include <chrono>

#include <opencv2\core.hpp>

#include "GlobalOptions/GlobalOptions.h"
//typedef std::pair<int, cv::Mat> FrameStruct;

using namespace std;

class BufferRead
{
public:
	BufferRead();
	~BufferRead();

	FrameStruct GetFrame();

	void AddFrame(cv::Mat img);
	void SetStreamEnd();
	
	bool IsStreamEnd();

	int GetSize();

	static mutex bufLock;
	static condition_variable_any bufVariable;

private:
	list < FrameStruct > _mBuffer;

	int _mFrameId = 0;

	bool _mIsStreamEnd = false;

};

#endif