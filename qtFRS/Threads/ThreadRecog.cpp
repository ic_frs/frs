#include "ThreadRecog.h"

ThreadRecog::ThreadRecog(BufferRead *bufR, BufferWrite *bufW, QImageList* fBuf)
{
	_mReadBuf = bufR;
	_mWriteBuf = bufW;	
	_mFacesBuf = fBuf;
}


ThreadRecog::~ThreadRecog()
{
	//delete _mWorkingThread;
}

void ThreadRecog::runThread(Recognizer& recognizer)
{
	cout << "Thread num " << this_thread::get_id() << ": Start new thread" << endl << endl;
	
	_mWorkingThread = new thread(&ThreadRecog::processFrame, *this, ref(recognizer));
}

void ThreadRecog::stopThread()
{
	cout << "Thread num " << this_thread::get_id() << ": Stop" << endl;
	
	_mWorkingThread->join();
}

void ThreadRecog::processFrame(Recognizer& recognizer)
{
	cout << "Thread num " << this_thread::get_id() << ": Run" << endl;

	FaceDetector detector(OPTIONS["Global"]["MinFaceDetectSize"].int_data, OPTIONS["Global"]["FaceDetectorCascadeFile"].string_data);

	cv::Mat curFaceImg;
	cv::Mat recPersonFaceImg;
	cv::Mat frameFace;

	vector < cv::Mat > recPersonsFacesImgs;
	vector < cv::Mat > frameFaces;

	int truePositiveRecognized = 0;
	int falsePositiveRecognized = 0;
	int faceDetectionCount = 0;

	path curPersonVideoPath;
	string videoFileName;
	int curPersonNum;

	if (OPTIONS["Global"]["ExecutionType"].bool_data == 1)
	{
		curPersonVideoPath.append(OPTIONS["Global"]["ThreadDecoderInput"].string_data);
		videoFileName = curPersonVideoPath.filename().string();
		curPersonNum = stoi(curPersonVideoPath.remove_filename().filename().string());
	}
	
	while (true)
	{
		if ((_mReadBuf->IsStreamEnd()) && (_mReadBuf->GetSize() == 0))
		{
			_mWriteBuf->SetStreamEnd();
			break;
		}

		FrameStruct curFrame(_mReadBuf->GetFrame());

		if (!curFrame.second.empty())
		{
			recPersonsFacesImgs.clear();
			frameFaces.clear();
			
			PersonData recPerson;

			cv::Mat imageOPENCVcopy;
			curFrame.second.copyTo(imageOPENCVcopy);

			vector < cv::Point2f > curFacePoints;
			vector < cv::Rect > curFacesRects;

			int facesCount = detector.detect(curFrame.second);
			curFacesRects = detector.getFacesRects();

			if (OPTIONS["Global"]["ExecutionType"].bool_data == 1)
			{
				faceDetectionCount += facesCount;
			}
			
			chrono::system_clock::time_point today = chrono::system_clock::now();
			time_t tt;
			tt = chrono::system_clock::to_time_t(today);

			char curTime[80];
			char curDay[80];

			strftime(curTime, 80, "%Hh_%Mm_%Ss", localtime(&tt));
			strftime(curDay, 80, "%d_%B_%Y", localtime(&tt));

			string fileName = curTime;
			string folderNameStr;

			for (int i = 0; i < facesCount; ++i)
			{
				cv::rectangle(imageOPENCVcopy, curFacesRects[i], CV_RGB(255, 0, 0), 2, CV_AA, 0);
				cv::Mat faceImageOPENCV(curFrame.second, curFacesRects[i]);
				cv::resize(faceImageOPENCV, faceImageOPENCV, cv::Size(50, 50));
				cv::cvtColor(faceImageOPENCV, faceImageOPENCV, cv::COLOR_BGR2RGB);
				QImage faceImageQT((uchar*)faceImageOPENCV.data, faceImageOPENCV.cols, faceImageOPENCV.rows, faceImageOPENCV.step, QImage::Format_RGB888);

				_mFacesBuf->push_back(faceImageQT);

				detector.getFace(i, curFaceImg, curFacePoints);

				if (!recPerson.imagePath.empty())
				{					
					recPersonFaceImg = cv::imread(recPerson.imagePath, CV_LOAD_IMAGE_GRAYSCALE);
					recPersonsFacesImgs.push_back(recPersonFaceImg);

					frameFace = curFrame.second(curFacesRects[i]);
					frameFaces.push_back(frameFace);

					if (OPTIONS["Global"]["ExecutionType"].bool_data == 1)
					{
						if (curPersonNum == (recPerson.personId + 1))
						{
							++truePositiveRecognized;
						}
						else
						{
							++falsePositiveRecognized;
						}
					}
					
					folderNameStr = OPTIONS["Global"]["RecognizedPersonStorage"].string_data + "\\" + to_string(recPerson.personId + 1);
				}
				else
				{
					folderNameStr = OPTIONS["Global"]["UnRecognizedPersonStorage"].string_data;
				}
				/*
				folderNameStr += "\\" + string(curDay);
				path folderName(folderNameStr);
				create_directories(folderName);

				imwrite(folderNameStr + "\\" + fileName + "_" + to_string(this_thread::get_id().hash()) + ".jpg", curFrame.second);
				*/
			}

			FrameStruct copyFrame(curFrame);
			copyFrame.second = imageOPENCVcopy;
			_mWriteBuf->AddFrame(copyFrame, frameFaces, recPersonsFacesImgs);
		}
	}

	if (OPTIONS["Global"]["ExecutionType"].bool_data == 1)
	{
		fstream truePositiveRecognizedFile;
		fstream falsePositiveRecognizedFile;
		fstream faceDetectionCountFile;
		
		truePositiveRecognizedFile.open(curPersonVideoPath.string() + "\\truePositive_" + videoFileName + "_" + to_string(this_thread::get_id().hash()) + ".txt", fstream::out);
		falsePositiveRecognizedFile.open(curPersonVideoPath.string() + "\\falsePositive_" + videoFileName + "_" + to_string(this_thread::get_id().hash()) + ".txt", fstream::out);
		faceDetectionCountFile.open(curPersonVideoPath.string() + "\\faceDetection_" + videoFileName + "_" + to_string(this_thread::get_id().hash()) + ".txt", fstream::out);

		truePositiveRecognizedFile	<< truePositiveRecognized	<< endl;
		falsePositiveRecognizedFile << falsePositiveRecognized	<< endl;
		faceDetectionCountFile		<< faceDetectionCount		<< endl;

		truePositiveRecognizedFile.close();
		falsePositiveRecognizedFile.close();
		faceDetectionCountFile.close();
	}
}