#include "Decoder.h"

Decoder::Decoder(BufferRead* buf, QSharedPointer<cv::VideoCapture> capture, string streamName)
{
	_mReadBuf = buf;

	//�������������� ����������
	_mReader = capture;
	_mReader.create();
	_mReader->open(streamName);
}

Decoder::~Decoder()
{
	//delete _mWorkingThread;
}

void Decoder::runThread()
{
	cout << "Thread num " << this_thread::get_id() << ": Start decoder thread" << endl << endl;

	_mWorkingThread = new thread(&Decoder::readStream, *this);
}

void Decoder::stopThread()
{
	cout << "Thread num " << this_thread::get_id() << ": Stop decoder thread" << endl;

	_mWorkingThread->join();
	delete _mWorkingThread;
}

void Decoder::readStream()
{
	cv::Mat frame;

	while (true)
	{
		*_mReader >> frame;

		if (frame.empty())
		{
			_mReadBuf->SetStreamEnd();
			break;
		}

		//TODO: ������� ���������� ������� ������, ��������� � ���� = ���
		if (_mReadBuf->GetSize() < 50)
		{
			_mReadBuf->AddFrame(frame);
		}
		
		Sleep(OPTIONS["Global"]["ReadStreamPause"].int_data);
		//Sleep(333);
		//cv::waitKey(333);
		//cv::waitKey(OPTIONS["Global"]["ReadStreamPause"].int_data);
		
	}
}