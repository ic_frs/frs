#include "FrameProcessingRunnable.h"

FrameProcessingRunnable::FrameProcessingRunnable(
	QSharedPointer<FrameBuffer> readBuf,
	QSharedPointer<FrameBuffer> outBuf,
	QSharedPointer<FaceBuffer> faceBuf,
	QSharedPointer<Recognizer> recognizer,
	QSharedPointer<QMutex> readMutex,
	QSharedPointer<QWaitCondition> readWaitCondition,
	QSharedPointer<QSemaphore> threadSemaphore)
{
	mReadBuffer = readBuf;
	mOutputBuffer = outBuf;
	mFaceBuffer = faceBuf;
	mRecognizer = recognizer;
	mReadMutex = readMutex;
	mReadWaitCondition = readWaitCondition;
	mThreadSemaphore = threadSemaphore;
}

FrameProcessingRunnable::~FrameProcessingRunnable()
{
	mThreadSemaphore->acquire();
}

void FrameProcessingRunnable::run()
{
	mThreadSemaphore->release();

	mFaceDetector = new FaceDetector(OPTIONS["Global"]["MinFaceDetectSize"].int_data, OPTIONS["Global"]["FaceDetectorCascadeFile"].string_data);
	
	cv::Mat curFaceImg;
	cv::Mat recPersonFaceImg;
	cv::Mat frameFace;
	std::vector<cv::Mat> recPersonsFacesImgs;
	std::vector<cv::Mat> frameFaces;

	while (!isSetToFinish)
	{	
		//������� ������� ����� � ������
		mReadMutex->lock();
		if (mReadBuffer->size() == 0)
			mReadWaitCondition->wait(mReadMutex.data());
		mReadMutex->unlock();

		if (isSetToFinish) break;
		
		FrameStruct currentFrame(mReadBuffer->GetFrame());
		if (!currentFrame.frameMat.empty())
		{
			emit appendTextToMainWindowLog(tr("Frame Processing Thread: frame id " + currentFrame.id)); //TODO ����������� � ����� (�� �������������� �������)
			recPersonsFacesImgs.clear();
			frameFaces.clear();

			PersonData recPerson;

			cv::Mat imageOPENCVcopy;
			currentFrame.frameMat.copyTo(imageOPENCVcopy);

			std::vector<cv::Point2f> curFacePoints;
			std::vector<cv::Rect> curFacesRects;

			
			curFacesRects = mFaceDetector->getFacesRects();
			trk::Tracker nTracker(mFaceDetector->getFacesRects());
			vector <int> curNumTr = nTracker.lastTracks.Nums;

			//��������� �����
			int facesCount = mFaceDetector->detect(currentFrame.frameMat);
			curFacesRects = mFaceDetector->getFacesRects();
			nTracker.tracking(curFacesRects, curNumTr);

			//������� �����
			std::chrono::system_clock::time_point today = std::chrono::system_clock::now();
			time_t tt = std::chrono::system_clock::to_time_t(today);
			char curTime[80], curDay[80];
			strftime(curTime, 80, "%Hh_%Mm_%Ss", localtime(&tt));
			strftime(curDay, 80, "%d_%B_%Y", localtime(&tt));

			//������������ ������ ���� � ��������� ��� � ����� ���
			for (int i = 0; i < facesCount; ++i)
			{
				//���������
				mFaceDetector->getFace(i, curFaceImg, curFacePoints);
				mRecognizer->recognize(curFaceImg, curFacePoints, recPerson);

				//������� � �����
				cv::rectangle(imageOPENCVcopy, curFacesRects[i], CV_RGB(255, 0, 0), 2, CV_AA, 0);

				//��������� ������
				char text_buf[32];
				//����� � ������
				itoa(curNumTr[i], text_buf, 10);
				//����� ������
				putText(imageOPENCVcopy, text_buf, cvPoint(curFacesRects[i].x, curFacesRects[i].y), CV_FONT_HERSHEY_COMPLEX, 1.0, cvScalar(0, 0, 255));
				
				//���������� �� ����� 
				cv::Mat faceImageOPENCV(currentFrame.frameMat, curFacesRects[i]);
				FaceStruct faceStruct(faceImageOPENCV, curFacesRects[i], currentFrame.id);

				if (!recPerson.imagePath.empty())
				{
					faceStruct.setPersonData(recPerson);
				}

				mFaceBuffer->AddFace(currentFrame.id, faceStruct);			
			}

			FrameStruct copyFrame(imageOPENCVcopy, currentFrame.id);
			mOutputBuffer->AddFrame(copyFrame);
		}
	}

	delete mFaceDetector;
}

void FrameProcessingRunnable::setToFinish()
{
	isSetToFinish = true;
}