#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <list>
#include <mutex>
#include <iostream>
#include <thread>

#include <QReadWriteLock>

#include <opencv2\core.hpp>
#include <opencv2\highgui.hpp>
#include <opencv2\imgproc.hpp>

#include "GlobalOptions.h"


class FrameBuffer
{
public:
	FrameBuffer();
	~FrameBuffer();

	FrameStruct GetFrame();
	void AddFrame(cv::Mat frameMat);
	void AddFrame(FrameStruct frameStruct);

	int size();

	QReadWriteLock lock;
	int mFrameId = 0;

private:
	//FrameList mBuffer;
	FrameMap mFrameMap;
};

#endif