#include "ThreadWrite.h"

//VideoWriter writer;

ThreadWrite::ThreadWrite(BufferWrite *buf, FrameList *obuf, string fName)
{
	writeBuf = buf;
	outBuf = obuf;
	fileName = fName;
	//cv::namedWindow("FRS", CV_WINDOW_NORMAL);
}


ThreadWrite::~ThreadWrite()
{
	//delete workingThread;
}

void ThreadWrite::runThread()
{
	cout << "Main thread " << this_thread::get_id() << ": Start writing thread" << endl << endl;

	workingThread = new thread(&ThreadWrite::writeFrame, *this);
}

void ThreadWrite::stopThread()
{
	cout << "Main thread " << this_thread::get_id() << ": Stop writing thread" << endl;

	workingThread->join();
}

void ThreadWrite::writeFrame()
{
	cout << "Writing thread " << this_thread::get_id() << ": Write frame" << endl;

	//FrameStruct curFrame;

	while (true)
	{
		FrameStruct curFrame;
		curFrame = writeBuf->GetFrame();

		if (!curFrame.second.empty())
			break;
	}

	while (true)
	{
		if ((writeBuf->IsStreamEnd()) && (writeBuf->GetSize() == 0))
		{
			break;
		}		
		else if ((!writeBuf->IsStreamEnd() && writeBuf->GetSize() >= ((int)thread::hardware_concurrency() - 2)) || (writeBuf->IsStreamEnd() && writeBuf->GetSize() != 0))
		{
			writeBuf->SortBuf();

			FrameStruct curFrame;
			curFrame = writeBuf->GetFrame();

			if (!curFrame.second.empty())
			{
				//imshow("FRS", curFrame.second);
				outBuf->push_back(curFrame);
				cv::waitKey(1);
			}
		}
	}
}