#include "BiovectorBase.h"

BiovectorBase::BiovectorBase(QObject *parent) : QObject(parent)
{ 
	mBiovectorsAsRowsMat = new cv::Mat;
	mBiovectorsLabels = new cv::Mat;

	mPersonsCnt = 0; 
}

BiovectorBase::~BiovectorBase()
{
	clustersData.clear();
	delete mBiovectorsAsRowsMat;
	delete mBiovectorsLabels;
}

void BiovectorBase::createBivectorBase(const std::string &srcPath, FaceDetector &detector, std::vector<QSharedPointer<FaceImageProcessor>> &faceProcs)
{
	cv::Mat baseImage;
	cv::Mat curFace;

	std::fstream biovectorFile;

	path basePath(srcPath);

	recursive_directory_iterator baseItr(basePath);

	while (baseItr != recursive_directory_iterator())
	{
		file_status curStatus = baseItr->status();

		if (is_regular_file(curStatus))
		{
			bool isValidImageExtension = find(GlobalOptions::IMAGE_EXTENSIONS.begin(), GlobalOptions::IMAGE_EXTENSIONS.end(), extension(*baseItr)) != GlobalOptions::IMAGE_EXTENSIONS.end();

			if (isValidImageExtension)
			{
				baseImage = cv::imread(baseItr->path().string(), CV_LOAD_IMAGE_GRAYSCALE);
				int facesCount = detector.detect(baseImage);

				if (facesCount != 0)
				{
					path curImageBiovectorPath(baseItr->path().string() + ".txt");
					remove(curImageBiovectorPath);

					Biovector curFaceBiovector;
					std::vector < cv::Point2f > curFacePoints;

					detector.getFace(0, curFace, curFacePoints);
					for (auto &faceProc : faceProcs)
					{
						faceProc->processFace(curFace, curFacePoints, curFaceBiovector);
					}

					if (curFaceBiovector.size() == 0)
					{
						std::string stdM = "Biovector is 0";
						QString m = QString::fromStdString(stdM);
						biovectorBaseMessageAppear(m);
						continue;
					}

					biovectorFile.open(baseItr->path().string() + ".txt", std::fstream::out);

					for (auto &curFaceBiovectorFeature : curFaceBiovector)
					{
						biovectorFile << curFaceBiovectorFeature << std::endl;
					}

					biovectorFile.close();

					std::string stdM = "Create biovector " + baseItr->path().string();
					QString m = QString::fromStdString(stdM);
					biovectorBaseMessageAppear(m);
				}
				else
				{
					std::string stdM = "Attention: No face found! " + baseItr->path().string();
					QString m = QString::fromStdString(stdM);
					biovectorBaseMessageAppear(m);
				}
			}
			
		}

		++baseItr;
	}

	std::string stdM = "\nBiovectors have created\n";
	QString m = QString::fromStdString(stdM);
	biovectorBaseMessageAppear(m);
}

void BiovectorBase::readBiovectorFile(const std::string fileName, std::vector < float > &biovec)
{
	std::fstream personBiovectorFile;
	personBiovectorFile.open(fileName, std::fstream::in);

	float feature;

	while (true)
	{
		personBiovectorFile >> feature;

		if (personBiovectorFile.eof())
		{
			break;
		}

		biovec.push_back(feature);
	}

	personBiovectorFile.close();
}

bool sortByNum(const path first, const path second)
{
	return std::stoi(first.filename().string()) < std::stoi(second.filename().string());
}

void BiovectorBase::readBiovectorBase(const std::string &srcPath)
{
	if (mBiovectorsAsRowsMat != nullptr && mBiovectorsLabels != nullptr)
	{
		mBiovectorsAsRowsMat->release();
		mBiovectorsLabels->release();
	}

	path basePath(srcPath);
	directory_iterator baseDirIter(basePath);
	directory_iterator baseDirIterDimensionSize(basePath);

	std::vector < float > curPersonBiovector;

	while (baseDirIterDimensionSize != directory_iterator())
	{
		if (is_directory(baseDirIterDimensionSize->status()))
		{
			directory_iterator curDirIter(baseDirIterDimensionSize->path());
			while (curDirIter != directory_iterator())
			{
				if (is_regular_file(curDirIter->status()))
				{
					if (extension(*curDirIter) == ".txt") //�������� ��� �����������
					{
						readBiovectorFile(curDirIter->path().string(), curPersonBiovector);
						break;
					}
				}

				++curDirIter;
			}

			if (!curPersonBiovector.empty())
				break;
		}

		++baseDirIterDimensionSize;
	}

	mBiovectorsAsRowsMat->create(cv::Size(curPersonBiovector.size(), 0), CV_32FC1);

	std::vector < path > directoriesNames;

	while (baseDirIter != directory_iterator())
	{
		if (is_directory(baseDirIter->status()))
			directoriesNames.push_back(baseDirIter->path());

		++baseDirIter;
	}

	std::sort(directoriesNames.begin(),
			  directoriesNames.end(),
			  [](const path first, const path second)
			  {
			      return std::stoi(first.filename().string()) < std::stoi(second.filename().string());
			  });

	int personNum = 0;
	int faceNum = 0;

	mBiovectorsLabels = new cv::Mat();

	for (auto &curDirName : directoriesNames)
	{
		faceNum = 0;
		std::vector < PersonData > personCluster;

		directory_iterator curDirIter(curDirName);
		PersonData curPersonData;

		while (curDirIter != directory_iterator())
		{
			if (is_regular_file(curDirIter->status()))
			{
				if (extension(*curDirIter) == ".txt") //�������� ��� �����������
				{
					mBiovectorsLabels->push_back(personNum);
					curPersonBiovector.clear();

					readBiovectorFile(curDirIter->path().string(), curPersonBiovector);

					cv::Mat biovectorMat(curPersonBiovector, true);
					cv::transpose(biovectorMat, biovectorMat);

					mBiovectorsAsRowsMat->resize(mBiovectorsAsRowsMat->rows + 1);
					biovectorMat.row(0).copyTo(mBiovectorsAsRowsMat->row(mPersonsCnt));

					curPersonData.imagePath = curDirIter->path().string().substr(0, curDirIter->path().string().size() - extension(*curDirIter).size());
					curPersonData.personId = personNum;
					curPersonData.faceId = faceNum;
					curPersonData.biovectorMatId = mPersonsCnt;

					personCluster.push_back(curPersonData);

					std::string stdM = "Read biovector" + curDirIter->path().string();
					QString m = QString::fromStdString(stdM);
					biovectorBaseMessageAppear(m);

					++faceNum;
					++mPersonsCnt;
				}
			}

			++curDirIter;
		}

		clustersData.push_back(personCluster);
		++personNum;
	}

	cv::transpose(*mBiovectorsLabels, *mBiovectorsLabels);

	std::string stdM = "\nBiovectors have read\n";
	QString m = QString::fromStdString(stdM);
	biovectorBaseMessageAppear(m);
}

void BiovectorBase::reduceDimensions(AbstractDimensionReduction *dimensionReductor)
{
	Biovector curBiovector;
	mBiovectorsAsRowsMat->row(0).copyTo(curBiovector);
	curBiovector = dimensionReductor->reduce(curBiovector);

	cv::Mat* reduceBiovectorCollection = new cv::Mat(0, curBiovector.size(), mBiovectorsAsRowsMat->type());

	for (int i = 0; i < mBiovectorsAsRowsMat->rows; ++i)
	{
		mBiovectorsAsRowsMat->row(i).copyTo(curBiovector);
		curBiovector = dimensionReductor->reduce(curBiovector);

		cv::Mat curBiovectorMat(curBiovector, true);
		cv::transpose(curBiovectorMat, curBiovectorMat);
		reduceBiovectorCollection->resize(reduceBiovectorCollection->rows + 1);
		curBiovectorMat.row(0).copyTo(reduceBiovectorCollection->row(i));
	}

	mBiovectorsAsRowsMat = reduceBiovectorCollection;
	qDebug() << mBiovectorsAsRowsMat->rows;
}

cv::Mat BiovectorBase::getBiovectorsAsRowsMat()
{
	return *mBiovectorsAsRowsMat;
}

cv::Mat BiovectorBase::getBiovectorLabels()
{
	return *mBiovectorsLabels;
}

int BiovectorBase::getClustersCnt()
{
	return clustersData.size();
}

int BiovectorBase::getClusterPersonsCnt(int clustreNum)
{
	return clustersData[clustreNum].size();
}

int BiovectorBase::getPersonsCnt()
{
	return mPersonsCnt;
}

int BiovectorBase::getBiovectorCollectionSize()
{
	return mBiovectorsAsRowsMat->rows;
}

PersonData BiovectorBase::getCurPerson(int clusterNum, int clusterPerson)
{
	return clustersData[clusterNum][clusterPerson];
}

//������ ���������� � �������������
void BiovectorBase::readPersonInfFile(const std::string fileName, std::vector < std::string > &persons)
{
	std::fstream personBiovectorFile;
	personBiovectorFile.open(fileName, std::fstream::in);

	std::string feature;

	while (true)
	{
		personBiovectorFile >> feature;

		if (personBiovectorFile.eof())
		{
			break;
		}

		persons.push_back(feature);
	}

	personBiovectorFile.close();
}

//������ ������������ ����������
void BiovectorBase::getPersonInf(const std::string &srcPath)
{
	path personInfPath(srcPath);
	directory_iterator personInfDirIter(personInfPath);
	directory_iterator personInfDirIterDimensionSize(personInfPath);
	
	std::vector <std::string> curPersonInf;

	while (personInfDirIterDimensionSize != directory_iterator())
	{
		if (is_directory(personInfDirIterDimensionSize->status()))
		{
			directory_iterator curDirIter(personInfDirIterDimensionSize->path());
			while (curDirIter != directory_iterator())
			{
				if (is_regular_file(curDirIter->status()))
				{
					if (extension(*curDirIter) == ".dat") //�������� ��� �����������
					{
						readPersonInfFile(curDirIter->path().string(), curPersonInf);
						break;
					}
				}
				
				++curDirIter;
			}

			if (!curPersonInf.empty())
				break;
		}

		++personInfDirIterDimensionSize;
	}

	std::vector < path > directoriesNames;

	while (personInfDirIter != directory_iterator())
	{
		if (is_directory(personInfDirIter->status()))
			directoriesNames.push_back(personInfDirIter->path());
		++personInfDirIter;
	}

	std::sort(directoriesNames.begin(),
			  directoriesNames.end(),
			  [](const path first, const path second)
			  {
			      return std::stoi(first.filename().string()) < std::stoi(second.filename().string());
			  });

	int personNum = 0;
	int faceNum = 0;

	for (auto &curDirName : directoriesNames)
	{
		faceNum = 0;
		
		std::vector < std::string > resultName;
		directory_iterator curDirIter(curDirName);
		QString curPersonName;

		while (curDirIter != directory_iterator())
		{
			if (is_regular_file(curDirIter->status()))
			{
				if (extension(*curDirIter) == ".dat") //�������� ��� �����������
				{
					curPersonInf.clear();
					std::string feature;
					std::fstream personBiovectorFile;
					personBiovectorFile.open(curDirIter->path().string(), std::fstream::in);
					getline(personBiovectorFile, feature, '\0');
					personBiovectorFile.close();
					curPersonName = QString::fromStdString(feature);
					std::string stdM = "Read person information " + curDirIter->path().string();
					QString m = QString::fromStdString(stdM);
					biovectorBaseMessageAppear(m);

					++faceNum;
					++mPersonsCnt;
				}
			}

			++curDirIter;
		}

		personNameCluster.push_back(curPersonName);
		++personNum;
	}
	std::string stdM = "\nperson information have read\n";
	QString m = QString::fromStdString(stdM);
	biovectorBaseMessageAppear(m);

}

/*���� �� �����*/
/*PersonData BiovectorBase::getByLabel(int label)
{
	int labelAccum = 0;

	for (int i = 0; i < (int)clustersData.size(); ++i)
	{
		labelAccum += clustersData[i].size();

		if (labelAccum > label)
		{
			for (int j = clustersData[i].size() - 1; j >= 0; --j)
			{
				--labelAccum;

				if (labelAccum == label)
				{
					return clustersData[i][j];
				}
			}
		}
	}
}*/