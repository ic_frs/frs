#ifndef BIOVECTORBASE_H
#define BIOVECTORBASE_H

#include <QtCore>

#include <opencv2\core.hpp>
#include <opencv2\highgui.hpp>
#include <fstream>
#include <boost\filesystem.hpp>

#include "AbstractDimensionReduction.h"

#include "FaceDetector.h"
#include "FaceImageProcessor.h"

#include "GlobalOptions.h"

using namespace boost::filesystem;

class BiovectorBase : public QObject
{
	Q_OBJECT

public:
	BiovectorBase(QObject *parent = 0);
	~BiovectorBase();

	void createBivectorBase(const std::string &srcPath, FaceDetector &detector, std::vector<QSharedPointer<FaceImageProcessor>> &faceProcs);
	void readBiovectorBase(const std::string &srcPath);
	void reduceDimensions(AbstractDimensionReduction* dimensionReductor);
	void readBiovectorFile(const std::string fileName, std::vector < float > &biovec);

	int getClustersCnt();
	int getClusterPersonsCnt(int clustreNum);
	int getPersonsCnt();
	int getBiovectorCollectionSize();

	cv::Mat getBiovectorsAsRowsMat();
	cv::Mat getBiovectorLabels();

	PersonData getCurPerson(int clusterNum, int clusterPerson);
	std::vector < QString > personNameCluster;
	void readPersonInfFile(const std::string fileName, std::vector < std::string > &persons);
	void getPersonInf(const std::string &srcPath);
	

signals:
	void biovectorBaseMessageAppear(QString& message);

private:
	std::vector<std::vector<PersonData>> clustersData;

	cv::Mat *mBiovectorsAsRowsMat;
	cv::Mat *mBiovectorsLabels;

	int mPersonsCnt;
};

#endif