#include "FaceDetector.h"

FaceDetector::FaceDetector(const int minSize, const std::string nameFaceClassificator)
{
	_mFaceTransform = new FaceImageTransform(150);
	_mShapeWrapper = new ShapeWrapper();
	_mFaceMinSize = minSize;

	if (!_mFaceCascade.load(nameFaceClassificator))
		std::cout << "\n Not loaded: " << nameFaceClassificator.c_str() << std::endl << std::endl;
	else
		std::cout << "\n Classificator loaded: " << nameFaceClassificator.c_str() << std::endl << std::endl;

}

FaceDetector::~FaceDetector()
{
	delete _mFaceTransform;
	delete _mShapeWrapper;
	
	_mDetectedDaces.clear();
	_mDetectedFacesPoints.clear();
}

int FaceDetector::detect(const cv::Mat &img)
{
	_mDetectedDaces.clear();
	_mDetectedFacesPoints.clear();
	_mDetectedFacesRects.clear();

	cv::Mat grayImg;

	if (img.channels() != 1)
		cvtColor(img, grayImg, CV_BGR2GRAY);
	else
		grayImg = img;

	_mFaceCascade.detectMultiScale(grayImg, _mDetectedFacesRects, 1.2, 2, 0 | CV_HAAR_FEATURE_MAX, cv::Size(_mFaceMinSize, _mFaceMinSize));

	//return _mDetectedFacesRects.size();

	std::vector <cv::Point2f> curFacePoints;
	cv::Mat curFace;

	for (int i = 0; i < (int)_mDetectedFacesRects.size(); ++i)
	{
		curFace = grayImg(_mDetectedFacesRects[i]);
		curFacePoints = _mShapeWrapper->getShape(curFace);

		curFace = _mFaceTransform->transformFaceImage(curFacePoints, curFace);

		if (!curFace.empty())
		{
			_mDetectedFacesPoints.push_back(curFacePoints);
			_mDetectedDaces.push_back(curFace);
		}
		else
		{
			_mDetectedFacesRects.erase(_mDetectedFacesRects.begin() + i);
		}
	}

	return _mDetectedDaces.size();
}

std::vector < cv::Rect > FaceDetector::getFacesRects()
{
	return _mDetectedFacesRects;
}

bool FaceDetector::getFace(const int index, cv::Mat& img, std::vector < cv::Point2f > &facePoints)
{
	if (index >= (int)_mDetectedDaces.size())
		return false;

	img = _mDetectedDaces[index].clone();
	facePoints = _mDetectedFacesPoints[index];
	
	return true;
}