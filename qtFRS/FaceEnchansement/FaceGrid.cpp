#include "FaceGrid.h"


FaceGrid::FaceGrid(int imageSize, size_t cellNumber, float scaleCoef, const cv::Mat& weightMap) 
{
	//createGrid(imageSize, cellNumber, scaleCoef, weightMap);
}

FaceGrid::~FaceGrid()
{
}

/*GridData FaceGrid::createGrid(int imageSize, size_t cellNumber, float scaleCoef, const cv::Mat& weightMap)
{
	//std::cout << "\n weightMap: " << weightMap;
	float size = ( imageSize / cellNumber ) * scaleCoef;
	float shift = ( ( size * float(cellNumber) ) - imageSize ) / float( cellNumber - 1 );

	float left, right;
	float top, bottom;

	for(size_t i = 0; i < cellNumber; ++i)
	{
		for(size_t j = 0; j < cellNumber; ++j)
		{
			left = i * size - shift * i;					
			top = j * size - shift * j;

			cv::Rect rc(left, top, size, size);
			cellsRects.push_back(rc);

			float weight;
			
			weight = calcWeight(weightMap, rc);

			grid.addCell(rc, weight);

			//std::cout << "\n" << i << "\t" << j << "\t" << weight;
		}
	}

	//std::cout << "\n imageSize: " << imageSize;
	//std::cout << "\n Cell Area: " << size * size;
	return grid;
}*/

std::vector<cv::Rect> FaceGrid::getRects( void )
{
	return cellsRects;
}

cv::Mat FaceGrid::getImageOfCell( cv::Mat& img, int index )
{
	return img(cellsRects[index]);
}

/*GridData FaceGrid::getGrid( void )
{
	return grid;
}*/

float FaceGrid::calcWeight( cv::Mat weightMap, cv::Rect rc )
{
	
	cv::Mat cell = weightMap(rc);
	float mean = cv::mean(cell)[0];

	return mean / 63.75;
}