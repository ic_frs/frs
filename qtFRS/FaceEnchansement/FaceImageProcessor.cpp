#include "FaceImageProcessor.h"

FaceImageProcessor::FaceImageProcessor()
{
	mFaceMask = new FaceImageMasking();
}

FaceImageProcessor::~FaceImageProcessor()
{
	if (mFaceMask)
		delete mFaceMask;
}

FaceImageProcessor::FaceImageProcessor(std::vector<QSharedPointer<AbstractFilter>> &filt, std::vector<QSharedPointer<AbstractExtractor>> &extr)
{
	mFaceMask = new FaceImageMasking();
	mFilters = filt;
	mExtractors = extr;
}

void FaceImageProcessor::zNormal(std::vector < float > &inputBioVector)
{
	std::vector<double> mean;
	std::vector<double> stddev;

	cv::meanStdDev(inputBioVector, mean, stddev);

	for (auto &feature : inputBioVector)
	{
		feature = (feature - mean[0]) / stddev[0];
	}
}

void FaceImageProcessor::processFace(const cv::Mat& img, std::vector < cv::Point2f >& facePoints, Biovector& dst)
{
	cv::Mat filtredImg;

	for (auto &curFilter : mFilters)
	{
		curFilter->process(img, filtredImg);
	}

	medianBlur(filtredImg, filtredImg, 5);
	equalizeHist(filtredImg, filtredImg);
	normalize(filtredImg, filtredImg, 0, 255, cv::NORM_MINMAX);
	mFaceMask->maskingByEyeBrow(facePoints, filtredImg);

	//imshow("filtredImg", filtredImg);
	cv::waitKey(1);

	Biovector dstPart;

	for (auto &curExtructor : mExtractors)
	{
		curExtructor->extractFeatures(filtredImg, dstPart);
		//zNormal(dstPart);
		dst.insert(dst.end(), dstPart.begin(), dstPart.end());
	}
}