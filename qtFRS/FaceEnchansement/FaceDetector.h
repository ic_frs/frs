#ifndef CPUFACEDETECTOR_H
#define CPUFACEDETECTOR_H

#include <opencv2\core.hpp>
#include <opencv2\objdetect.hpp>
#include <opencv2\imgproc.hpp>
#include <iostream>

#include "FaceImageTransform.h"
#include "ShapeWrapper.h"

class FaceDetector
{
public:
	FaceDetector(const int minSize, const std::string nameFaceClassificator);

	~FaceDetector();

	int detect(const cv::Mat &img);

	bool getFace(const int index, cv::Mat& img, std::vector < cv::Point2f > &facePoints);

	std::vector < cv::Rect > getFacesRects();

private:

	cv::CascadeClassifier _mFaceCascade;
	
	std::vector < cv::Mat > _mDetectedDaces;
	std::vector < cv::Rect > _mDetectedFacesRects;
	std::vector < std::vector < cv::Point2f > > _mDetectedFacesPoints;
	
	int _mFaceMinSize;

	FaceImageTransform* _mFaceTransform;
	ShapeWrapper* _mShapeWrapper;

};

#endif