#include "FaceImageTransform.h"

FaceImageTransform::FaceImageTransform(int imageWidth) : _mImgWidth(imageWidth)
{
}

FaceImageTransform::~FaceImageTransform(void)
{
}


cv::Mat FaceImageTransform::transformFaceImage(std::vector<cv::Point2f>& facePoints, cv::Mat faceImage)
{
	double angle = rotate(facePoints, faceImage);

	cv::Mat rotateImage;
	rotateImage = faceImage.clone();

	resize(facePoints, faceImage);
	cv::Mat scaleImage;
	scaleImage = faceImage.clone();

	return cropFace(facePoints, faceImage);
}

double FaceImageTransform::rotate( std::vector <cv::Point2f>& facePoints, cv::Mat& faceImage )
{
	cv::Point leftEye;
	cv::Point rightEye;

	getEyesCenters(facePoints, leftEye, rightEye);

	// ����� ����� �������
	cv::Point eyesCenter = cv::Point( int((leftEye.x + rightEye.x) * 0.5f), int((leftEye.y + rightEye.y) * 0.5f) );

	// ���� ����� �������
	double dy = (rightEye.y - leftEye.y);
	double dx = (rightEye.x - leftEye.x);
	double len = sqrt(dx * dx + dy * dy);
	double angle = atan2(dy, dx) * 180.0 / CV_PI; // ������� � �������
	
//	std::cout << "angle = " << angle << std::endl;

	cv::Mat rot_mat = cv::getRotationMatrix2D(eyesCenter, angle, 1.0);
	cv::Mat rotateImage;
	cv::warpAffine( faceImage, faceImage, rot_mat, cv::Size(faceImage.cols , faceImage.rows));

	/*-----------------������ ��������� �����----------*/
	// 1) ����� eyesCenter - ������ ���������:
	for (int i = 0; i < (int)facePoints.size(); ++i)
	{
		facePoints[i].x -= eyesCenter.x;
		facePoints[i].y -= eyesCenter.y;
	}

	// 2) ����������� ����� ���������� �����:
	double angleRad = -1 * angle * 3.14 / 180;
	cv::Point2f scaleFacePoint;
	allPointsRotate(eyesCenter, facePoints, angleRad);

	/*-------------------------------------------------*/

	return angle;
}

void FaceImageTransform::resize(std::vector <cv::Point2f>& facePoints, cv::Mat& faceImage)
{
	cv::Point leftEye;
	cv::Point rightEye;

	getEyesCenters(facePoints, leftEye, rightEye);

	double dist = sqrt( double(leftEye.x - rightEye.x) * double(leftEye.x - rightEye.x) + 
						double(leftEye.y - rightEye.y) * double(leftEye.y - rightEye.y) );

	double resize_koeff(80. / dist);

	//������������
	cv::Size size;
	size.width = int(faceImage.cols * resize_koeff);
	size.height = int(faceImage.rows * resize_koeff);

	for(int i = 0; i < (int)facePoints.size(); ++i)
	{
		facePoints[i].x *= resize_koeff;
		facePoints[i].y *= resize_koeff;
	}

	cv::resize(faceImage, faceImage, size, 0, 0, cv::INTER_CUBIC);
}

void FaceImageTransform::getEyesCenters(const std::vector<cv::Point2f>& facePoints, cv::Point& left, cv::Point& right)
{
	cv::Point leftEyeInner = facePoints[39];
	cv::Point leftEyeOuter = facePoints[36];
	 
	cv::Point rightEyeInner = facePoints[45];
	cv::Point rightEyeOuter = facePoints[42];

	left.x = (leftEyeInner.x + leftEyeOuter.x) / 2;
	left.y = (leftEyeInner.y + leftEyeOuter.y) / 2;
	 
	right.x = (rightEyeInner.x + rightEyeOuter.x) / 2;
	right.y = (rightEyeInner.y + rightEyeOuter.y) / 2;
}

void FaceImageTransform::allPointsRotate( cv::Point2f center, std::vector<cv::Point2f>& facePoints, float angle )
{
	cv::Mat rot(3,3, CV_64F);
	
	/*������� ������� ��������*/
	rot.at<double>(0,0) = cos(angle);
	rot.at<double>(0,1) = sin(angle);
	rot.at<double>(0,2) = 0;

	rot.at<double>(1,0) = -1 * sin(angle);
	rot.at<double>(1,1) = cos(angle);
	rot.at<double>(1,2) = 0;

	rot.at<double>(2,0) = -1 * center.x * (cos(angle) - 1.) + center.y * sin(angle);
	rot.at<double>(2,1) = -1 * center.y * (cos(angle) - 1.) - center.x * sin(angle);
	rot.at<double>(2,2) = 1;
	/*------------------------*/

	cv::Point2f shift;
	shift.x = 0;
	shift.y = 0;

	pointRotate(rot, shift);

	for(size_t i = 0; i < facePoints.size(); ++i)
	{
		pointRotate(rot, facePoints[i]);

 		facePoints[i].x += center.x;
 		facePoints[i].y += center.y;

		facePoints[i].x -= shift.x;
		facePoints[i].y -= shift.y;
	}
}

void FaceImageTransform::pointRotate( cv::Mat& rotateMat, cv::Point2f& rotatePoint )
{
	cv::Mat res(1,3, CV_64F);
	cv::Mat orig(1,3, CV_64F);
	cv::Mat rot(3,3, CV_64F);

	orig.at<double>(0,0) = rotatePoint.x;
	orig.at<double>(0,1) = rotatePoint.y;
	orig.at<double>(0,2) = 1.;

	res = orig * rotateMat;

	rotatePoint.x = res.at<double>(0,0);
	rotatePoint.y = res.at<double>(0,1);
}

cv::Mat FaceImageTransform::cropFace( std::vector<cv::Point2f>& facePoints, const cv::Mat& faceImage )
{
	int rectSideSize(_mImgWidth);

	cv::Rect cropRect;

	cv::Point leftEye;
	cv::Point rightEye;

	getEyesCenters(facePoints, leftEye, rightEye);

	
	cropRect.x = leftEye.x - (_mImgWidth - 80) / 2;
	cropRect.y = leftEye.y - (_mImgWidth - 80) / 2;

	cropRect.width = rectSideSize;
	cropRect.height = rectSideSize;
	
	cv::Mat res;
	
	if(cropRect.x >= 0 && cropRect.y >= 0 && cropRect.x + _mImgWidth < faceImage.cols && cropRect.y + _mImgWidth < faceImage.rows)
	{
		for (int i = 0; i < (int)facePoints.size(); ++i)
		{
			facePoints[i].x -= cropRect.x;
			facePoints[i].y -= cropRect.y;
		}

		res = faceImage(cropRect);
	}

	return res;
}