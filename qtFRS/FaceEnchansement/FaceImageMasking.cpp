#include "FaceImageMasking.h"

FaceImageMasking::FaceImageMasking()
{
}

FaceImageMasking::~FaceImageMasking()
{
}

void FaceImageMasking::masking( std::vector<cv::Point2f>& facePoints, cv::Mat& faceImage )
{
	cv::Point leftEye;
	cv::Point rightEye;
	getEyesCenters( facePoints, leftEye, rightEye );

	cv::Point centerEye;
	centerEye.x = leftEye.x + (rightEye.x - leftEye.x) * 0.5f;
	centerEye.y = 0;

	cv::Mat mask = cv::Mat( faceImage.size(), CV_8U, cv::Scalar(0) );	
	cv::Mat res = cv::Mat( faceImage.size(), CV_8U, cv::Scalar(150) );	

	cv::Size size;

	size = cv::Size( cvRound(faceImage.cols * FACE_ELLIPSE_W), cvRound(faceImage.rows * FACE_ELLIPSE_H ) );

	cv::ellipse(mask, centerEye, size, 0, 0, 360, cv::Scalar(255), CV_FILLED);

	faceImage.copyTo(res, mask); 

	res.copyTo(faceImage);
}

void FaceImageMasking::maskingByEyeBrow( std::vector<cv::Point2f>& facePoints, cv::Mat& faceImage )
{
	cv::Point leftEye;
	cv::Point rightEye;
	getEyesCenters( facePoints, leftEye, rightEye );

	cv::Point lEyeBrow = facePoints[17];
	cv::Point rEyeBrow = facePoints[26];

	int width = rEyeBrow.x - lEyeBrow.x;

	cv::Point centerEye;
	centerEye.x = lEyeBrow.x + (rEyeBrow.x - lEyeBrow.x) * 0.5f;
	centerEye.y = 0;

	cv::Mat mask = cv::Mat( faceImage.size(), CV_8U, cv::Scalar(0) );	
	cv::Mat res = cv::Mat( faceImage.size(), CV_8U, cv::Scalar(150) );	

	cv::Size size;

	size = cv::Size( cvRound(width / 2), cvRound(faceImage.rows * FACE_ELLIPSE_H ) );

	cv::ellipse(mask, centerEye, size, 0, 0, 360, cv::Scalar(255), CV_FILLED);

	faceImage.copyTo(res, mask); 

	res.copyTo(faceImage);
}

void FaceImageMasking::getEyesCenters(const std::vector<cv::Point2f>& facePoints, cv::Point& left, cv::Point& right)
{
	cv::Point leftEyeInner = facePoints[37];
	cv::Point leftEyeOuter = facePoints[40];

	cv::Point rightEyeInner = facePoints[44];
	cv::Point rightEyeOuter = facePoints[47];

	left.x = (leftEyeInner.x + leftEyeOuter.x) / 2;
	left.y = (leftEyeInner.y + leftEyeOuter.y) / 2;

	right.x = (rightEyeInner.x + rightEyeOuter.x) / 2;
	right.y = (rightEyeInner.y + rightEyeOuter.y) / 2;
}