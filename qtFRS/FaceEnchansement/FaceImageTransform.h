#ifndef FACEIMAGETRANSFORM_H
#define FACEIMAGETRANSFORM_H

#include <opencv/cv.h>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

class FaceImageTransform
{
public:

	FaceImageTransform(int imageWidth);

	virtual ~FaceImageTransform(void);

	cv::Mat transformFaceImage(std::vector <cv::Point2f>& facePoints, cv::Mat faceImage);
		
private:

	double rotate(std::vector <cv::Point2f>& facePoints, cv::Mat& faceImage);

	void resize(std::vector <cv::Point2f>& facePoints, cv::Mat& faceImage);
	
	void getEyesCenters(const std::vector<cv::Point2f>& facePoints, cv::Point& left, cv::Point& right);

	void allPointsRotate(cv::Point2f center, std::vector<cv::Point2f>& facePoints, float angle);

	void pointRotate(cv::Mat& rotateMat, cv::Point2f& rotatePoint/*, float angle*/);

	cv::Mat cropFace(std::vector<cv::Point2f>& facePoints, const cv::Mat& faceImage);

private:

	int _mImgWidth;
};

#endif