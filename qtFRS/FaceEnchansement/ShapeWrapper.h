#ifndef SHAPEWRAPPER_H
#define SHAPEWRAPPER_H

#include <opencv2/imgproc.hpp>
#include "dlib/image_processing/frontal_face_detector.h"
#include "dlib/image_processing/render_face_detections.h"
#include "dlib/image_processing.h"
#include "dlib/gui_widgets.h"
#include "dlib/image_io.h"
#include "dlib/opencv.h"

class ShapeWrapper
{
public:
	 ShapeWrapper();
	 ~ShapeWrapper();
	 std::vector <cv::Point2f> getShape(const cv::Mat& frame);

private:
	dlib::full_object_detection _mShape;
	dlib::shape_predictor _mShapePredictor;

};
#endif