#ifndef FACEIMAGEPROCESSOR_H
#define FACEIMAGEPROCESSOR_H

#include <QtCore>

#include <opencv2\core\core.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>

#include "AbstractFilter.h"
#include "AbstractExtractor.h"
#include "Biovector.h"
#include "FaceImageMasking.h"

class FaceImageProcessor
{
public:
	FaceImageProcessor();
	~FaceImageProcessor();
	FaceImageProcessor(std::vector<QSharedPointer<AbstractFilter>> &filt, std::vector<QSharedPointer<AbstractExtractor>> &extr);
	
	void processFace(const cv::Mat &src, std::vector < cv::Point2f > &facePoints, Biovector &dst);

private:
	void FaceImageProcessor::zNormal(std::vector < float > &src);

	std::vector<QSharedPointer<AbstractFilter>> mFilters;
	std::vector<QSharedPointer<AbstractExtractor>> mExtractors;

	FaceImageMasking *mFaceMask;

};
#endif