#include "FaceModeller.h"

FaceModeller::FaceModeller(const double angleRangeX, const double angleRangeY, int amountType, double step)
{
	_angleRangeX = angleRangeX;
	_angleRangeY = angleRangeY;
	_amountType = amountType;
	_step = step;
}

FaceModeller::~FaceModeller()
{

}

void FaceModeller::createRMImagesInFolder(const std::string &srcPath, FaceDetector &detector, FaceModeller &modeller)
{
	cv::Mat baseImage;
	cv::Mat curFace;

	std::fstream biovectorFile;

	path basePath(srcPath);

	recursive_directory_iterator baseItr(basePath);

	while (baseItr != recursive_directory_iterator())
	{
		file_status curStatus = baseItr->status();

		if (is_regular_file(curStatus))
		{
			std::string itrPathString = baseItr->path().string();
			bool isValidImageExtension = find(GlobalOptions::IMAGE_EXTENSIONS.begin(), GlobalOptions::IMAGE_EXTENSIONS.end(), extension(*baseItr)) != GlobalOptions::IMAGE_EXTENSIONS.end();
			bool isFrontalBased = itrPathString.find("frontal") != std::string::npos;
			bool isRM = itrPathString.find("_RM_") != std::string::npos;

			if (isValidImageExtension && isFrontalBased && !isRM)
			{
				baseImage = cv::imread(baseItr->path().string());
				int facesCount = detector.detect(baseImage);

				if (facesCount != 0)
				{
					createRotatedMats(baseImage);

					for (int q = 0; q < modeller._mMatVector.size(); q++)
					{
						path curRMPath(itrPathString + "_RM_" + modeller._mRotationLabels[q] + extension(*baseItr));
						remove(curRMPath);

						std::string curRMPathString = curRMPath.string();
						imwrite(curRMPathString, modeller._mMatVector[q]);

						std::cout << "Created rotated image " << curRMPathString << std::endl;
					}
				}
				else
				{
					std::cout << "Attention: No face found! " << baseItr->path().string() << std::endl;
				}
			}

		}

		++baseItr;
	}

	std::cout << "\nRotated images have been created\n" << endl;
}

void FaceModeller::createRotatedMats(cv::Mat srcImg)
{
	//������� �������� ������� ������� � ������ dlib-�
	long cols = (long)srcImg.cols;
	long rows = (long)srcImg.rows;
	dlib::array2d<dlib::rgb_pixel> img(rows, cols);
	for (int y = 0; y < rows; y++)
	{
		for (int x = 0; x < cols; x++)
		{
			cv::Vec3b pixel = srcImg.at<cv::Vec3b>(y, x);
			img[y][x].red = pixel.val[0];
			img[y][x].green = pixel.val[1];
			img[y][x].blue = pixel.val[2];
		}
	}

	//�������� �������������� �����������, ���������� ����
	dlib::frontal_face_detector detector = dlib::get_frontal_face_detector();
	std::vector<dlib::rectangle> dets = detector(img);

	//������ �������������� �������
	dlib::shape_predictor sp;
	dlib::deserialize("shape_predictor_68_face_landmarks.dat") >> sp;
	std::vector<dlib::full_object_detection> shapes;

	for (unsigned long j = 0; j < dets.size(); ++j)
	{
		dlib::full_object_detection shape = sp(img, dets[j]);

		cv::Point3f ptsD[71];
		char* tempo = new char[10];
		for (int i = 0; i < 68; i++)
		{
			ptsD[i].x = shape.part(i).x();
			ptsD[i].y = shape.part(i).y();
		}
		ptsD[68].x = ptsD[16].x;
		ptsD[68].y = 0;

		ptsD[69].x = srcImg.cols / 2;
		ptsD[69].y = 0;

		ptsD[70].x = ptsD[0].x;
		ptsD[70].y = 0;

		//�������� 3D-������ ����
		double** Model3D;
		cv::Point2i* coords;

		coords = new cv::Point2i[71];
		Model3D = new double*[123];
		for (int i = 0; i < 123; i++)
			Model3D[i] = new double[181];

		add3rdPointDLIB(ptsD, coords, Model3D);

		shapes.push_back(shape);

		//����������� ���������������
		double coef = (double)(ptsD[40].x - ptsD[30].x) / 35;

		//����� ���������
		cv::Point3f centerP;
		centerP.x = ptsD[52].x;
		centerP.y = ptsD[52].y;
		centerP.z = ptsD[52].z;


		//����������� ������� ����������
		_ptsD = ptsD;
		_coords = coords;
		_Model3D = Model3D;
		_coef = &coef;
		_centerP = &centerP;
		_srcImg = &srcImg;

		_mMatVector.clear();
		_mRotationLabels.clear();
		
		switch (_amountType)
		{
			case FM_STEPPED_ROTATIONS:

			//break;
			case FM_4LIMIT_ROTATIONS:

			//break;
			case FM_8LIMIT_ROTATIONS:
			default:
				for (double x = -_angleRangeX; x <= _angleRangeX; x += _angleRangeX)
				{
					for (double y = -_angleRangeY; y <= _angleRangeY; y += _angleRangeY)
					{
						cv::Mat resImg = createSingleRotatedMat(x, y);
						_mMatVector.push_back(resImg);

						std::string sx = std::to_string(round(x));
						std::string sy = std::to_string(round(y));
						_mRotationLabels.push_back(sx.substr(0, sx.find('.')) + "_" + sy.substr(0, sy.find('.')));
					}
				}
			break;
		}
	}
}
cv::Mat FaceModeller::createSingleRotatedMat(double angleX, double angleY)
{
	//������ ���� � ��������� �� ������������ (���� ����� ��� ��������)
	MFace faceModelI(_ptsD, _coords, _Model3D, *_coef);
	faceModelI.breakATrs(1000);
	MFace faceModelNC(faceModelI);

	double alpha = angleX * CV_PI / 180;
	double beta = angleY * CV_PI / 180;

	faceModelNC.rotation(*_centerP, alpha, beta);

	// ���������� ����� �� ��������� �� ������
	for (int i = 0; i < faceModelNC.faceTriangles.size(); i++)
	{
		for (int j = i + 1; j < faceModelNC.faceTriangles.size(); j++)
		{
			if (compareByZmin(faceModelNC.faceTriangles[j], faceModelNC.faceTriangles[i]))
			{
				Triangle tmpTriI = faceModelI.faceTriangles[i];
				Triangle tmpTriC = faceModelNC.faceTriangles[i];
				faceModelI.faceTriangles[i] = faceModelI.faceTriangles[j];
				faceModelNC.faceTriangles[i] = faceModelNC.faceTriangles[j];
				faceModelI.faceTriangles[j] = tmpTriI;
				faceModelNC.faceTriangles[j] = tmpTriC;
			}
		}
	}

	//������ ��� � �������������� �����������
	cv::Mat resImg = cv::Mat::zeros(cv::Size(_srcImg->size()), _srcImg->type());

	// ��������� �������� �������������
	for (int i = 0; i < faceModelNC.faceTriangles.size(); i++)
	{
		warpTextureFromTriangle(faceModelI.faceTriangles[i].getTrianglePoints(), *_srcImg, faceModelNC.faceTriangles[i].getTrianglePoints(), resImg);
	}

	return resImg;
}

void FaceModeller::add3rdPointDLIB(cv::Point3f facePoints[71], cv:: Point2i* coords, double** Model3DLIB)
{
	std::ifstream modelIn("Face.txt", std::ios::in);
	for (int i = 0; i < 181; i++)
	for (int j = 0; j < 122; j++)
		modelIn >> Model3DLIB[j + 1][i + 1];

	modelIn.close();

	double coef = (double)(facePoints[40].x - facePoints[30].x) / 35;

	// ������ ����
	coords[0].x = 114; coords[0].y = 64;
	coords[1].x = 113; coords[1].y = 74;
	coords[2].x = 115; coords[2].y = 88;
	coords[3].x = 112; coords[3].y = 113;
	coords[4].x = 107; coords[4].y = 138;
	coords[5].x = 102; coords[5].y = 153;
	coords[6].x = 93; coords[6].y = 165;
	coords[7].x = 79; coords[7].y = 177;
	coords[8].x = 65; coords[8].y = 178;
	coords[9].x = 20; coords[9].y = 135;
	coords[10].x = 37; coords[10].y = 165;
	coords[11].x = 26; coords[11].y = 149;
	coords[12].x = 20; coords[12].y = 134;
	coords[13].x = 15; coords[13].y = 113;
	coords[14].x = 11; coords[14].y = 91;
	coords[15].x = 13; coords[15].y = 80;
	coords[16].x = 12; coords[16].y = 66;

	// ����� �����
	coords[17].x = 109; coords[17].y = 48;
	coords[18].x = 98; coords[18].y = 47;
	coords[19].x = 104; coords[19].y = 48;
	coords[20].x = 85; coords[20].y = 49;
	coords[21].x = 75; coords[21].y = 52;

	// ������ �����
	coords[22].x = 49; coords[22].y = 53;
	coords[23].x = 39; coords[23].y = 51;
	coords[24].x = 34; coords[24].y = 51;
	coords[25].x = 29; coords[25].y = 50;
	coords[26].x = 18; coords[26].y = 50;

	// ���
	coords[27].x = 62; coords[27].y = 65;
	coords[28].x = 62; coords[28].y = 83;
	coords[29].x = 62; coords[29].y = 92;
	coords[30].x = 62; coords[30].y = 104;
	coords[31].x = 77; coords[31].y = 115;
	coords[32].x = 70; coords[32].y = 116;
	coords[33].x = 63; coords[33].y = 117;
	coords[34].x = 55; coords[34].y = 115;
	coords[35].x = 48; coords[35].y = 113;

	// ����� ����
	coords[36].x = 103; coords[36].y = 70;
	coords[37].x = 95; coords[37].y = 64;
	coords[38].x = 87; coords[38].y = 65;
	coords[39].x = 80; coords[39].y = 70;
	coords[40].x = 87; coords[40].y = 72;
	coords[41].x = 95; coords[41].y = 73;

	// ������ ����
	coords[42].x = 45; coords[42].y = 69;
	coords[43].x = 39; coords[43].y = 63;
	coords[44].x = 31; coords[44].y = 64;
	coords[45].x = 24; coords[45].y = 69;
	coords[46].x = 32; coords[46].y = 71;
	coords[47].x = 38; coords[47].y = 71;

	// ���
	coords[48].x = 85; coords[48].y = 140;
	coords[49].x = 79; coords[49].y = 133;
	coords[50].x = 70; coords[50].y = 129;
	coords[51].x = 63; coords[51].y = 130;
	coords[52].x = 54; coords[52].y = 129;
	coords[53].x = 46; coords[53].y = 131;
	coords[54].x = 40; coords[54].y = 137;
	coords[55].x = 46; coords[55].y = 142;
	coords[56].x = 54; coords[56].y = 146;
	coords[57].x = 62; coords[57].y = 146;
	coords[58].x = 71; coords[58].y = 149;
	coords[59].x = 79; coords[59].y = 144;
	coords[60].x = 83; coords[60].y = 140;
	coords[61].x = 72; coords[61].y = 138;
	coords[62].x = 62; coords[62].y = 137;
	coords[63].x = 54; coords[63].y = 137;
	coords[64].x = 42; coords[64].y = 137;
	coords[65].x = 54; coords[65].y = 134;
	coords[66].x = 62; coords[66].y = 134;
	coords[67].x = 72; coords[67].y = 135;

	coords[68].x = 0; coords[68].y = 0;
	coords[69].x = 61; coords[69].y = 0;
	coords[70].x = 121; coords[70].y = 0;

	for (int i = 0; i < 71; i++)
		facePoints[i].z = Model3DLIB[coords[i].x][coords[i].y] * coef;
}

void FaceModeller::warpTextureFromTriangle(cv::Point2f srcTri[3], cv::Mat originalImage, cv::Point2f dstTri[3], cv::Mat warp_final)
{
	cv::Mat warp_mat(2, 3, CV_32FC1);
	cv::Mat warp_dst, warp_mask;
	CvPoint trianglePoints[3];
	trianglePoints[0] = dstTri[0];
	trianglePoints[1] = dstTri[1];
	trianglePoints[2] = dstTri[2];
	warp_dst = cv::Mat::zeros(originalImage.rows, originalImage.cols, originalImage.type());
	warp_mask = cv::Mat::zeros(originalImage.rows, originalImage.cols, originalImage.type());
	/// Get the Affine Transform
	warp_mat = getAffineTransform(srcTri, dstTri);
	/// Apply the Affine Transform to the src image
	warpAffine(originalImage, warp_dst, warp_mat, warp_dst.size());
	cvFillConvexPoly(new IplImage(warp_mask), trianglePoints, 3, CV_RGB(255, 255, 255), 0, 0);
	warp_dst.copyTo(warp_final, warp_mask);
}

bool FaceModeller::compareByZmin(const Triangle& d1, const Triangle& d2)
{
	int minz1 = d1.pt1.z;
	if (d1.pt2.z < minz1) minz1 = d1.pt2.z;
	if (d1.pt3.z < minz1) minz1 = d1.pt3.z;

	int minz2 = d2.pt1.z;
	if (d2.pt2.z < minz2) minz2 = d2.pt2.z;
	if (d2.pt3.z < minz2) minz2 = d2.pt3.z;

	return (minz1 < minz2);
}

bool FaceModeller::compareByZmax(const Triangle& d1, const Triangle& d2)
{
	int maxz1 = d1.pt1.z;
	if (d1.pt2.z > maxz1) maxz1 = d1.pt2.z;
	if (d1.pt3.z > maxz1) maxz1 = d1.pt3.z;

	int maxz2 = d2.pt1.z;
	if (d2.pt2.z > maxz2) maxz2 = d2.pt2.z;
	if (d2.pt3.z > maxz2) maxz2 = d2.pt3.z;

	return (maxz1 > maxz2);
}