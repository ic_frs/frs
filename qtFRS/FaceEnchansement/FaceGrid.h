#ifndef FACEGRID_H
#define FACEGRID_H

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <stdlib.h>
#include <stdio.h>

class FaceGrid
{
public:
	FaceGrid(int imageSize, size_t cellNumber, float scaleCoef, const cv::Mat& weightMap);

	~FaceGrid();

	//GridData createGrid(int imageSize, size_t cellNumber, float scaleCoef, const cv::Mat& weightMap);

	cv::Mat getImageOfCell(cv::Mat& img, int index);

	std::vector<cv::Rect> getRects(void);

	//GridData getGrid(void);

private:
	float calcWeight(cv::Mat weightMap, cv::Rect rc);

private:
	//GridData grid;

	std::vector<cv::Rect> cellsRects;
};

#endif