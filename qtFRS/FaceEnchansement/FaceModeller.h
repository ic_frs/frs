#ifndef FACEMODELLER_H
#define FACEMODELLER_H

#include <opencv2\core.hpp>
#include <opencv2\objdetect.hpp>
#include <opencv2\imgproc.hpp>

#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_io.h>

#include <fstream>
#include <boost\filesystem.hpp>

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <ppl.h>

#include "GlobalOptions.h"
#include "FaceDetector.h"
#include "FaceModel.h"


#define FM_8LIMIT_ROTATIONS 1
#define FM_4LIMIT_ROTATIONS 2
#define FM_STEPPED_ROTATIONS 3

using namespace fml;
using namespace boost::filesystem;

class FaceModeller
{
public:
	//������� � ����������� ������������� � �� ��������
	std::vector<cv::Mat> _mMatVector;
	std::vector<std::string> _mRotationLabels;

public:
	FaceModeller(const double angleRangeX, const double angleRangeY, int amountType, double step);
	~FaceModeller();

	//���������� ����������� ��������� ��� ��� ���� ����������� � �����
	void createRMImagesInFolder(const std::string &srcPath, FaceDetector &detector, FaceModeller &modeller);

	//�������� ������� ����������� ��������� ������� � ��������, ���������������� ���� ��������
	void createRotatedMats(cv::Mat srcImg);

	//������������ ������ ������ � �������� �����������
	cv::Mat createSingleRotatedMat(double angleX, double angleY);

private:
	int _amountType;
	double _step, _angleRangeX, _angleRangeY;
	
	//������� ������
	std::string *_srcPath;
	cv::Point3f *_ptsD;
	cv::Point2i *_coords;
	double **_Model3D;
	double *_coef;
	cv::Point3f *_centerP;
	cv::Mat *_srcImg;


private:
	//���������� ������� ���������� ����������� ������ ����
	void add3rdPointDLIB(cv::Point3f facePoints[71], cv::Point2i* coords, double** Model3DLIB);

	//��������� �������� �������������
	void warpTextureFromTriangle(cv::Point2f srcTri[3], cv::Mat originalImage, cv::Point2f dstTri[3], cv::Mat warp_final);

	//����� ����� ������� ��� ������� ����� ����
	bool compareByZmin(const Triangle& d1, const Triangle& d2);
	bool compareByZmax(const Triangle& d1, const Triangle& d2);
};

#endif