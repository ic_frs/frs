#include "ShapeWrapper.h"

 ShapeWrapper::ShapeWrapper()
{
	dlib::deserialize("shape_predictor_68_face_landmarks.dat") >> _mShapePredictor;
}

ShapeWrapper::~ShapeWrapper()
{

}

std::vector <cv::Point2f> ShapeWrapper::getShape( const cv::Mat& frame )
{
	cv::Rect rect;
	rect.x = 0;
	rect.y = 0;
	rect.width = frame.cols;
	rect.height = frame.rows;

	cv::Mat rgbFrame;
	
	if(frame.channels() == 1)
		cvtColor(frame, rgbFrame, CV_GRAY2RGB);
	else
		rgbFrame = frame;
	
	std::vector <cv::Point2f> res;

	// ������� ���������, ������������� ����� ����������, � �������� dlib'������
	dlib::rectangle dRect(long(rect.tl().x), long(rect.tl().y), long(rect.br().x), long(rect.br().y));

	// ������� cv::Mat � array2d<rgb_pixel>
	dlib::array2d<dlib::rgb_pixel> img;
	dlib::assign_image(img, dlib::cv_image<dlib::bgr_pixel>(rgbFrame));

	// ����� ������ ����� ���� (shapes)
	_mShape = _mShapePredictor(img, dRect);

	// ������� ����� dlib::Point � cv::Point
	std::vector <dlib::point> shapeParts;
	for (int i = 0; i < (int)_mShape.num_parts(); ++i)
	{
		shapeParts.push_back(_mShape.part(i));
		cv::Point2f shapePartCv;
		shapePartCv.x = shapeParts[i].x() - rect.x;
		shapePartCv.y = shapeParts[i].y() - rect.y;
		res.push_back(shapePartCv);
	}

	return  res;
}

