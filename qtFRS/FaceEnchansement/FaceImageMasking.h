#ifndef FACEIMAGEMASKING_H
#define FACEIMAGEMASKING_H

#include <opencv/cv.h>
#include <opencv2/imgproc/imgproc.hpp>

static const double DESIRED_LEFT_EYE_X = 0.19;
static const double DESIRED_LEFT_EYE_Y = 0.14;
static const double FACE_ELLIPSE_CY = 0.40;
static const double FACE_ELLIPSE_W = 0.5;         // Should be atleast 0.5
static const double FACE_ELLIPSE_H = 1.4;         // Controls how tall the face mask is.

class FaceImageMasking
{
public:
	FaceImageMasking();

	~FaceImageMasking();

	void masking(std::vector<cv::Point2f>& facePoints, cv::Mat& faceImage);

	void maskingByEyeBrow(std::vector<cv::Point2f>& facePoints, cv::Mat& faceImage);

private:
	void getEyesCenters(const std::vector<cv::Point2f>& facePoints, cv::Point& left, cv::Point& right);
};

#endif