#include "Tracker.h"
using namespace trk;

Tracker::Tracker()
{
}

// ������������� ��������
Tracker::Tracker(vector <Rect> firstRects)
{	// ����������� ����� ��������������� � �� ������
	lastTracks.Rects = firstRects;
	for (int i = 0; i < firstRects.size(); i++)
		lastTracks.Nums.push_back(i+1);
}

// ����� ������������ ����� �������
void Tracker::tracking(vector <Rect> curRects, vector <int> &curNums)
{	// ����������� ����� ����� ���������������
	Tracks curTracks;
	curTracks.Rects = curRects;
	// ��������� ������ ������� �������
	curNums.clear();
	// ��������� �������� ���������������
	for (int j = 0; j < curTracks.Rects.size(); j++)
	{
		// ����� �������������� �� �������� ������
		Point2d curCenter;
		curCenter.x = curTracks.Rects[j].x + curTracks.Rects[j].width / 2;
		curCenter.y = curTracks.Rects[j].y + curTracks.Rects[j].height / 2;
		for (int i = 0; i < lastTracks.Rects.size(); i++)
		{
			// ����� �������������� �� �������� ������
			Point2d prevCenter;
			prevCenter.x = lastTracks.Rects[i].x + lastTracks.Rects[i].width / 2;
			prevCenter.y = lastTracks.Rects[i].y + lastTracks.Rects[i].height / 2;
			// ���������� �� ������ �� ����
			int prevRad = sqrt((lastTracks.Rects[i].x - prevCenter.x)*(lastTracks.Rects[i].x - prevCenter.x) + (lastTracks.Rects[i].y - prevCenter.y)*(lastTracks.Rects[i].y - prevCenter.y));

			// ���������� ����� ��������
			int curDist = sqrt((curCenter.x - prevCenter.x)*(curCenter.x - prevCenter.x) + (curCenter.y - prevCenter.y)*(curCenter.y - prevCenter.y)); 
			// ���� �������������� ������, ����������� �� ���������� �����
			if (prevRad>curDist)
			{
				curTracks.Nums.push_back(lastTracks.Nums[i]);
				break;
			}
		}
		// ���� ��� �������� ��������������, ������� ���������� ����� ������������� ����� �� ���������
		if (j >= curTracks.Nums.size())
		{	// �������� ����� �������
			int maxNum = 0;
			if (lastTracks.Nums.size() > 0)
				maxNum = lastTracks.Nums[distance(lastTracks.Nums.begin(), max_element(begin(lastTracks.Nums), end(lastTracks.Nums)))];
			// ����� 
			for (int k = 1; k <= maxNum; k++)
				if (find(begin(lastTracks.Nums), end(lastTracks.Nums), k) == end(lastTracks.Nums))
					curTracks.Nums.push_back(k);
			// �� �� ���� � ��� ��������� ������� ���, �� +1
			if (j >= curTracks.Nums.size())
				curTracks.Nums.push_back(maxNum+1);
		}
	}
	// ������ ����� ���������� �����
	lastTracks = curTracks;
	curNums = curTracks.Nums;
}