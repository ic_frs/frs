#include <opencv2\core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <stdio.h>
#include <iostream>

using namespace cv;
using namespace std;

#ifndef _TRACKER_H_
#define _TRACKER_H_
namespace trk{
	struct Tracks
	{
		vector <Rect> Rects;
		vector <int> Nums;
	};

	class Tracker
	{
	public:
		Tracks lastTracks;					// ����� �����-������������� ����
		Tracker();							// ����������� �� ���������
		Tracker(vector <Rect> firstRects);	// �����������, ������������ ��������� ��� � �����

		void tracking(vector <Rect> curRects, vector <int> &curNums); // �������, ������������ ��������� ��� � ����� � �����������
	};
} //namespace trk
#endif