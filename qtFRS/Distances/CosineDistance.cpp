#include "CosineDistance.h"

CosineDistance::CosineDistance(void)
{
}

CosineDistance::~CosineDistance(void)
{
}

double CosineDistance::calc(const Biovector& v1, const Biovector& v2)
{
	double biovectProduct = std::inner_product(v1.begin(), v1.end(), v2.begin(), 0.0);
	
	double v1Norm = l2norm(v1);
	double v2Norm = l2norm(v2);

	return (-(biovectProduct / (v1Norm * v2Norm)) + 1.0) / 2.0;
}

double CosineDistance::calc(const Biovector& v1, const Biovector& v2, bool print)
{
	return 0;
}

double CosineDistance::l2norm(const Biovector& v)
{
	double res = 0.0;

	for (size_t i = 0; i < v.size(); ++i)
		res += v[i] * v[i];

	res = sqrt(res);

	if (res == 0)
	{
		res = 0.000000000001;
	}

	return res;
}