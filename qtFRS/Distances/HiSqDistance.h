#ifndef HISQDISTANCE_H
#define HISQDISTANCE_H

#include <math.h>
#include "AbstractDistance.h"

class HiSqDistance : public AbstractDistance
{

public:
	HiSqDistance(void);

	~HiSqDistance(void);

	double calc(const Biovector& v1, const Biovector& v2);
};

#endif