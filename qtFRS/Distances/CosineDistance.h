#ifndef COSINEDISTANCE_H
#define COSINEDISTANCE_H

#include <math.h>
#include <numeric>
#include "AbstractDistance.h"

class CosineDistance : public AbstractDistance
{

public:
	CosineDistance(void);

	~CosineDistance(void);

	double calc(const Biovector& v1, const Biovector& v2);

	double calc(const Biovector& v1, const Biovector& v2, bool print);

	double l2norm(const Biovector& v);
};

#endif