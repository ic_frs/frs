#ifndef EUCLIDDISTANCE_H
#define EUCLIDDISTANCE_H

#include <math.h>
#include "AbstractDistance.h"

class EuclidDistance : public AbstractDistance
{

public:
	EuclidDistance(void);

	~EuclidDistance(void);

	double calc(const Biovector& v1, const Biovector& v2);

	double calc(const Biovector& v1, const Biovector& v2, bool print);
};

#endif