#include "HiSqDistance.h"

HiSqDistance::HiSqDistance(void)
{
}

HiSqDistance::~HiSqDistance(void)
{
}

double HiSqDistance::calc( const Biovector& v1, const Biovector& v2)
{
	assert(v1.size() == v2.size() && "v1.size() != v2.size()");
	assert(v1.getSubVectorCount() == v2.getSubVectorCount() && "v1.getSubVectorCount() != v2.getSubVectorCount()");

	double res = 0;

	for (size_t i = 0; i < v1.getSubVectorCount(); ++i)
	{
		Biovector sv1 = v1.getSubVector(i);
		Biovector sv2 = v2.getSubVector(i);
		
		float weight1 = v1.getSubVectorWeight(i);
		float weight2 = v2.getSubVectorWeight(i);

		assert(weight1 == weight2 && "weight1 != weight2");

		assert(sv1.getSubVectorCount() == sv2.getSubVectorCount() && "sv1.getSubVectorCount() != sv2.getSubVectorCount()");

		double localRes = 0;
		Biovector sv3 = sv1 - sv2;
		Biovector sv4 = sv1 + sv2;
		sv3 *= sv3;

		for(size_t j = 0; j < sv3.size(); ++j)
		{
			if(sv4[j] != 0 && sv3[j] != 0)
				localRes += sv3[j] / sv4[j];
		}

		res += weight1 * localRes;
	}

	return res * 1000.;
}