#ifndef ABSTRACTDISTANCE_H
#define ABSTRACTDISTANCE_H

#include "Biovector.h"

class AbstractDistance
{

public:

	virtual ~AbstractDistance(void)
	{};

	virtual double calc(const Biovector& v1, const Biovector& v2) = 0;
};

#endif