#include "EuclidDistance.h"

EuclidDistance::EuclidDistance(void)
{
}

EuclidDistance::~EuclidDistance(void)
{
}

double EuclidDistance::calc( const Biovector& v1, const Biovector& v2 )
{
	double res = 0;

	Biovector v3 = v1 - v2;
	v3 *= v3;

	for(size_t i = 0; i < v3.size(); ++i)
		res += v3[i];
	
	return sqrt(res);
}

double EuclidDistance::calc( const Biovector& v1, const Biovector& v2, bool print )
{
	return 0;
}