#ifndef QTFRSCONFIGURATIONMANAGER_H
#define QTFRSCONFIGURATIONMANAGER_H

#include <windows.h>

#include <QDialog>
#include <QFileDialog>
#include <QDate>
#include <QSettings>
#include <QDebug>
#include <QLineEdit> 
#include <QCheckBox> 
#include <QScrollArea> 

#include "qtFRSMainWindow.h"
#include "GlobalOptions/GlobalOptions.h"



namespace Ui {
class qtFRSConfigurationManager;
}

class qtFRSConfigurationManager : public QDialog
{
    Q_OBJECT

public:
    explicit qtFRSConfigurationManager(QWidget *parent = 0);
    ~qtFRSConfigurationManager();

	QHash<QString, QWidget*> formFields;

private:
    Ui::qtFRSConfigurationManager *ui;

private slots:
void closeWindow();
void saveNewConfiguration();
void loadConfiguration();
void loadManagerData();
void saveConfiguration();
void deleteConfiguration();
void discardChanges();
void initConfigurationManagement();
void updateConfigurationList();
void updateFormFields();
void disableConfigurationManagement();
void setDefaultConfiguration();
void resetValuesToDefault();
void configComboBoxChanged(const int index);

signals:
	void issueOpenConfigManager(bool updateList = true);

};

#endif // QTFRSCONFIGURATIONMANAGER_H
